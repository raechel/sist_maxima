<?php

date_default_timezone_set("America/Guatemala");

class main {


	public function openConn()
	{
					
		    $host = 'pdb2.awardspace.com';
			$user = 'emda_sist';
			$pass = 'libertad87*';
			$db   = 'emda_sist';
			
			$conn = mysqli_connect($host,$user,$pass,$db);
				
		
		/*	$host = 'localhost';
			$user = 'root';
			$pass = 'root';
			$db   = 'emda_sist';
			
			$conn = mysqli_connect($host,$user,$pass,$db);*/
		
		
		return $conn;
	}
	
	
	public function getActiveUsersAndPlans()
	{
		$conn = $this->openConn();
		
		$query = "SELECT c.organization_id, s.amount, c.name as client_name, ca.* 
			FROM client_agreement ca, service s, client c
				WHERE s.service_id = ca.service_id
					AND ca.status_id = 1
					AND day(agreement_date) = day(now())
					AND c.client_id = ca.client_id";
					
		if($result = mysqli_query($conn,$query))
		{
			$array = array();
			while($row = mysqli_fetch_array($result))
			{				
				$array[] = $row;
			}
		}			
		
		mysqli_close($conn);
		return $array;
		
	}


	public function checkIfInvoiceAlreadyExists($clientAgreementId,$serviceId)
	{
		$conn = $this->openConn();
		
		$query = "SELECT * FROM payment_invoice 
					WHERE month(created_at) = month(now())
						AND year(created_at) = year(now())
						AND invoice_type IN (1,3)
						AND custom_invoice is null 
						AND service_id = '$serviceId' 
						AND client_agreement_id = '$clientAgreementId'; ";
		
		$result = mysqli_query($conn, $query);
		$num = mysqli_num_rows($result);
		if($num > 0)
		{
			mysqli_close($conn);
			return true;
		}				
		
		mysqli_close($conn);
		return false;
									
	}
	
	
	public function insertInvoice($clientId,$serviceId,$organizationId,$clientAgreementId,$amount,$discount)
	{
		$conn = $this->openConn();
		$now = date("Y-m-d H:i:s");
		
		$query = "INSERT payment_invoice (client_id, service_id, organization_id, invoice_status_id, client_agreement_id, amount, discount, invoice_type, created_at) 
					VALUES ('$clientId','$serviceId','$organizationId','1','$clientAgreementId','$amount','$discount',1,'$now'); ";
		if($result = mysqli_query($conn,$query) or die(mysqli_error($conn)))
		{
			mysqli_close($conn);
			return true;
		};
		
		mysqli_close($conn);
		return false;			
	}
	

}

?>