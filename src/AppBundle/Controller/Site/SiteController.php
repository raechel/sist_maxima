<?php

namespace AppBundle\Controller\Site;

use AppBundle\Entity\Plan;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use AppBundle\Helper\MailHelper;
use AppBundle\Helper\UrlHelper;
use AppBundle\Helper\UtilsHelper;
use AppBundle\Helper\TumblrHelper;
use Symfony\Component\HttpFoundation\Response;


class SiteController extends Controller
{
	
    /**
     * Lists all plan entities.
     *
     * @Route("/", name="site_home")
     * @Method("GET")
     */
    public function indexAction()
    {
    
	 	
		
        return $this->render('@App/Site/index.html.twig', array(
            'page'=>'home',
            'posts'=>$selectedPosts,
            'features'=>$arr
        ));
    }


	/**     
     * @Route("/que-es", name="site_about")
     * @Method("GET")
     */
    public function aboutAction()
    {
        		
        return $this->render('@App/Site/about.html.twig', array(
            'page'=>'about'            
        ));
    }
	
	/**     
     * @Route("/contacto", name="site_contact")
     * @Method("GET")
     */
    public function contactAction()
    {
        
		
        return $this->render('@App/Site/contact.html.twig', array(
            'page'=>'contact'
        ));
    }	

	/**     
     * @Route("/cursos/{slug}/{id}", name="site_course_detail")
     * @Method("GET")
     */
    public function courseDetailAction(Request $request, $slug, $id)
    {
        
        return $this->render('@App/Site/course_detail.html.twig', array(
           "title"=>"Temp" 
        ));
    }	
	
	
	/**     
     * @Route("/cursos", name="site_courses")
     * @Method("GET")
     */
    public function coursesAction()
    {
     
		
        return $this->render('@App/Site/courses.html.twig', array(
            
        ));
    }	
	

	/**     
     * @Route("/beneficios/{id}/{type}", name="site_benefits_content")
     * @Method("GET")
     */
    public function benefitsAction(Request $request)
    {
        
		$id = $request->get('id');
		
		$arr = array();
		$mainTitle = "";
		
		switch($id)
		{
			case 1:
				$mainTitle = "Estas son algunas de las cosas que usted, como Director, puede hacer en Kaans:";				
				$filter = 'Directores';	
			break;
			case 2:
				$mainTitle = "Estas son algunas de las cosas que usted, como Docente, puede hacer en Kaans:";
				$filter = 'Docentes';		
			break;
			case 3:
				$mainTitle = "Estas son algunas de las cosas que usted, como Alumno, puede hacer en Kaans:";
				$filter = 'Alumnos';		
			break;
			case 4:
				$mainTitle = "Estas son algunas de las cosas que usted, como Padre de Familia, puede hacer en Kaans:";
				$filter = 'Padres';		
			break;	 
		}
		
		
		$features = new UtilsHelper();
		$feats = $features->featureList();
		$arr = array();		
		foreach($feats as $f)
		{
			if($f['for'] == $filter)
			{
				$arr[] = $f;
			}
		}
		
		if($request->get('type') == 1)
		{
	        return $this->render('@App/Site/_benefits_content.html.twig', array(
	            'id'=>$id,
	            'list' => $arr,
	            'main_title'=>$mainTitle
	        ));
        } else {
        	return $this->render('@App/Site/_benefits_content_v2.html.twig', array(
	            'id'=>$id,
	            'list' => $arr,
	            'main_title'=>$mainTitle
	        ));
        }
    }



   /**
     * Lists all plan entities.
     *
     * @Route("/solicitud_contacto/{type}", name="site_send_email")
     * @Method("POST")
     */
    public function sendEmailAction(Request $request, $type)
    {

		$email = new MailHelper();
		
		$msg = '';		
		
		$name = $request->get('name');
		if($request->get('school'))
		{
			$name = $request->get('name').' - '.$request->get('school');
		}
		
		if($request->get('message'))
		{
			$msg = $request->get('message');	
		}
		
		if($email->publicContactForm($request->get('email'), $name, $request->get('phone'), $msg, $type))
		{
			$code = 'MF000';
		} else {
			$code = 'MF255';
		}
				
		return new Response($code);
		
		
        
    }

}
