<?php

namespace AppBundle\Controller\Backend;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use AppBundle\Entity\Inventory;
use AppBundle\Entity\InventorySell;
use AppBundle\Entity\InventoryPurchase;
use AppBundle\Form\InventoryType;
use AppBundle\Form\InventoryPurchaseType;
use AppBundle\Helper\UserHelper;
use AppBundle\Helper\UtilsHelper;
use AppBundle\Helper\UrlHelper;
use AppBundle\Helper\MailHelper;
use AppBundle\Repository\Kaans;
use Symfony\Component\HttpFoundation\JsonResponse;


class InventoryController extends Controller {
	
    
    private $moduleId = 6;
	private $moduleName = "Inventario";




	
	/**
	 * @Route("/backend/inventory/dashboard", name="backend_inventory_dashboard")
	 */
	public function dashboardAction(Request $request) {
		$this->get ( "session" )->set ( "module_id",10);
		$this->get ( "session" )->set ( "module_name","Reporte de Ventas");
		$userData = $this->get ( "session" )->get ( "userData" );
		
		$mp = Kaans::getModulePermission($this->moduleId, $this->get("session")->get("userModules"));
		
		$em = $this->getDoctrine()->getManager();
		$topSells = $em->getRepository ( 'AppBundle:Inventory' )->getTopSells($userData);
		$inventoryStatus = $em->getRepository ( 'AppBundle:Inventory' )->getInventoryStatus($userData, false, 10);
		
		$bestDayToSell = $em->getRepository('AppBundle:Inventory')->reportBestDaySell($userData);
		$bestDayToSell_labels_arr = array();
		$bestDayToSell_values_arr = array();
		foreach($bestDayToSell as $key => $value)
		{
			$bestDayToSell_labels_arr[] = "'".$key."'";
			$bestDayToSell_values_arr[] = "'".(strlen($value)>0 ? $value : 0)."'";	
		};
		$bestDayToSell_labels = implode(",",$bestDayToSell_labels_arr);
		$bestDayToSell_values = implode(",",$bestDayToSell_values_arr);
		
		
		
		$utilitiesLabel_arr = array();
		$utilitiesValue_arr = array();
		for($i=1; $i<=12; $i ++)
		{
			$utilitiesYear = $em->getRepository('AppBundle:Inventory')->reportUtilitiesYear($userData, $i);			
			$month = $this->getMonthName($i);										
			$utilitiesLabel_arr[] = "'".$month."'";
			$utilitiesValue_arr[] = "'".(strlen($utilitiesYear['difference'])>0 ? $utilitiesYear['difference'] : 0)."'";																		
		}
		
		$utilitiesLabel = implode(",",$utilitiesLabel_arr);
		$utilitiesValue = implode(",",$utilitiesValue_arr);
		
		
		
		
		$reportPurchaseLabel_arr = array();
		$reportPurchase_arr = array();
		
		$bestHours = $em->getRepository('AppBundle:Inventory')->reportBestHours($userData);			
		foreach($bestHours as $item)
		{
			$reportPurchaseLabel_arr[] = $item['hour'];
			$reportPurchaseValue_arr[] = $item['sell_prices'];	
		}
		
		$utilitiesLabel = implode(",",$reportPurchaseLabel_arr);
		$utilitiesValue = implode(",",$reportPurchaseValue_arr);
		
		
		
		
		$inventoryMainLabel_arr = array();
		$inventoryMainValue_arr = array();
		for($i=1; $i<=12; $i ++)
		{
			$result = $em->getRepository('AppBundle:Inventory')->inventoryMainReport($userData, $i);			
			$month = $this->getMonthName($i);										
			$inventoryMainLabel_arr[] = "'".$month."'";
			
			$preResulSell = (strlen($result['net_sell_price'])>0 ? number_format($result['net_sell_price'],2,".",".") : 0);
			$inventoryMainValue_arr[] = "'".$preResulSell."'";
			
			$preResultPurchase = (strlen($result['net_purchase_price'])>0 ? number_format($result['net_purchase_price'],2,".",".") : 0);
			$difference = ($preResulSell - $preResultPurchase);
			$inventoryMainValue2_arr[] = "'".$difference."'";
			
			$netPurchase = (strlen($result['net_purchase'])>0 ? number_format($result['net_purchase'],2,".",".") : 0);			
			$inventoryMainValue3_arr[] = "'".$netPurchase."'";																			
		}
		
		$inventoryMainLabel = implode(",",$inventoryMainLabel_arr);
		$inventoryMainValue = implode(",",$inventoryMainValue_arr);
		$inventoryMainValue2 = implode(",",$inventoryMainValue2_arr);
		$inventoryMainValue3 = implode(",",$inventoryMainValue3_arr);
		
		
		
		
		return $this->render ( '@App/Backend/Inventory/dashboard.html.twig', array (				
		    "permits" => $mp,
		    "topSells" => $topSells,
		    "inventoryStatus"=>$inventoryStatus,
		    
			"bestDayToSell_labels" => $bestDayToSell_labels,
			"bestDayToSell_values" => $bestDayToSell_values,
			
			"utilitiesLabel" => $utilitiesLabel,
			"utilitiesValue" => $utilitiesValue,
			
			"reportPurchaseLabel" => $reportPurchaseLabel,
			"reportPurchaseValue" => $reportPurchaseValue,
			
			"inventoryMainLabel" => $inventoryMainLabel,
			"inventoryMainValue" => $inventoryMainValue,
			"inventoryMainValue2" => $inventoryMainValue2,
			"inventoryMainValue3" => $inventoryMainValue3
			
		));
	}


 	/**
     * @Route("/backend/inventory/purchase", name="backend_inventory_purchase")
     */
	public function indexPurchaseAction(Request $request) {
				
		$this->get("session")->set("module_id", 8);	
		$this->get ( "session" )->set ( "module_name",$this->moduleName);	
		$userData = $this->get ( "session" )->get ( "userData" );
		
		$object = new InventoryPurchase();
		$form = $this->createForm ( new InventoryPurchaseType (), $object );
		$form->handleRequest ( $request );
		$em = $this->getDoctrine()->getManager();

		$thisUser = $em->getRepository ( 'AppBundle:User' )->findOneBy(array('userId'=>$userData['id']));
		
		// Validar formulario
		if ($form->isSubmitted ()) {
			if ($form->isValid ()) {
				

				if($error)
				{
					// Add error flash
					$this->addFlash('error_message', $error);
					
				} else {
										
					$em = $this->getDoctrine()->getManager();					
					
					$organization = $this->getDoctrine()->getManager()->getRepository ( 'AppBundle:Organization' )->findOneBy(array('organizationId'=>$userData['organization_id']));
					
					$object->setOrganization($organization);															    				
				    $object->setCreatedBy($thisUser);
				    $object->setCreatedAt(new \DateTime());			
				    $em->persist($object);
				    $em->flush();
					
					$this->addFlash ( 'success_message', $this->getParameter ( 'exito' ) );
					return $this->redirectToRoute ( "backend_inventory_purchase");
					
				}
			}
			else {
				$this->addFlash ( 'error_message', $this->getParameter ( 'error_form' ).$form->getErrorsAsString() );
			}
		}
		
		
		
		$list = $em->getRepository ( 'AppBundle:InventoryPurchase' )->findBy (array(
			'organization'=>$userData['organization_id']			
		));
		
		$products = $em->getRepository ( 'AppBundle:Inventory' )->findBy (array(
			'organization'=>$userData['organization_id'],
			'isActive'=>'1'			
		));
		
		$mp = Kaans::getModulePermission($this->moduleId, $this->get("session")->get("userModules"));		
		
		return $this->render ( '@App/Backend/Inventory/purchase_index.html.twig', array (
				"form" => $form->createView (),
				"list" => $list,				
		        "permits" => $mp,
		        'userInfo'=>$thisUser,		        		        
		        'userData'=>$userData,
		        "products"=>$products		        
		) );
		
	}


    /**
     * @Route("/backend/inventory", name="backend_inventory")
     */
	public function indexAction(Request $request) {
				
		$this->get("session")->set("module_id", $this->moduleId);	
		$this->get ( "session" )->set ( "module_name",$this->moduleName);	
		$userData = $this->get ( "session" )->get ( "userData" );
		
		$object = new Inventory();
		$form = $this->createForm ( new InventoryType (), $object );
		$form->handleRequest ( $request );
		$em = $this->getDoctrine()->getManager();

		$thisUser = $em->getRepository ( 'AppBundle:User' )->findOneBy(array('userId'=>$userData['id']));
		
		// Validar formulario
		if ($form->isSubmitted ()) {
			if ($form->isValid ()) {
				
						
				$em = $this->getDoctrine()->getManager();
				
				$file = $object->getImagePath();
				if($file)
				{						
					$fileName = md5(uniqid()) . '.' . $file->guessExtension();
					$file->move($this -> getParameter('upload_files_products'), $fileName);						
					$object->setImagePath($fileName);												
				} 
				
				$organization = $this->getDoctrine()->getManager()->getRepository ( 'AppBundle:Organization' )->findOneBy(array('organizationId'=>$userData['organization_id']));
				
				$object->setOrganization($organization);															    				
			    $object->setCreatedBy($this->getUser());
			    $object->setCreatedAt(new \DateTime());			
			    $em->persist($object);
			    $em->flush();
				
				$this->addFlash ( 'success_message', $this->getParameter ( 'exito' ) );
				return $this->redirectToRoute ( "backend_inventory");
					
				
			} else {
				$this->addFlash ( 'error_message', $this->getParameter ( 'error_form' ).$form->getErrorsAsString() );
			}
		}
		
		
		
		$list = $em->getRepository ( 'AppBundle:Inventory' )->findBy (array(
			'organization'=>$userData['organization_id'],
			'isActive'=>1
		));
		//$paginator = $this->get ( 'knp_paginator' );
		
		//$pagination = $paginator->paginate ( $queryUser, $request->query->getInt ( 'page', 1 ), $this->getParameter ( "number_of_rows" ) );
		
		$mp = Kaans::getModulePermission($this->moduleId, $this->get("session")->get("userModules"));		
		
		return $this->render ( '@App/Backend/Inventory/index.html.twig', array (
				"form" => $form->createView (),
				"list" => $list,				
		        "permits" => $mp,
		        'userInfo'=>$thisUser,		        		        
		        'userData'=>$userData		        
		) );
		
	}




	
	/**
     * @Route("/backend/inventory/cart/complete", name="backend_inventory_cart_complete")
     */
	public function cartCompleteAction(Request $request)
	{
		$key = $request->get('key')-1;
		$currentCart = $this->get("session")->get("cart");
		$cartArr = json_decode($currentCart);
		
		$userData = $this->get ( "session" )->get ( "userData" );
		
		$organization = $this->getDoctrine()->getManager()->getRepository ( 'AppBundle:Organization' )->findOneBy(array(
			'organizationId'=>$userData['organization_id']
		));
		
		foreach($cartArr as $thisKey => $item)
		{
		
			$inventory = $this->getDoctrine()->getManager()->getRepository ( 'AppBundle:Inventory' )->findOneBy(array(
				'inventoryId'=>$item->id
			));	
			
			$object = new InventorySell();			
			$object->setOrganization($organization);
			$object->setInventory($inventory);
			$object->setInvoiceNumber($request->get('invoice'));
			$object->setPurchasePrice($item->purchase_price);
			$object->setDiscountTotal($item->discount);
			$grandTotal = ($item->sell_price*$item->quantity)-$item->discount; 
			$object->setGrandTotal($grandTotal);
			$object->setSellPrice($item->sell_price);
			$object->setQuantity($item->quantity);
		    $object->setCreatedBy($this->getUser());
		    $object->setCreatedAt(new \DateTime());
			
			$em = $this->getDoctrine()->getManager();			
		    $em->persist($object);
		    $em->flush();	
		}		
		
		$this->get("session")->set("cart",json_encode(array()));		
		return $this->redirectToRoute('backend_inventory_sell',array("thanks"=>true));		
	}	
		


	
	/**
     * @Route("/backend/inventory/cart/clean", name="backend_inventory_cart_clean")
     */
	public function cartCleanAction(Request $request)
	{
				
		$this->get("session")->set("cart",json_encode(array()));		
		return $this->redirectToRoute('backend_inventory_sell');		
	}	

	
	/**
     * @Route("/backend/inventory/cart/discount", name="backend_inventory_discount_cart")
     */
	public function cartDiscountAction(Request $request)
	{
		$key = $request->get('key')-1;
		$currentCart = $this->get("session")->get("cart");
		$cartArr = json_decode($currentCart);
		
		$arrClean = array();
		foreach($cartArr as $thisKey => $item)
		{
			
			if($thisKey == $key)
			{
				$item->discount = $request->get("value");				
			} else {
				
			}		
			
			$arrClean[] = $item;
		}		
				
		$this->get("session")->set("cart",json_encode($arrClean));		
		return $this->redirectToRoute('backend_inventory_sell');		
	}	
	
	
	

	/**
     * @Route("/backend/inventory/cart/remove", name="backend_inventory_remove_cart")
     */
	public function cartRemoveAction(Request $request)
	{
		$key = $request->get('key')-1;
		$currentCart = $this->get("session")->get("cart");
		$cartArr = json_decode($currentCart);
		
		$arrClean = array();
		foreach($cartArr as $thisKey => $item)
		{
			if($thisKey == $key)
			{
				
			} else {
				$arrClean[] = $item;
			}
			
		}		
		
		
		$this->get("session")->set("cart",json_encode($arrClean));		
		return $this->redirectToRoute('backend_inventory_sell');		
	}	
	




	/**
     * @Route("/backend/inventory/status", name="backend_inventory_status")
     */
	public function inventoryStatusAction(Request $request)
	{
		$this->get("session")->set("module_id",9);	
		$this->get ( "session" )->set ( "module_name",$this->moduleName);	
		$userData = $this->get ( "session" )->get ( "userData" );		
			
		$em = $this->getDoctrine()->getManager();
		$list = $em->getRepository ( 'AppBundle:Inventory' )->getInventoryStatus($userData);
		
		return $this->render ( '@App/Backend/Inventory/inventory_status.html.twig', array (							
	        'list' => $list
		));
		
			
	}	
	
	
	

	/**
     * @Route("/backend/inventory/sell/history", name="backend_inventory_sell_history")
     */
	public function historyAction(Request $request)
	{
		$this->get("session")->set("module_id",7);	
		$this->get ( "session" )->set ( "module_name",$this->moduleName);	
		$userData = $this->get ( "session" )->get ( "userData" );		
		
		$from = "";
		$to   = "";
		$resume = false;
		if($request->get('dateFrom') && $request->get('dateTo'))
		{
			$from = $request->get('dateFrom'); 
			$to   = $request->get('dateTo');
			$resume = true;
		}	
			
		$em = $this->getDoctrine()->getManager();
		$list = $em->getRepository ( 'AppBundle:Inventory' )->getSellHistory($userData, $from, $to);
		
		return $this->render ( '@App/Backend/Inventory/sell_history.html.twig', array (							
	        'list' => $list,
	        "from" => $from,
	        "to"   => $to,
	        "resume" => $resume	        
		));
		
			
	}	
	

	/**
     * @Route("/backend/inventory/get/available", name="backend_inventory_get_available")
     */
	public function getAvailableAction(Request $request)
	{
		$userData = $this->get ( "session" )->get ( "userData" );		
		if($request->get('id'))
		{
			$cart = json_decode($this->get ( "session" )->get ( "cart" ));
			$thisCart = 0;
			foreach($cart as $item)
			{
				if($item->id == $request->get('id'))
				{
					$thisCart += $item->quantity;
				}
			}
			
			$id = $request->get('id');	
			$em = $this->getDoctrine()->getManager();
			$inventory = $em->getRepository ( 'AppBundle:Inventory' )->getInventoryStatus($userData, $request->get('id'));
			return $this->render ( '@App/Backend/Inventory/inventory_available.html.twig', array (							
		        'inventory' => $inventory,
		        'thisCart'=>$thisCart
			));
		}
			
	}	
	
			
	/**
     * @Route("/backend/inventory/get/measure", name="backend_inventory_get_measure")
     */
	public function getMeasureAction(Request $request)
	{
			
		if($request->get('id'))
		{
			$id = $request->get('id');	
			$em = $this->getDoctrine()->getManager();
			$inventory = $em->getRepository ( 'AppBundle:Inventory' )->findOneBy(array('inventoryId'=>$id));
			return $this->render ( '@App/Backend/Inventory/measure_options.html.twig', array (							
		        'inventory' => $inventory
			));
		}
			
	}	
	
	
		
	 /**
     * @Route("/backend/inventory/sell", name="backend_inventory_sell")
     */
	public function sellAction(Request $request) {
				
		$this->get("session")->set("module_id",7);	
		$this->get ( "session" )->set ( "module_name","Realizar venta");	
		$userData = $this->get ( "session" )->get ( "userData" );
		//$this->get("session")->set("cart",json_encode(array()));
		
		if($this->get("session")->get("cart"))
		{
			$currentCart = $this->get("session")->get("cart");
		} else {
			$this->get("session")->set("cart",json_encode(array()));
			$currentCart = $this->get("session")->get("cart");
		};
		
		$thanks = 0;
		if($request->get('thanks'))
		{
			$this->addFlash ( 'success_message', "¡Venta reportada con exito!");
		}
		
		
		$em = $this->getDoctrine()->getManager();
		$thisUser = $em->getRepository ( 'AppBundle:User' )->findOneBy(array('userId'=>$userData['id']));
		
		$products = $em->getRepository("AppBundle:Inventory")->findBy(array(
			'organization'=>$userData['organization_id'],
			'isActive'=>1	
		));

		if($request->get('pid') && $request->get('quantity'))
		{
			
			$product = $em->getRepository ( 'AppBundle:Inventory' )->findOneBy(array('inventoryId'=>$request->get('pid')));
						
			$cartArr = json_decode($currentCart);
			$new = array(
				'id'=>$product->getInventoryId(),
				'name'=>$product->getName(),
				'sell_price'=>$product->getSellPrice(),
				'purchase_price'=>$product->getPurchasePrice(),
				'quantity'=>$request->get('quantity'),
				'discount'=>0,
				'measure'=>$product->getMeasureType()->getName()
			);
			
			array_push($cartArr,$new);			
			$this->get("session")->set("cart",json_encode($cartArr));
			$currentCart = $this->get("session")->get("cart");
			
		}
				
				
		$mp = Kaans::getModulePermission($this->moduleId, $this->get("session")->get("userModules"));		
		
		return $this->render ( '@App/Backend/Inventory/sell.html.twig', array (				
				"list" => $list,				
		        "permits" => $mp,
		        'userInfo' => $thisUser,		        		        
		        'userData' => $userData,
		        'results' => $results,
		        'currentCart'=>json_decode($currentCart),
		        'products' => $products		        	       
		) );
		
	}



	/**
	 * @Route("/backend/inventory/edit/{id}", name="backend_inventory_edit")
	 */
	public function editAction(Request $request) {
		
		$userData = $this->get ( "session" )->get ( "userData" );
		$md5 = $request->get ( "id" );
		
		$objectId = $this->getDoctrine()->getManager()->getRepository ( 'AppBundle:Inventory' )->findOneByMd5Id($md5);
		$object   = $this->getDoctrine()->getManager()->getRepository ( 'AppBundle:Inventory' )->findOneBy(array('inventoryId'=>$objectId,'isActive'=>"1"));
		
		$thisUser = $this->getDoctrine()->getManager()->getRepository ( 'AppBundle:User' )->findOneBy(array('userId'=>$userData['id']));
		
		if ($object) {
					
			$form = $this->createForm ( new InventoryType (), $object );
			$form->handleRequest ( $request );
			
			$oldImage = $object -> getImagePath();
						
			if ($form->isSubmitted ()) {
				if ($form->isValid ()) {
							
							
					$em = $this->getDoctrine()->getManager();
									
					$file = $object->getImagePath();
					if ($file) {
						$fileName = md5(uniqid()) . '.' . $file->guessExtension();
						$file -> move($this -> getParameter('upload_files_products'), $fileName);						
						$object -> setImagePath($preUrl.$fileName);
						
					} else {
						$object -> setImagePath($oldImage);
					}
									
					
					$em->persist($object);
					$em->flush();
				
					$this->addFlash ( 'success_message', $this->getParameter ( 'exito_actualizar' ) );
				} else {
					// Error validation
					$this->addFlash ( 'error_message', $this->getParameter ( 'error_form' ) );
				}
			}

			$mp = Kaans::getModulePermission($this->moduleId, $this->get("session")->get("userModules"));
			
			return $this->render ( '@App/Backend/Inventory/edit.html.twig', array(
				"form" => $form->createView (),
				"edit" => true,
				'userInfo'=>$thisUser,
				'object'=>$object,					
				"permits" => $mp					
			) );
		} else {
			$this->addFlash ( 'error_message', $this->getParameter ( 'error_editar' ) );
		}
		return $this->redirectToRoute ( "backend_client");
	}
	
	
	
	

	/**
	 * @Route("/backend/inventory/delete/{id}", name="backend_inventory_delete")
	 */
	public function deleteAction(Request $request) {
			
		$md5 = $request->get ( "id" );
		$userId = $this->getDoctrine()->getManager()->getRepository ( 'AppBundle:Inventory' )->findOneByMd5Id($md5);
		$object   = $this->getDoctrine()->getManager()->getRepository ( 'AppBundle:Inventory' )->findOneByInventoryId($objectId);
		
		if ($object) {
			// Can't delete yourself
			
				$em = $this->getDoctrine ()->getManager ();

				// Make inactive
				$object->setIsActive(0);
				$em->persist( $object );
				$em->flush ();
				
				$this->addFlash ( 'success_message', $this->getParameter ( 'exito_eliminar' ) );
			
		} else {
			$this->addFlash ( 'error_message', $this->getParameter ( 'error_eliminar' ) );
		}
		
		return $this->redirectToRoute ( "backend_inventory");
	}
	
	
	/**
	 * @Route("/backend/inventory/history/delete/{id}", name="backend_inventory_history_delete")
	 */
	public function deleteHistoryAction(Request $request) {
			
		$md5 = $request->get ( "id" );
		$objectId = $this->getDoctrine()->getManager()->getRepository ( 'AppBundle:Inventory' )->findOneSellByMd5Id($md5);
		$object   = $this->getDoctrine()->getManager()->getRepository ( 'AppBundle:InventorySell' )->findOneByInventorySellId($objectId['inventory_sell_id']);
		
		if ($object) {
			// Can't delete yourself
			
				$em = $this->getDoctrine ()->getManager ();				
				$em->remove($object);				
				$em->flush ();
				
				$this->addFlash ( 'success_message', $this->getParameter ( 'exito_eliminar' ) );
			
		} else {
			$this->addFlash ( 'error_message', $this->getParameter ( 'error_eliminar' ) );
		}
		
		return $this->redirectToRoute ( "backend_inventory_sell_history");
	}
	
	
	

	/**
	 * @Route("/backend/inventory/purchase/delete/{id}", name="backend_inventory_purchase_delete")
	 */
	public function deletePurchaseAction(Request $request) {
			
		$md5 = $request->get ( "id" );
		$objectId = $this->getDoctrine()->getManager()->getRepository ( 'AppBundle:Inventory' )->findOnePurchaseByMd5Id($md5);
		$object   = $this->getDoctrine()->getManager()->getRepository ( 'AppBundle:InventoryPurchase' )->findOneByInventoryPurchaseId($objectId['inventory_purchase_id']);
		
		if ($object) {
			// Can't delete yourself
			
				$em = $this->getDoctrine ()->getManager ();

				// Make inactive
				$em->remove($object);				
				$em->flush ();
				
				$this->addFlash ( 'success_message', $this->getParameter ( 'exito_eliminar' ) );
			
		} else {
			$this->addFlash ( 'error_message', $this->getParameter ( 'error_eliminar' ).$f );
		}
		
		return $this->redirectToRoute ( "backend_inventory_purchase");
	}
		


	public function getMonthName($i)
	{
		switch($i)
			{
				case 1:
					$month = "ENERO";
				break;
				case 2:
					$month = "FEBRERO";
				break;
				case 3:
					$month = "MARZO";
				break;
				case 4:
					$month = "ABRIL";
				break;
				case 5:
					$month = "MAYO";
				break;
				case 6:
					$month = "JUNIO";
				break;
				case 7:
					$month = "JULIO";
				break;
				case 8:
					$month = "AGOSTO";
				break;
				case 9:
					$month = "SEPTIEMBRE";
				break;
				case 10:
					$month = "OCTUBRE";
				break;
				case 11:
					$month = "NOVIEMBRE";
				break;
				case 12:
					$month = "DICIEMBRE";
				break;	

			}
			
			return $month;
	}	

}
