<?php

namespace AppBundle\Controller\Backend;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use AppBundle\Entity\PaymentInvoiceStatus;
use AppBundle\Entity\PaymentInvoice;
use AppBundle\Entity\PaymentInvoiceLog;
use AppBundle\Form\PaymentInvoiceStatusType;
use AppBundle\Form\PaymentInvoiceType;
use AppBundle\Helper\UserHelper;
use AppBundle\Helper\UtilsHelper;
use AppBundle\Helper\UrlHelper;
use AppBundle\Helper\MailHelper;
use AppBundle\Repository\Kaans;
use Symfony\Component\HttpFoundation\JsonResponse;


class InvoiceController extends Controller {
	
    
    private $moduleId = 12;
	private $moduleName = "Consulta de Recibos";
	
    /**
     * @Route("/backend/invoice", name="backend_invoice")
     */
	public function indexAction(Request $request) {
		
		$this->get("session")->set("module_id", $this->moduleId);	
		$this->get ( "session" )->set ( "module_name",$this->moduleName);	
		$userData = $this->get ( "session" )->get ( "userData" );
		$em = $this->getDoctrine()->getManager();
		
		$filter = 1;
		if($request->get('filter'))
		{
			$filter = $request->get('filter');
			switch($filter)
			{
				case 1:
					$title = "Actuales";
				break;
				case 2:					
					$title = "Atrasadas";
				break;
				case 3:
					$title = "Pagadas";
				break;
			}
		}
		
		$clientListRaw = $em->getRepository ( 'AppBundle:Client' )->findBy(array('organization'=>$userData['organization_id']));
		$clientList = array();
		foreach($clientListRaw as $item)
		{
			$clientList[] = $item->getClientId();
		}
		
		$object = new PaymentInvoice ();
		$form = $this->createForm ( new PaymentInvoiceType (), $object, array("clientAgreementList"=>implode(",",$clientList),"organizationId"=>$userData['organization_id']) );
		$form->handleRequest ( $request );
		$em = $this->getDoctrine()->getManager();

		// Validar formulario
		if ($form->isSubmitted ()) {
			
				
			if ($form->isValid ()) {
					
				$serviceId = $request->get('service');
				$servicePlain = $request->get('service_plain');
						
				//$user = $this->create($user, $this->getUser());					
				$organization = $this->getDoctrine()->getManager()->getRepository ( 'AppBundle:Organization' )->findOneBy(array('organizationId'=>$userData['organization_id']));
				
				//$invoice      = $this->getDoctrine()->getManager()->getRepository ( 'AppBundle:InvoiceStatus' )->findOneBy(array('invoiceStatusId'=>1));				
				$object->setOrganization($organization);
				if($request->get('type_invoice') == '1')
				{
					
					$service = $this->getDoctrine()->getManager()->getRepository ( 'AppBundle:Service' )->findOneBy(array('serviceId'=>$serviceId));
					$object->setService($service);
				} else {
					$object->setCustomInvoice($servicePlain);
				}	
				$object->setInvoiceType($request->get('type_invoice'));																    			
				
			    /*$object->setCreatedBy($this->getUser());*/
			    $object->setCreatedAt(new \DateTime($request->get('created_at')));		
			    $em->persist($object);
			    $em->flush();
				
				$this->addFlash ( 'success_message', $this->getParameter ( 'exito' ) );
				return $this->redirectToRoute ( "backend_invoice");
								
			} else {
				//."|".$form->getErrorsAsString()
				$this->addFlash ( 'error_message', $this->getParameter ( 'error_form' ));
			}
		}
		
		
		$em = $this->getDoctrine()->getManager();

		$thisUser = $em->getRepository ( 'AppBundle:User' )->findOneBy(array('userId'=>$userData['id']));
		
		$list = $em->getRepository ( 'AppBundle:PaymentInvoice' )->getInvoices($userData,$filter);
		
		$serviceList = $this->getDoctrine()->getManager()->getRepository ( 'AppBundle:Service' )->findBy(array('organization'=>$userData['organization_id'],'statusId'=>1));
		$clientList = $this->getDoctrine()->getManager()->getRepository ( 'AppBundle:Client' )->findBy(array('organization'=>$userData['organization_id'],'statusId'=>1));
		$mp = Kaans::getModulePermission($this->moduleId, $this->get("session")->get("userModules"));		
		
		
		return $this->render ( '@App/Backend/Invoice/index.html.twig', array (
				
				"list" => $list,	
				"filter" => $filter,		
		        "permits" => $mp,
		        "clientList"=>$clientList,
		        "serviceList" => $serviceList,
		        "title"=>$title,
		        'userInfo'=>$thisUser,		        		        
		        'userData'=>$userData,
		        "form" => $form->createView ()		        
		) );
	}

	
	/**
	 * @Route("/backend/invoice/edit/{id}", name="backend_invoice_edit")
	 */
	public function editAction(Request $request) {
		
		$userData = $this->get ( "session" )->get ( "userData" );
		$md5 = $request->get ( "id" );
		
		$objectId = $this->getDoctrine()->getManager()->getRepository ( 'AppBundle:Client' )->findOneByMd5Id($md5);
		$object   = $this->getDoctrine()->getManager()->getRepository ( 'AppBundle:Client' )->findOneBy(array('clientId'=>$objectId,'statusId'=>"1"));
		
		$thisUser = $this->getDoctrine()->getManager()->getRepository ( 'AppBundle:User' )->findOneBy(array('userId'=>$userData['id']));
		
		if ($object) {
					
			$form = $this->createForm ( new ClientType (), $object );
			$form->handleRequest ( $request );
			if ($form->isSubmitted ()) {
				if ($form->isValid ()) {
									
					// Set updated By
					//$object->setCreatedBy($editor);
					//$object->setCreatedAt(new \DateTime());			
					$em = $this->getDoctrine()->getManager();
					$em->persist($object);
					$em->flush();
				
					$this->addFlash ( 'success_message', $this->getParameter ( 'exito_actualizar' ) );
				} else {
					// Error validation
					$this->addFlash ( 'error_message', $this->getParameter ( 'error_form' ) );
				}
			}

			$mp = Kaans::getModulePermission($this->moduleId, $this->get("session")->get("userModules"));
			
			return $this->render ( '@App/Backend/Client/edit.html.twig', array(
				"form" => $form->createView (),
				"edit" => true,
				'userInfo'=>$thisUser,
				'object'=>$object,					
				"permits" => $mp					
			) );
		} else {
			$this->addFlash ( 'error_message', $this->getParameter ( 'error_editar' ) );
		}
		return $this->redirectToRoute ( "backend_client");
	}


	
	/**
	 * @Route("/backend/invoice/agreement_selection", name="backend_invoice_agreement_selection")
	 */
	public function agreementSelectionAction(Request $request) {
		
		$cid = $request->get("cid");
		$list = $this->getDoctrine()->getManager()->getRepository ( 'AppBundle:ClientAgreement' )->findBy(array('client'=>$cid));
		return $this->render ( '@App/Backend/Invoice/_agreement_selection.html.twig', array(
				"list"=>$list				
			) );	
		
	}
	
	
	public function setPaymentLog($paymentObj, $comments,$logType,$custom=false)
	{
		$userData = $this->get ( "session" )->get ( "userData" );
		$user = $this->getDoctrine()->getManager()->getRepository ( 'AppBundle:User' )->findOneByUserId($userData['id']);
		
		if($logType == 1)
		{
			$comments = "Se modifico descuento a Q$custom. ".$comments;;
		}
		
		$object = new PaymentInvoiceLog();
		$object->setClient($paymentObj->getClient());
		$object->setComments($comments);
		// 1 = modificacion
		// 2 = reporte de pago
		$object->setLogType($logType);
		$object->setPaymentInvoice($paymentObj);
		$object->setCreatedAt(new \DateTime);
		$object->setCreatedBy($user);
		
		$em = $this->getDoctrine()->getManager();
		$em->persist($object);
		$em->flush();
	}
	
	/**
	 * @Route("/backend/invoice/management/{id}", name="backend_invoice_management")
	 */
	public function managementnAction(Request $request,$id) {
			//var_dump($request);die;
		$userData = $this->get ( "session" )->get ( "userData" );
		$cid = $id;
		
		$raw = $this->getDoctrine()->getManager()->getRepository ( 'AppBundle:PaymentInvoice' )->findOneInvoiceByMd5Id($cid);
		$object = $this->getDoctrine()->getManager()->getRepository ( 'AppBundle:PaymentInvoice' )->findOneByInvoiceId($raw['invoice_id']);
		
		if($request->get('discount') && $request->get('comments'))
		{
			$object->setDiscount($request->get('discount'));
			$object->setVoucherNumber($request->get('payment_voucher'));
			$em = $this->getDoctrine()->getManager();
			$em->persist($object);
			$em->flush();
			
			$this->setPaymentLog($object,$request->get('comments'),1,$request->get('discount'));
			
			$this->addFlash ( 'success_message', $this->getParameter ( 'exito_actualizar' ) );		
		}
		
		if($request->get('report_payment'))
		{
			$invoiceStatus = $this->getDoctrine()->getManager()->getRepository ( 'AppBundle:PaymentInvoiceStatus' )->findOneBy(array('invoiceStatusId'=>2));
			$object->setInvoiceStatus($invoiceStatus);
			$object->setVoucherNumber($request->get('payment_voucher'));
			$object->setPayedAt(new \DateTime);
			$object->setPaymentBy($userData['id']);
			
			
			$em = $this->getDoctrine()->getManager();
			$em->persist($object);
			$em->flush();
			
			$this->setPaymentLog($object,"Se confirma el pago de esta factura",2);
			
			$this->addFlash ( 'success_message', $this->getParameter ( 'exito_actualizar' ) );		
		}
		
		$logs = $this->getDoctrine()->getManager()->getRepository ( 'AppBundle:PaymentInvoiceLog' )->findByPaymentInvoice($raw['invoice_id'],array('paymentInvoiceLogId'=>'DESC'));
		
		if($request->get('for_print'))
		{
			return $this->render ( '@App/Backend/Invoice/invoice_management_print.html.twig', array(
				"object"=>$object,
				"logs"=>$logs,
				'userData'=>$userData,
				"invoiceId" => $id
			));	
		} else {
			return $this->render ( '@App/Backend/Invoice/invoice_management.html.twig', array(
				"object"=>$object,
				"logs"=>$logs,
				'userData'=>$userData,
				"invoiceId" => $id,
				"id" => $cid,
				"ticket" => $request->get('payment_voucher')
			));	
		}
		
	}
	
	
	
	/**
	 * @Route("/backend/invoice/delete/{id}", name="backend_invoice_delete")
	 */
	public function deleteAction(Request $request) {
			
		$md5 = $request->get ( "id" );
		 
		$objectId = $this->getDoctrine()->getManager()->getRepository ( 'AppBundle:PaymentInvoice' )->findOneInvoiceByMd5Id($md5);
		
		$object   = $this->getDoctrine()->getManager()->getRepository ( 'AppBundle:PaymentInvoice' )->findOneByInvoiceId($objectId['invoice_id']);
		
		
		if ($object) {
			// Can't delete yourself
			$status   = $this->getDoctrine()->getManager()->getRepository ( 'AppBundle:PaymentInvoiceStatus' )->findOneByInvoiceStatusId(3);
			$em = $this->getDoctrine ()->getManager ();
			$object->setInvoiceStatus($status); 
			$em->persist( $object );
			$em->flush ();
			
			$this->addFlash ( 'success_message', $this->getParameter ( 'exito_eliminar' ) );
			
		} else {
			$this->addFlash ( 'error_message', $this->getParameter ( 'error_eliminar' ) );
		}
		
		return $this->redirectToRoute ( "backend_invoice");
	}
	
	


	 

	public function base64_url_encode($input) {
	 return strtr(base64_encode($input), '+/=', '._-');
	}
}
