<?php

namespace AppBundle\Controller\Backend;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use AppBundle\Entity\PaymentInvoiceCut;
use AppBundle\Form\PaymentInvoiceCutType;
use AppBundle\Repository\Kaans;
use AppBundle\Helper\UrlHelper;

/**
 * Board controller. 
 */
class InvoiceCutController extends Controller {
	
	private $moduleId = 6;
	private $moduleName = "Cierres de caja";


	/**
	 * @Route("/backend/payment_invoice/cut/print", name="backend_payment_invoice_cut_print")
	 */
	public function cutPrintAction(Request $request) {
			
		$this->get ( "session" )->set ( "module_id",$this->moduleId);		
		$this->get ( "session" )->set ( "module_name",$this->moduleName);
		$userData = $this->get ( "session" )->get ( "userData" );
		
		$em = $this -> getDoctrine() -> getManager();		
		$pendingAmountDetails = $this -> getDoctrine() -> getRepository('AppBundle:PaymentInvoice') -> getReportedCutDetails($request->get('cutId'));
		
		
		$mp = Kaans::getModulePermission($this->moduleId, $this->get("session")->get("userModules"));
				
		return $this->render ( '@App/Backend/Invoice/cut_print.html.twig', array (				
		    "permits" => $mp,		    
		    "pendingAmountDetails" => $pendingAmountDetails		      
		));		
		
	}	
		
		
	/**
	 * @Route("/backend/payment_invoice/cut", name="backend_payment_invoice_cut")
	 */
	public function indexAction(Request $request) {
			
		$this->get ( "session" )->set ( "module_id",$this->moduleId);		
		$this->get ( "session" )->set ( "module_name",$this->moduleName);
		$userData = $this->get ( "session" )->get ( "userData" );
		
		$object = new PaymentInvoiceCut();
		$form = $this -> createForm(new PaymentInvoiceCutType(), $object);
		$form -> handleRequest($request);		
		
		$em = $this -> getDoctrine() -> getManager();				
		
		
		
		$selUser = $request->get('selected_user');
		if($request->get('report_user_id'))
		{
			$selUser = $request->get('report_user_id');
		}
						
		$pendingAmountDetails = array();				
		$totalPendingAmount = "";
		$cutList = array();
		if($selUser)
		{
			if(strlen($selUser)>0)
			{
				$cutList = $this -> getDoctrine() -> getRepository('AppBundle:PaymentInvoiceCut') -> findBy(array('user'=>$selUser),array('paymentInvoiceCutId'=>'DESC'));
				$selectedUserObject   = $this -> getDoctrine() -> getRepository('AppBundle:User') -> findOneBy(array('userId'=>$selUser));
				$lastCutDate          = $this -> getDoctrine() -> getRepository('AppBundle:PaymentInvoice') -> getLastCutDate($selUser);
				
				//$pendingAmount        = $this -> getDoctrine() -> getRepository('AppBundle:PaymentInvoice') -> getPendingAmount($selUser,$lastCutDate['created_at']);
				$pendingAmountDetails = $this -> getDoctrine() -> getRepository('AppBundle:PaymentInvoice') -> getPendingAmountDetails($selUser,$lastCutDate['created_at']);
				
				//$totalPendingAmount = $pendingAmount['pending_amount'];
			}
		}
		
		
			
		if($request->get('report_pending_amount') && $request->get('report_user_id'))
		{
			
			
			$userSelected = $this->getDoctrine()->getRepository('AppBundle:User') -> findOneBy(array('userId'=>$request->get('report_user_id')));
			$userCreated = $this->getDoctrine()->getRepository('AppBundle:User') -> findOneBy(array('userId'=>$userData['user_id']));
			$organization = $this->getDoctrine()->getManager()->getRepository ( 'AppBundle:Organization' )->findOneBy(array('organizationId'=>$userData['organization_id']));
			
			
			$cut = new PaymentInvoiceCut();
			$cut->setOrganization($organization);
			$cut->setUser($userSelected);
			$cut->setTotalAmount($request->get('report_pending_amount'));
			$cut->setComments($request->get('report_comments'));
			$cut->setCreatedAt(new \DateTime());
			$cut->setCreatedBy($userCreated);
			$em -> persist($cut);
			$em -> flush();
			
			$invoiceIds = array();
			$readyIds = "";
			foreach($pendingAmountDetails as $item)
			{
				$invoiceIds[] = $item['invoice_id'];
			}
			$readyIds = implode(",",$invoiceIds);
			$pendingAmountDetails = $this -> getDoctrine() -> getRepository('AppBundle:PaymentInvoice') -> reportCutForInvoices($readyIds,$cut->getPaymentInvoiceCutId());
			
			
			$this -> addFlash('success_message', $this -> getParameter('exito'));
			
		}
				
		
		
		//TODO: este es el mismo codigo de arriba, hay que arreglarlo para que solo se ejecute 1 v ez
		$selUser = $request->get('selected_user');
		if($request->get('report_user_id'))
		{
			$selUser = $request->get('report_user_id');
		}
						
		$pendingAmountDetails = array();				
		$totalPendingAmount = "";
		$cutList = array();
		if($selUser)
		{
			if(strlen($selUser)>0)
			{
				$cutList = $this -> getDoctrine() -> getRepository('AppBundle:PaymentInvoiceCut') -> findBy(array('user'=>$selUser),array('paymentInvoiceCutId'=>'DESC'));
				$selectedUserObject   = $this -> getDoctrine() -> getRepository('AppBundle:User') -> findOneBy(array('userId'=>$selUser));
				$lastCutDate          = $this -> getDoctrine() -> getRepository('AppBundle:PaymentInvoice') -> getLastCutDate($selUser);
				
				//$pendingAmount        = $this -> getDoctrine() -> getRepository('AppBundle:PaymentInvoice') -> getPendingAmount($selUser,$lastCutDate['created_at']);
				$pendingAmountDetails = $this -> getDoctrine() -> getRepository('AppBundle:PaymentInvoice') -> getPendingAmountDetails($selUser,$lastCutDate['created_at']);
				
				//$totalPendingAmount = $pendingAmount['pending_amount'];
			}
		}
	
		
				// Validar formulario
		/*if ($form -> isSubmitted()) {
			if ($form -> isValid()) {
								
				$object -> setCreatedAt(new \DateTime());
				$object -> setCreatedBy($userData["id"]);
				$object -> setOrganization($userInfo->getOrganization());	
				$em -> persist($object);
				$em -> flush();
					
				$visibleTo = $request->get("visible_to_departments");			
				foreach($visibleTo as $visible)
				{
										
					$department = $this -> getDoctrine() -> getRepository('AppBundle:Department') -> findOneByDepartmentId($visible);
					$flowDepartment = new FlowDepartment();
					$flowDepartment->setDepartment($department);
					$flowDepartment->setFlow($object);
					$em -> persist($flowDepartment);
					$em -> flush();	
				}
		

				$this -> addFlash('success_message', $this -> getParameter('exito'));
				return $this -> redirectToRoute("backend_flow",array('pid'=>$pid));
			} else {
				$this -> addFlash('error_message', $this -> getParameter('error_form'));
			}
		}*/
		
		$userList = $this -> getDoctrine() -> getRepository('AppBundle:User') -> findBy(array('organization'=>$userData['organization_id']));
		
		$mp = Kaans::getModulePermission($this->moduleId, $this->get("session")->get("userModules"));
				
		return $this->render ( '@App/Backend/Invoice/cut.html.twig', array (				
		    "permits" => $mp,
		    "form"    => $form -> createView(),
		    "userList"      => $userList,
		    "cutList"       => $cutList,
		    "selUserObject" => $selectedUserObject,
		    //"pendingAmount" => $totalPendingAmount,
		    "pendingAmountDetails" => $pendingAmountDetails,
		    "selectedUser" => $request->get('selected_user')  
		));
	}



}
