<?php

namespace AppBundle\Controller\Backend;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use AppBundle\Entity\Step;
use AppBundle\Entity\StepRequirement;
use AppBundle\Form\StepType;
use AppBundle\Form\RequirementType;
use AppBundle\Repository\Kaans;
use AppBundle\Helper\UrlHelper;

/**
 * Board controller.
 */
class StepController extends Controller {
	
	private $moduleId = 8;
	private $moduleName = "Pasos del flujo";


	/**
	 * @Route("/backend/step/flow/{fid}/process/{pid}", name="backend_step")
	 */
	public function indexAction(Request $request,$fid,$pid) {
			
		$this->get ( "session" )->set ( "module_id",$this->moduleId);		
		$this->get ( "session" )->set ( "module_name",$this->moduleName);
		$userData = $this->get ( "session" )->get ( "userData" );
		
		$object = new Step();
		$form = $this -> createForm(new StepType(), $object);
		$form -> handleRequest($request);
		
		$userInfo = $this -> getDoctrine() -> getRepository('AppBundle:User') -> findOneByUserId($userData['id']);
		$flowInfo = $this -> getDoctrine() -> getRepository('AppBundle:Flow') -> findOneByMd5Id($fid);
		$flow     = $this -> getDoctrine() -> getRepository('AppBundle:Flow') -> findOneByFlowId($flowInfo['flow_id']);
		
				// Validar formulario
		if ($form -> isSubmitted()) { 
			if ($form -> isValid()) {

				$object -> setCreatedAt(new \DateTime());
				$object -> setCreatedBy($userData["id"]);
				$object -> setOrganization($userInfo->getOrganization());
				$object -> setFlow($flow);
				$em = $this -> getDoctrine() -> getManager();				
				$em -> persist($object);
				$em -> flush();

				$this -> addFlash('success_message', $this -> getParameter('exito'));
				return $this -> redirectToRoute("backend_step",array('pid'=>$pid,'fid'=>$fid));
			} else {
				$this -> addFlash('error_message', $this -> getParameter('error_form'));
			}
		}
		
		
		$list = $this -> getDoctrine() -> getRepository('AppBundle:Step') -> getStepsByFlowId($flowInfo['flow_id']);
		$resultList = array(); 
		foreach($list as $item)
		{
			$stepReqs = $this -> getDoctrine() -> getRepository('AppBundle:Step') -> getStepsReqsByStepId(md5($item['stepId'])); 
			$resultList[] = array('step'=>$item,'requirements'=>$stepReqs);	
		}
		
		
		
		$mp = Kaans::getModulePermission($this->moduleId, $this->get("session")->get("userModules"));
		
		$departments = $this -> getDoctrine() -> getRepository('AppBundle:Department') -> getList($userData, $userInfo->getOrganization()->getOrganizationId());
		$reqTypes    = $this -> getDoctrine() -> getRepository('AppBundle:RequirementType') -> findAll();
		$positions   = $this -> getDoctrine() -> getRepository('AppBundle:Position') -> findBy(array("Organization"=>$userInfo->getOrganization()->getOrganizationId()));
		
		return $this->render ( '@App/Backend/Step/index.html.twig', array (				
		    "permits" => $mp,
		    "form"    => $form->createView(),
		    "list"    => $list,
		    'fid' 	  => $fid,
		    'pid'	  => $pid,
		    'flow'	  => $flow,
		    'departments' => $departments,
		    'reqTypes' => $reqTypes,
		    'positions' => $positions,
		    'resultList' => $resultList
		));
	}


	/**
	 * @Route("/backend/step_requirement/save", name="backend_step_requirement_save")
	 */
	public function stepReqSaveAction(Request $request) {
			
		$this->get ( "session" )->set ( "module_id",$this->moduleId);		
		$this->get ( "session" )->set ( "module_name",$this->moduleName);
		$userData = $this->get ( "session" )->get ( "userData" );
				
		$userInfo = $this -> getDoctrine() -> getRepository('AppBundle:User') -> findOneByUserId($userData['id']);
		$srid = false;
		if($request->get('req'))
		{
			
			$values = $request->get('req');
			$name = $values['name'];
			$desc = $values['description'];
			$dept = $values['department'];
			$type = $values['type'];
			$posi = $values['position'];
			$fid  = $values['fid'];
			$pid  = $values['pid'];
			$sid  = $values['sid'];
			$isPersonal = (isset($values['is_personal']) ? '1' : '0');
			$bossMustApprove = (isset($values['boss_must_approve']) ? '1' : '0');
			
			$stepEntity = $this -> getDoctrine() -> getRepository('AppBundle:Step') -> findOneByMd5Id($sid);
			$step = $this -> getDoctrine() -> getRepository('AppBundle:Step') -> findOneByStepId($stepEntity['step_id']);
			$reqType = $this -> getDoctrine() -> getRepository('AppBundle:RequirementType') -> findOneByRequirementTypeId($type);
			$dep = $this -> getDoctrine() -> getRepository('AppBundle:Department') -> findOneByDepartmentId($dept);
			$pos = $this -> getDoctrine() -> getRepository('AppBundle:Position') -> findOneByPositionId($posi);
			
			if(isset($values['srid']))
			{
				$srid = $values['srid'];
				$stepReqEntity = $this -> getDoctrine() -> getRepository('AppBundle:Step') -> findOneReqByMd5Id($values['srid']);			
				$object = $this -> getDoctrine() -> getRepository('AppBundle:StepRequirement') -> findOneByStepRequirementId($stepReqEntity['step_requirement_id']);
			} else {
				$object = new StepRequirement();
			}
			$object -> setName($name);
			$object -> setDescription($desc);
			$object -> setStep($step);
			if($isPersonal == 0 || $bossMustApprove == 0)
			{
				$object -> setPosition($pos);
				$object -> setDepartment($dep);
			}
			$object -> setIsPersonal($isPersonal);
			$object -> setBossMustApprove($bossMustApprove);
			$object -> setRequirementType($reqType);
			$object -> setCreatedAt(new \DateTime());
			$object -> setCreatedBy($userData["id"]);
			$object -> setOrganization($userInfo->getOrganization());			
			$em = $this -> getDoctrine() -> getManager();				
			$em -> persist($object);
			$em -> flush();
			
			if($object->getStepRequirementId())
			{
				$this -> addFlash('success_message', $this -> getParameter('exito'));
			} else {
				$this -> addFlash('error_message', $this -> getParameter('error_form'));
			}
			
			
		}
		
		if($srid)
		{
			return $this -> redirectToRoute("backend_step_req_edit",array('pid'=>$pid,'fid'=>$fid,'id'=>$srid,'sid'=>$sid));
		} else {	
			return $this -> redirectToRoute("backend_step",array('pid'=>$pid,'fid'=>$fid));
		}
		
		
	}



	/**
	 * @Route("/backend/step/position_select", name="backend_step_position_select")
	 */
	public function selectAction(Request $request) {
			
		$this->get ( "session" )->set ( "module_id",$this->moduleId);		
		$this->get ( "session" )->set ( "module_name",$this->moduleName);
		$userData = $this->get ( "session" )->get ( "userData" );
		
		$departmentId = $request->get("deptId");		
				
		$userInfo = $this -> getDoctrine() -> getRepository('AppBundle:User') -> findOneByUserId($userData['id']);
		$positions   = $this -> getDoctrine() -> getRepository('AppBundle:Position') -> findBy(
			array(
				"Organization"=>$userInfo->getOrganization()->getOrganizationId(),
				"Department"=>$departmentId
			));
		
		return $this->render ( '@App/Backend/Step/position_filter.html.twig', array (						    
		    'positions' => $positions,
		    'edit'=>$request->get('edit'),
			'selectedId'=>$request->get('selectedId')
		));
	}





	/**
	 * @Route("/backend/step/flow/{fid}/process/{pid}/edit/{id}", name="backend_step_edit"), requirements={"id": "\w+"})
	 */
	public function editAction(Request $request, $fid, $pid) {
			
		$md5 = $request -> get("id");
		$objectId = $this -> getDoctrine() -> getRepository('AppBundle:Step') -> findOneByMd5Id($md5);
		$object = $this -> getDoctrine() -> getRepository('AppBundle:Step') -> findOneBy(array("stepId" => $objectId));

		if ($object) {

			$form = $this -> createForm(new StepType(), $object);
			$form -> handleRequest($request);

			if ($form -> isSubmitted()) {
				if ($form -> isValid()) {

					$userData = $this -> get("session") -> get("userData");

					$em = $this -> getDoctrine() -> getManager();
					$em -> persist($object);
					$em -> flush();
					$this -> addFlash('success_message', $this -> getParameter('exito_actualizar'));					
				} else {
					return $this -> render('@App/Backend/Step/edit.html.twig', 
						array(
							"form" => $form -> createView(),
							'pid'=>$pid,
							'fid'=>$fid
						));
				}
			} else {
				return $this -> render('@App/Backend/Step/edit.html.twig', 
					array(
						"form" => $form -> createView(),
						'pid'=>$pid,
						'fid'=>$fid
					));
					
			}
		} else {
			$this -> addFlash('error_message', $this -> getParameter('error_editar'));
			
		}
		
		
		return $this -> render('@App/Backend/Step/edit.html.twig', 
			array(
				"form" => $form -> createView(),
				'pid'=>$pid,
				'fid'=>$fid
			));
		
	}




	/**
	 * @Route("/backend/step_req/flow/{fid}/process/{pid}/step/{sid}/edit/{id}", name="backend_step_req_edit"), requirements={"id": "\w+"})
	 */
	public function editReqAction(Request $request, $fid, $pid, $sid,$id) {
			
		$md5 = $request -> get("id");
		
		$objectId = $this -> getDoctrine() -> getRepository('AppBundle:Step') -> findOneReqByMd5Id($md5);
		
		$object = $this -> getDoctrine() -> getRepository('AppBundle:StepRequirement') -> findOneBy(array("stepRequirementId" => $objectId['step_requirement_id']));

		$userData = $this->get ( "session" )->get ( "userData" );				
		$userInfo = $this -> getDoctrine() -> getRepository('AppBundle:User') -> findOneByUserId($userData['id']);

		$departments = $this -> getDoctrine() -> getRepository('AppBundle:Department') -> getList($userData, $userInfo->getOrganization()->getOrganizationId());
		$reqTypes    = $this -> getDoctrine() -> getRepository('AppBundle:RequirementType') -> findAll();
		$positions   = $this -> getDoctrine() -> getRepository('AppBundle:Position') -> findBy(array("Organization"=>$userInfo->getOrganization()->getOrganizationId()));

		if ($object) {

			if ($request->get('req')) {
	
			} else {
				return $this -> render('@App/Backend/Step/edit_req.html.twig', 
					array(
						'sid'=>$sid,
						'pid'=>$pid,
						'fid'=>$fid,
						'reqTypes'=>$reqTypes,
						'departments'=>$departments,
						'object'=>$object,
						'id'=>$id
					));
					
			}
		} else {
			//$this -> addFlash('error_message', $this -> getParameter('error_editar'));
			
		}
		
		
		return $this -> render('@App/Backend/Step/edit_req.html.twig', 
			array(
				
				'pid'=>$pid,
				'fid'=>$fid,
				'sid'=>$sid,
				'reqTypes'=>$reqTypes,
				'departments'=>$departments,
				'object'=>$object,
				'id'=>$id
			));
		
	}


	/**
	 * @Route("/backend/step_req/flow/{fid}/process/{pid}/delete/{id}", name="backend_step_req_delete")
	 */
	public function deleteReqAction(Request $request,$fid,$pid) {
		$md5Id = $request -> get("id");
		$objectInfo = $this -> getDoctrine() -> getRepository('AppBundle:Step') -> findOneReqByMd5Id($md5Id);
		$object = $this -> getDoctrine() -> getRepository('AppBundle:StepRequirement') -> findOneBy(array("stepRequirementId" => $objectInfo['step_requirement_id']));
		if ($object) {
			
			$em = $this -> getDoctrine() -> getManager();
			// Eliminar
			$em -> remove($object);
			$em -> flush();

			$this -> addFlash('success_message', $this -> getParameter('exito_eliminar'));
		} else {
			$this -> addFlash('error_message', $this -> getParameter('error_eliminar'));
		}

		return $this -> redirectToRoute("backend_step",array('fid'=>$fid,'pid'=>$pid));
	}




	/**
	 * @Route("/backend/step/flow/{fid}/process/{pid}/delete/{id}", name="backend_step_delete")
	 */
	public function deleteAction(Request $request,$fid,$pid) {
		$md5Id = $request -> get("id");
		$objectInfo = $this -> getDoctrine() -> getRepository('AppBundle:Step') -> findOneByMd5Id($md5Id);
		
		$object = $this -> getDoctrine() -> getRepository('AppBundle:Step') -> findOneBy(array("stepId" => $objectInfo['step_id']));
		if ($object) {
			 
			$em = $this -> getDoctrine() -> getManager();
			// Eliminar
			$em -> remove($object);
			$em -> flush();

			$this -> addFlash('success_message', $this -> getParameter('exito_eliminar'));
		} else {
			$this -> addFlash('error_message', $this -> getParameter('error_eliminar'));
		}

		return $this -> redirectToRoute("backend_step",array('fid'=>$fid,'pid'=>$pid));
	}


}
