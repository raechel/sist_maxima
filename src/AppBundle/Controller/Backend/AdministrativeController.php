<?php

namespace AppBundle\Controller\Backend;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use AppBundle\Entity\Institute;
use AppBundle\Form\InstituteType;

use AppBundle\Entity\InstituteWorkingDay;
use AppBundle\Form\InstituteWorkingDayType;

use AppBundle\Entity\InstitutePlan;
use AppBundle\Form\InstitutePlanType;

use AppBundle\Entity\Grade;
use AppBundle\Form\GradeType;

use AppBundle\Entity\GradeSection;
use AppBundle\Form\GradeSectionType;

use AppBundle\Entity\InstituteCycle;
use AppBundle\Form\InstituteCycleType;

use AppBundle\Entity\Subject;
use AppBundle\Form\SubjectType;

use AppBundle\Entity\SubjectGradeSection;
use AppBundle\Form\SubjectGradeSectionType;

use AppBundle\Repository\Kaans;
 
/**
 * Administrative controller.
 */
class AdministrativeController extends Controller {

	private $moduleId = 3;

	/**
	 * @Route("/backend/institute", name="backend_institute")
	 */
	public function indexAction(Request $request) {
		$this -> get("session") -> set("module_id", 11);
		$object = new Institute();
		$form = $this -> createForm(new InstituteType(), $object);
		$form -> handleRequest($request);
		$userData = $this -> get("session") -> get("userData");

		// Validar formulario
		if ($form -> isSubmitted()) {
			if ($form -> isValid()) {

				// logo
				$file = $object -> getLogoPath();
				if ($file) {
					$fileName = md5(uniqid()) . '.' . $file -> guessExtension();
					$file -> move($this -> getParameter('upload_files_images'), $fileName);
					$object -> setLogoPath($fileName);
				}

				// save
				$object -> setCreatedAt(new \DateTime());
				$object -> setCreatedBy($userData["id"]);
				$em = $this -> getDoctrine() -> getManager();
				
				if (!$em->isOpen()) {
				    $em = $em->create($em->getConnection(),$em->getConfiguration());
				}
				$em -> persist($object);
				$em -> flush();

				$this -> addFlash('success_message', $this -> getParameter('exito'));
				return $this -> redirectToRoute("backend_institute");
			} else {
				$this -> addFlash('error_message', $this -> getParameter('error_form'));
			}
		}

		$query = $this -> getDoctrine() -> getRepository('AppBundle:Institute') -> findAll();

		$paginator = $this -> get('knp_paginator');

		$pagination = $paginator -> paginate($query, $request -> query -> getInt('page', 1), $this -> getParameter("number_of_rows"));
		$mp = Kaans::getModulePermission($this -> moduleId, $this -> get("session") -> get("userModules"));

		return $this -> render('@App/Backend/Institute/index.html.twig', array(
			"form" => $form -> createView(), 
			"list" => $pagination, 
			"action" => "backend_institute", 
			"permits" => $mp
		));
	}

	#---------------------- INSTITUTE PLAN ---------------------- #

	/**
	 * @Route("/backend/institute/plan/{id}", name="backend_institute_plan"), requirements={"id": "\w+"})
	 */
	public function institutePlanAction(Request $request, $id) {

		$md5 = $request -> get("id");

		$objectId = $this -> getDoctrine() -> getRepository('AppBundle:Institute') -> findOneByMd5Id($md5);
		$objectList = $this -> getDoctrine() -> getRepository('AppBundle:InstitutePlan') -> findByInstitute(array("instituteId" => $objectId['institute_id']));

		$object = new InstitutePlan();
		$form = $this -> createForm(new InstitutePlanType(), $object);
		$form -> handleRequest($request);

		if ($form -> isSubmitted()) {
			if ($form -> isValid()) {

				$instituteEntity = $this -> getDoctrine() -> getRepository('AppBundle:Institute') -> findOneByInstituteId($objectId['institute_id']);
				$object -> setInstitute($instituteEntity);

				$em = $this -> getDoctrine() -> getManager();
				$em -> persist($object);
				$em -> flush();

				$this -> addFlash('success_message', $this -> getParameter('exito'));
				return $this -> redirectToRoute("backend_institute_plan", array('id' => md5($objectId['institute_id'])));
			} else {
				$this -> addFlash('error_message', $this -> getParameter('error_form'));
			}
		}

		$mp = Kaans::getModulePermission($this -> moduleId, $this -> get("session") -> get("userModules"));

		return $this -> render('@App/Backend/Institute/plan.html.twig', array('list' => $objectList, "form" => $form -> createView(), "instituteId" => $objectId['institute_id'], "permits" => $mp));
	}

	/**
	 * @Route("/backend/institute/plan/delete/{id}", name="backend_institute_plan_delete")
	 */
	public function deletePlanAction(Request $request) {
		$md5Id = $request -> get("id");
		$institutePlan = $this -> getDoctrine() -> getRepository('AppBundle:Institute') -> findInstitutePlanByMd5Id($md5Id);

		$object = $this -> getDoctrine() -> getRepository('AppBundle:InstitutePlan') -> findOneBy(array("institutePlanId" => $institutePlan['institute_plan_id']));

		if ($object) {

			//@TODO ELIMINAR SOLO LOS INSTITUTOS QUE NO TENGAN NINGUNA DEPENDENCIA,
			//SI YA EXISTE UNA DEPENDENCIA, CAMBIAR DE ESTATUS

			$em = $this -> getDoctrine() -> getManager();
			// Eliminar
			$em -> remove($object);
			$em -> flush();

			$this -> addFlash('success_message', $this -> getParameter('exito_eliminar'));
		} else {
			$this -> addFlash('error_message', $this -> getParameter('error_eliminar'));
		}

		return $this -> redirectToRoute("backend_institute_plan", array('id' => md5($object -> getInstitute() -> getInstituteId())));
	}

	#---------------------- INSTITUTE PLAN ---------------------- #
	#---------------------- INSTITUTE WORKING ---------------------- #

	/**
	 * @Route("/backend/institute/working/{id}", name="backend_institute_working"), requirements={"id": "\w+"})
	 */
	public function instituteWorkingAction(Request $request, $id) {

		$md5 = $request -> get("id");

		$objectId = $this -> getDoctrine() -> getRepository('AppBundle:Institute') -> findOneByMd5Id($md5);
		$objectList = $this -> getDoctrine() -> getRepository('AppBundle:InstituteWorkingDay') -> findByInstitute(array("instituteId" => $objectId['institute_id']));

		$instituteEntity = $this -> getDoctrine() -> getRepository('AppBundle:Institute') -> findOneByInstituteId($objectId['institute_id']);	
		$workings = $this -> getDoctrine() -> getRepository('AppBundle:InstitutePlan') -> findBy(array("institute" => $instituteEntity));

		$object = new InstituteWorkingDay();
		$form = $this -> createForm(new InstituteWorkingDayType(), $object);
		$form -> handleRequest($request);

		if ($form -> isSubmitted()) {
			if ($form -> isValid()) {

				
				$object -> setInstitute($instituteEntity);

				$em = $this -> getDoctrine() -> getManager();
				$em -> persist($object);
				$em -> flush();

				$this -> addFlash('success_message', $this -> getParameter('exito'));
				return $this -> redirectToRoute("backend_institute_working", array('id' => md5($objectId['institute_id'])));
			} else {
				$this -> addFlash('error_message', $this -> getParameter('error_form'));
			}
		}

		$mp = Kaans::getModulePermission($this -> moduleId, $this -> get("session") -> get("userModules"));

		return $this -> render('@App/Backend/Institute/working.html.twig', array('list' => $objectList, "form" => $form -> createView(), "instituteId" => $objectId['institute_id'],'workings'=>$workings, "permits" => $mp));
	}

	/**
	 * @Route("/backend/institute/working/delete/{id}", name="backend_institute_working_delete")
	 */
	public function deleteWorkingAction(Request $request) {
		$md5Id = $request -> get("id");
		$instituteWorking = $this -> getDoctrine() -> getRepository('AppBundle:Institute') -> findInstituteWorkingByMd5Id($md5Id);

		$object = $this -> getDoctrine() -> getRepository('AppBundle:InstituteWorkingDay') -> findOneBy(array("instituteWorkingDayId" => $instituteWorking['institute_working_day_id']));

		if ($object) {

			//@TODO ELIMINAR SOLO LOS INSTITUTOS QUE NO TENGAN NINGUNA DEPENDENCIA,
			//SI YA EXISTE UNA DEPENDENCIA, CAMBIAR DE ESTATUS

			$em = $this -> getDoctrine() -> getManager();
			// Eliminar
			$em -> remove($object);
			$em -> flush();

			$this -> addFlash('success_message', $this -> getParameter('exito_eliminar'));
		} else {
			$this -> addFlash('error_message', $this -> getParameter('error_eliminar'));
		}

		return $this -> redirectToRoute("backend_institute_working", array('id' => md5($object -> getInstitute() -> getInstituteId())));
	}

	#---------------------- FINAL INSTITUTE WORKING ---------------------- #
	#---------------------- START WORKING - PLAN void ---------------------- #

	/**
	 * @Route("/backend/institute/plan_working/{id}/type/{type}", name="backend_institute_plan_working"), requirements={"id": "\w+"})
	 */
	public function dynamicPlanWorkingAction(Request $request) {

		$type = $request -> get('type');
		$id = $request -> get('id');

		$object = $this -> getDoctrine() -> getRepository('AppBundle:Institute') -> findOneByInstituteId($id);

		switch($type) {
			//Plan
			case 1 :
				$objectAll = $this -> getDoctrine() -> getRepository('AppBundle:InstitutePlan') -> findBy(array("institute" => $object));
				break;
			//Working
			case 2 :
				$objectAll = $this -> getDoctrine() -> getRepository('AppBundle:InstituteWorkingDay') -> findBy(array("institute" => $object));
				break;
		}

		return $this -> render('@App/Backend/Institute/working_plan.html.twig', array('options' => $objectAll, 'type' => $type));
	}

	#---------------------- FINAL WORKING - PLAN void ---------------------- #

	/**
	 * @Route("/backend/institute/edit/{id}", name="backend_institute_edit"), requirements={"id": "\w+"})
	 */
	public function editAction(Request $request) {
		$md5 = $request -> get("id");

		$objectId = $this -> getDoctrine() -> getRepository('AppBundle:Institute') -> findOneByMd5Id($md5);
		$object = $this -> getDoctrine() -> getRepository('AppBundle:Institute') -> findOneBy(array("instituteId" => $objectId));

		if ($object) {

			$oldImage = $object -> getLogoPath();

			$form = $this -> createForm(new InstituteType(), $object);
			$form -> handleRequest($request);

			if ($form -> isSubmitted()) {
				if ($form -> isValid()) {

					$userData = $this -> get("session") -> get("userData");

					// logo
					$file = $object -> getLogoPath();
					if ($file) {
						$fileName = md5(uniqid()) . '.' . $file -> guessExtension();
						$file -> move($this -> getParameter('upload_files_images'), $fileName);
						$object -> setLogoPath($fileName);
					} else {
						$object -> setLogoPath($oldImage);
					}

					/*$object->setUpdatedAt ( new \DateTime () );
					 $object->setUpdatedBy ($userData["id"]);*/
					$em = $this -> getDoctrine() -> getManager();
					$em -> persist($object);
					$em -> flush();
					$this -> addFlash('success_message', $this -> getParameter('exito_actualizar'));
				} else {
					return $this -> render('@App/Backend/Institute/edit.html.twig', array("form" => $form -> createView()));
				}
			} else {
				return $this -> render('@App/Backend/Institute/edit.html.twig', array("form" => $form -> createView()));
			}
		} else {
			$this -> addFlash('error_message', $this -> getParameter('error_editar'));
		}
		return $this -> redirectToRoute("backend_institute");
	}

	/**
	 * @Route("/backend/institute/delete/{id}", name="backend_institute_delete")
	 */
	public function deleteAction(Request $request) {
		$md5Id = $request -> get("id");
		$instituteId = $this -> getDoctrine() -> getRepository('AppBundle:Institute') -> findOneByMd5Id($md5Id);
		$object = $this -> getDoctrine() -> getRepository('AppBundle:Institute') -> findOneBy(array("instituteId" => $instituteId));
		if ($object) {

			//@TODO ELIMINAR SOLO LOS INSTITUTOS QUE NO TENGAN NINGUNA DEPENDENCIA,
			//SI YA EXISTE UNA DEPENDENCIA, CAMBIAR DE ESTATUS

			$em = $this -> getDoctrine() -> getManager();
			// Eliminar
			$em -> remove($object);
			$em -> flush();

			$this -> addFlash('success_message', $this -> getParameter('exito_eliminar'));
		} else {
			$this -> addFlash('error_message', $this -> getParameter('error_eliminar'));
		}

		return $this -> redirectToRoute("backend_institute");
	}

	//======== GRADE ========//

	/**
	 * @Route("/backend/grade", name="backend_grade")
	 */
	public function indexGradeAction(Request $request) {
		$this -> get("session") -> set("module_id", 11);
		$object = new Grade();
		$form = $this -> createForm(new GradeType(), $object);
		$form -> handleRequest($request);
		$userData = $this -> get("session") -> get("userData");

		// Validar formulario
		if ($form -> isSubmitted()) {
			if ($form -> isValid()) {
				
				$values = $request->get('appbundle_egrade');
				
				// save
				$user = $this->getDoctrine()->getRepository('AppBundle:User')->findOneBy(array('userId'=>$userData["id"]));
				$object -> setCreatedBy($user);
				
				$workingDay = $this->getDoctrine()->getRepository('AppBundle:workingDay')->findOneBy(array('workingDayId'=>$values['workingDay']));
				$object -> setWorkingDay($workingDay);
				
				$plan = $this->getDoctrine()->getRepository('AppBundle:Plan')->findOneBy(array('planId'=>$values['plan']));				
				$object -> setPlan($plan);
				
				
				$em = $this -> getDoctrine() -> getManager();
				$em -> persist($object);
				$em -> flush();

				$this -> addFlash('success_message', $this -> getParameter('exito'));
				return $this -> redirectToRoute("backend_grade");
			} else {
				$this -> addFlash('error_message', $this -> getParameter('error_form'));
			}
		}

		$query = $this -> getDoctrine() -> getRepository('AppBundle:Grade') -> findAll();
		$paginator = $this -> get('knp_paginator');

		$pagination = $paginator -> paginate($query, $request -> query -> getInt('page', 1), $this -> getParameter("number_of_rows"));
		$mp = Kaans::getModulePermission($this -> moduleId, $this -> get("session") -> get("userModules"));

		return $this -> render('@App/Backend/Grade/index.html.twig', array("form" => $form -> createView(), "list" => $pagination, "action" => "backend_grade", "permits" => $mp));
	}

	/**
	 * @Route("/backend/grade/edit/{id}", name="backend_grade_edit"), requirements={"id": "\w+"})
	 */
	public function editGradeAction(Request $request) {
		$md5 = $request -> get("id");

		$objectId = $this -> getDoctrine() -> getRepository('AppBundle:Institute') -> findOneGradeByMd5Id($md5);

		$object = $this -> getDoctrine() -> getRepository('AppBundle:Grade') -> findOneBy(array("gradeId" => $objectId['grade_id']));

		if ($object) {

			$form = $this -> createForm(new GradeType(), $object);
			$form -> handleRequest($request);

			if ($form -> isSubmitted()) {
				if ($form -> isValid()) {

					$userData = $this -> get("session") -> get("userData");

					$em = $this -> getDoctrine() -> getManager();
					$em -> persist($object);
					$em -> flush();
					$this -> addFlash('success_message', $this -> getParameter('exito_actualizar'));
				} else {
					return $this -> render('@App/Backend/Grade/edit.html.twig', array("form" => $form -> createView()));
				}
			} else {

				return $this -> render('@App/Backend/Grade/edit.html.twig', array("form" => $form -> createView()));
			}
		} else {
			$this -> addFlash('error_message', $this -> getParameter('error_editar'));
		}
		return $this -> redirectToRoute("backend_grade");
	}

	/**
	 * @Route("/backend/grade/delete/{id}", name="backend_grade_delete")
	 */
	public function deleteGradeAction(Request $request) {

		$md5Id = $request -> get("id");
		$instituteId = $this -> getDoctrine() -> getRepository('AppBundle:Institute') -> findOneGradeByMd5Id($md5Id);
		$object = $this -> getDoctrine() -> getRepository('AppBundle:Grade') -> findOneBy(array("gradeId" => $instituteId['grade_id']));
		if ($object) {

			//@TODO ELIMINAR SOLO LOS INSTITUTOS QUE NO TENGAN NINGUNA DEPENDENCIA,
			//SI YA EXISTE UNA DEPENDENCIA, CAMBIAR DE ESTATUS

			$em = $this -> getDoctrine() -> getManager();
			// Eliminar
			$em -> remove($object);
			$em -> flush();

			$this -> addFlash('success_message', $this -> getParameter('exito_eliminar'));
		} else {
			$this -> addFlash('error_message', $this -> getParameter('error_eliminar'));
		}

		return $this -> redirectToRoute("backend_grade");
	}

	//======== GRADE SECTION ========//

	/**
	 * @Route("/backend/gradesection", name="backend_gradesection")
	 */
	public function indexGradeSectionAction(Request $request) {
		$this -> get("session") -> set("module_id", 11);
		$object = new GradeSection();
		$form = $this -> createForm(new GradeSectionType(), $object);
		$form -> handleRequest($request);
		$userData = $this -> get("session") -> get("userData");

		// Validar formulario
		if ($form -> isSubmitted()) {
			if ($form -> isValid()) {

				// save
				$user = $this->getDoctrine()->getRepository('AppBundle:User')->findOneBy(array('userId'=>$userData["id"]));
				$object -> setCreatedBy($user);
				$em = $this -> getDoctrine() -> getManager();
				$em -> persist($object);
				$em -> flush();

				$this -> addFlash('success_message', $this -> getParameter('exito'));
				return $this -> redirectToRoute("backend_gradesection");
			} else {
				$this -> addFlash('error_message', $this -> getParameter('error_form'));
			}
		}

		$query = $this -> getDoctrine() -> getRepository('AppBundle:GradeSection') -> findAll();
		$paginator = $this -> get('knp_paginator');

		$pagination = $paginator -> paginate($query, $request -> query -> getInt('page', 1), $this -> getParameter("number_of_rows"));
		$mp = Kaans::getModulePermission($this -> moduleId, $this -> get("session") -> get("userModules"));

		return $this -> render('@App/Backend/GradeSection/index.html.twig', array("form" => $form -> createView(), "list" => $pagination, "action" => "backend_gradesection", "permits" => $mp));
	}

	/**
	 * @Route("/backend/gradesection/edit/{id}", name="backend_gradesection_edit"), requirements={"id": "\w+"})
	 */
	public function editGradeSectionAction(Request $request) {
		$md5 = $request -> get("id");

		$objectId = $this -> getDoctrine() -> getRepository('AppBundle:Institute') -> findOneGradeSectionByMd5Id($md5);

		$object = $this -> getDoctrine() -> getRepository('AppBundle:GradeSection') -> findOneBy(array("gradeSectionId" => $objectId['grade_section_id']));

		if ($object) {

			$form = $this -> createForm(new GradeSectionType(), $object);
			$form -> handleRequest($request);

			if ($form -> isSubmitted()) {
				if ($form -> isValid()) {

					$userData = $this -> get("session") -> get("userData");

					$em = $this -> getDoctrine() -> getManager();
					$em -> persist($object);
					$em -> flush();
					$this -> addFlash('success_message', $this -> getParameter('exito_actualizar'));
				} else {
					return $this -> render('@App/Backend/GradeSection/edit.html.twig', array("form" => $form -> createView()));
				}
			} else {

				return $this -> render('@App/Backend/GradeSection/edit.html.twig', array("form" => $form -> createView()));
			}
		} else {
			$this -> addFlash('error_message', $this -> getParameter('error_editar'));
		}
		return $this -> redirectToRoute("backend_gradesection");
	}

	/**
	 * @Route("/backend/gradesection/delete/{id}", name="backend_gradesection_delete")
	 */
	public function deleteGradeSectionAction(Request $request) {

		$md5Id = $request -> get("id");
		$instituteId = $this -> getDoctrine() -> getRepository('AppBundle:Institute') -> findOneGradeSectionByMd5Id($md5Id);
		$object = $this -> getDoctrine() -> getRepository('AppBundle:GradeSection') -> findOneBy(array("gradeSectionId" => $instituteId['grade_section_id']));
		if ($object) {

			//@TODO ELIMINAR SOLO LOS INSTITUTOS QUE NO TENGAN NINGUNA DEPENDENCIA,
			//SI YA EXISTE UNA DEPENDENCIA, CAMBIAR DE ESTATUS

			$em = $this -> getDoctrine() -> getManager();
			// Eliminar
			$em -> remove($object);
			$em -> flush();

			$this -> addFlash('success_message', $this -> getParameter('exito_eliminar'));
		} else {
			$this -> addFlash('error_message', $this -> getParameter('error_eliminar'));
		}

		return $this -> redirectToRoute("backend_gradesection");
	}

	//======== SUBJECTS ========//

	/**
	 * @Route("/backend/subject", name="backend_subject")
	 */
	public function indexSubjectAction(Request $request) {
		$this -> get("session") -> set("module_id", 11);
		$object = new Subject();
		$form = $this -> createForm(new SubjectType(), $object);
		$form -> handleRequest($request);
		$userData = $this -> get("session") -> get("userData");

		// Validar formulario
		if ($form -> isSubmitted()) {
			if ($form -> isValid()) {

				// save
				$user = $this -> getDoctrine() -> getRepository('AppBundle:User') -> findOneByUserId($userData["id"]);
				$object -> setCreatedBy($user);
				$em = $this -> getDoctrine() -> getManager();
				$em -> persist($object);
				$em -> flush();

				$this -> addFlash('success_message', $this -> getParameter('exito'));
				return $this -> redirectToRoute("backend_subject");
			} else {
				$this -> addFlash('error_message', $this -> getParameter('error_form'));
			}
		}

		$query = $this -> getDoctrine() -> getRepository('AppBundle:Subject') -> findAll();
		$paginator = $this -> get('knp_paginator');

		$pagination = $paginator -> paginate($query, $request -> query -> getInt('page', 1), $this -> getParameter("number_of_rows"));
		$mp = Kaans::getModulePermission($this -> moduleId, $this -> get("session") -> get("userModules"));

		return $this -> render('@App/Backend/Subject/index.html.twig', array("form" => $form -> createView(), "list" => $pagination, "action" => "backend_subject", "permits" => $mp));
	}

	/**
	 * @Route("/backend/subject/edit/{id}", name="backend_subject_edit"), requirements={"id": "\w+"})
	 */
	public function editSubjectAction(Request $request) {
		$md5 = $request -> get("id");

		$objectId = $this -> getDoctrine() -> getRepository('AppBundle:Institute') -> findOneSubjectByMd5Id($md5);

		$object = $this -> getDoctrine() -> getRepository('AppBundle:Subject') -> findOneBy(array("subjectId" => $objectId['subject_id']));

		if ($object) {

			$form = $this -> createForm(new SubjectType(), $object);
			$form -> handleRequest($request);

			if ($form -> isSubmitted()) {
				if ($form -> isValid()) {

					$userData = $this -> get("session") -> get("userData");

					$em = $this -> getDoctrine() -> getManager();
					$em -> persist($object);
					$em -> flush();
					$this -> addFlash('success_message', $this -> getParameter('exito_actualizar'));
				} else {
					return $this -> render('@App/Backend/Subject/edit.html.twig', array("form" => $form -> createView()));
				}
			} else {

				return $this -> render('@App/Backend/Subject/edit.html.twig', array("form" => $form -> createView()));
			}
		} else {
			$this -> addFlash('error_message', $this -> getParameter('error_editar'));
		}
		return $this -> redirectToRoute("backend_subject");
	}

	/**
	 * @Route("/backend/subject/delete/{id}", name="backend_subject_delete")
	 */
	public function deleteSubjectAction(Request $request) {

		$md5Id = $request -> get("id");
		$instituteId = $this -> getDoctrine() -> getRepository('AppBundle:Institute') -> findOneSubjectByMd5Id($md5Id);
		$object = $this -> getDoctrine() -> getRepository('AppBundle:Subject') -> findOneBy(array("subjectId" => $instituteId['subject_id']));
		if ($object) {

			//@TODO ELIMINAR SOLO LOS INSTITUTOS QUE NO TENGAN NINGUNA DEPENDENCIA,
			//SI YA EXISTE UNA DEPENDENCIA, CAMBIAR DE ESTATUS

			$em = $this -> getDoctrine() -> getManager();
			// Eliminar
			$em -> remove($object);
			$em -> flush();

			$this -> addFlash('success_message', $this -> getParameter('exito_eliminar'));
		} else {
			$this -> addFlash('error_message', $this -> getParameter('error_eliminar'));
		}

		return $this -> redirectToRoute("backend_subject");
	}

	//======== SUBJECTS GRADE SECTIONS ========//

	/**
	 * @Route("/backend/dynamic/teacher/{id}", name="backend_dynamic_teacher", defaults={"id":"0"})
	 */
	public function dynamicTeacher($id) {

		//ROL DE MAESTROS
		$roleId = 3;

		$gradeSection = $this -> getDoctrine() -> getRepository('AppBundle:GradeSection') -> findOneBy(array('gradeSectionId' => $id));

		$teachers = $this -> getDoctrine() -> getRepository('AppBundle:User') -> findBy(array('userRole' => $roleId, 'institute' => $gradeSection -> getGrade() -> getInstitute() -> getInstituteId()));

		return $this -> render('@App/Backend/SubjectGradeSection/teachers.html.twig', array("teachers" => $teachers));

	}

	/**
	 * @Route("/backend/subject/grade", name="backend_subject_grade")
	 */
	public function indexSubjectGradeAction(Request $request) {
		$this -> get("session") -> set("module_id", 11);
		$object = new SubjectGradeSection();
		$form = $this -> createForm(new SubjectGradeSectionType(), $object);
		$form -> handleRequest($request);
		$userData = $this -> get("session") -> get("userData");

		// Validar formulario
		if ($form -> isSubmitted()) {
			if ($form -> isValid()) {

				// save
				$object -> setCreatedBy($userData["id"]);
				$em = $this -> getDoctrine() -> getManager();
				$em -> persist($object);
				$em -> flush();

				$this -> addFlash('success_message', $this -> getParameter('exito'));
				return $this -> redirectToRoute("backend_subject_grade");
			} else {
				$this -> addFlash('error_message', $this -> getParameter('error_form'));
			}
		}

		$query = $this -> getDoctrine() -> getRepository('AppBundle:SubjectGradeSection') -> findAll();
		$paginator = $this -> get('knp_paginator');

		$pagination = $paginator -> paginate($query, $request -> query -> getInt('page', 1), $this -> getParameter("number_of_rows"));
		$mp = Kaans::getModulePermission($this -> moduleId, $this -> get("session") -> get("userModules"));

		return $this -> render('@App/Backend/SubjectGradeSection/index.html.twig', array("form" => $form -> createView(), "list" => $pagination, "action" => "backend_subject_grade", "permits" => $mp));
	}

	/**
	 * @Route("/backend/subject/grade/edit/{id}", name="backend_subject_grade_edit"), requirements={"id": "\w+"})
	 */
	public function editSubjectGradeAction(Request $request) {
		$md5 = $request -> get("id");

		$objectId = $this -> getDoctrine() -> getRepository('AppBundle:Institute') -> findOneSubjectGradeByMd5Id($md5);

		$object = $this -> getDoctrine() -> getRepository('AppBundle:SubjectGradeSection') -> findOneBy(array("subjectGradeSectionId" => $objectId['subject_grade_section_id']));

		if ($object) {

			$form = $this -> createForm(new SubjectGradeSectionType(), $object);
			$form -> handleRequest($request);

			if ($form -> isSubmitted()) {
				if ($form -> isValid()) {

					$userData = $this -> get("session") -> get("userData");

					$em = $this -> getDoctrine() -> getManager();
					$em -> persist($object);
					$em -> flush();
					$this -> addFlash('success_message', $this -> getParameter('exito_actualizar'));
				} else {
					return $this -> render('@App/Backend/SubjectGradeSection/edit.html.twig', array("form" => $form -> createView()));
				}
			} else {

				return $this -> render('@App/Backend/SubjectGradeSection/edit.html.twig', array("form" => $form -> createView()));
			}
		} else {
			$this -> addFlash('error_message', $this -> getParameter('error_editar'));
		}
		return $this -> redirectToRoute("backend_subject_grade");
	}

	/**
	 * @Route("/backend/subject/grade/delete/{id}", name="backend_subject_grade_delete")
	 */
	public function deleteSubjectGradeAction(Request $request) {

		$md5Id = $request -> get("id");
		$instituteId = $this -> getDoctrine() -> getRepository('AppBundle:Institute') -> findOneSubjectGradeByMd5Id($md5Id);
		$object = $this -> getDoctrine() -> getRepository('AppBundle:SubjectGradeSection') -> findOneBy(array("subjectGradeSectionId" => $instituteId['subject_grade_section_id']));
		if ($object) {

			//@TODO ELIMINAR SOLO LOS INSTITUTOS QUE NO TENGAN NINGUNA DEPENDENCIA,
			//SI YA EXISTE UNA DEPENDENCIA, CAMBIAR DE ESTATUS

			$em = $this -> getDoctrine() -> getManager();
			// Eliminar
			$em -> remove($object);
			$em -> flush();

			$this -> addFlash('success_message', $this -> getParameter('exito_eliminar'));
		} else {
			$this -> addFlash('error_message', $this -> getParameter('error_eliminar'));
		}

		return $this -> redirectToRoute("backend_subject_grade");
	}
	
	
	
	/**
	 * @Route("/backend/institute/cycle", name="backend_institute_cycle")
	 */
	public function indexCycleAction(Request $request) {
		$this -> get("session") -> set("module_id", 14);
		$userData = $this -> get("session") -> get("userData");
		
		$object = new InstituteCycle();
		$form = $this -> createForm(new InstituteCycleType(), $object);
		$form -> handleRequest($request);
		
		// Validar formulario
		if ($form -> isSubmitted()) {
			if ($form -> isValid()) {

				// save
				$object -> setCreatedBy($userData["id"]);
				$object -> setCreatedAt(new \DateTime());				
				$em = $this -> getDoctrine() -> getManager();
				$em -> persist($object);
				$em -> flush();

				$this -> addFlash('success_message', $this -> getParameter('exito'));
				return $this -> redirectToRoute("backend_institute_cycle");
			} else {
				$this -> addFlash('error_message', $this -> getParameter('error_form'));
			}
		}

		$query = $this -> getDoctrine() -> getRepository('AppBundle:InstituteCycle') -> findAll();		
		$mp = Kaans::getModulePermission(14, $this -> get("session") -> get("userModules"));

		return $this -> render('@App/Backend/Cycle/index.html.twig', array(
			"form" => $form -> createView(), 
			"list" => $query, 
			"action" => "backend_institute_cycle", 
			"permits" => $mp
		));
	}


	/**
	 * @Route("/backend/institute/cycle/edit/{id}", name="backend_institute_cycle_edit"), requirements={"id": "\w+"})
	 */
	public function editCycleAction(Request $request) {
	
		$md5 = $request -> get("id");
		$userData = $this -> get("session") -> get("userData");

		$objectId = $this -> getDoctrine() -> getRepository('AppBundle:Institute') -> findCycleByMd5Id($md5);
		$object = $this -> getDoctrine() -> getRepository('AppBundle:InstituteCycle') -> findOneBy(array("instituteCycleId" => $objectId['institute_cycle_id']));

		if ($object) {

			$form = $this -> createForm(new InstituteCycleType(), $object);
			$form -> handleRequest($request);

			if ($form -> isSubmitted()) {
				if ($form -> isValid()) {

					$em = $this -> getDoctrine() -> getManager();
					$em -> persist($object);
					$em -> flush();
					$this -> addFlash('success_message', $this -> getParameter('exito_actualizar'));
				} else {
					return $this -> render('@App/Backend/Cycle/edit.html.twig', array("form" => $form -> createView()));
				}
			} else {

				return $this -> render('@App/Backend/Cycle/edit.html.twig', array("form" => $form -> createView()));
			}
		} else {
			$this -> addFlash('error_message', $this -> getParameter('error_editar'));
		}
		return $this -> redirectToRoute("backend_institute_cycle");
	}


	/**
	 * @Route("/backend/institute/cycle/delete/{id}", name="backend_institute_cycle_delete")
	 */
	public function deleteCycleAction(Request $request) {

		$md5Id = $request -> get("id");
		$instituteId = $this -> getDoctrine() -> getRepository('AppBundle:Institute') -> findCycleByMd5Id($md5Id);
		$object = $this -> getDoctrine() -> getRepository('AppBundle:InstituteCycle') -> findOneBy(array("instituteCYcleId" => $instituteId['institute_cycle_id']));
		if ($object) {

			//@TODO ELIMINAR SOLO LOS INSTITUTOS QUE NO TENGAN NINGUNA DEPENDENCIA,
			//SI YA EXISTE UNA DEPENDENCIA, CAMBIAR DE ESTATUS

			$em = $this -> getDoctrine() -> getManager();
			// Eliminar
			$em -> remove($object);
			$em -> flush();

			$this -> addFlash('success_message', $this -> getParameter('exito_eliminar'));
		} else {
			$this -> addFlash('error_message', $this -> getParameter('error_eliminar'));
		}

		return $this -> redirectToRoute("backend_institute_cycle");
	}
	
	

}
