<?php

namespace AppBundle\Controller\Backend;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use AppBundle\Entity\Agenda;
use AppBundle\Form\AgendaType;

use AppBundle\Repository\Kaans;
use AppBundle\Helper\UrlHelper;

/**
 * Board controller.
 */
class StartProcessController extends Controller {
	
	private $moduleId = 9;
	private $moduleName = "Iniciar Proceso";


	/**
	 * @Route("/backend/process/menu", name="backend_start_menu")
	 */
	public function indexAction(Request $request) {
		$this->get ( "session" )->set ( "module_id",$this->moduleId);
		$this->get ( "session" )->set ( "module_name",$this->moduleName);
		$userData = $this->get ( "session" )->get ( "userData" );
		
		$mp = Kaans::getModulePermission($this->moduleId, $this->get("session")->get("userModules"));
		
		$em = $this -> getDoctrine() -> getManager();	
		$thisUser = $em->getRepository ( 'AppBundle:User' )->findOneBy(array('userId'=>$userData['id']));
		$procs = $this -> getDoctrine() -> getRepository('AppBundle:ProcessList') -> processStarter($thisUser->getOrganization()->getOrganizationId(),$thisUser->getDepartment()->getDepartmentId(),true);
		$flows = $this -> getDoctrine() -> getRepository('AppBundle:ProcessList') -> processStarter($thisUser->getOrganization()->getOrganizationId(),$thisUser->getDepartment()->getDepartmentId());
		
		return $this->render ( '@App/Backend/StartProcess/index.html.twig', array (				
		    "permits" => $mp,
		    "procs" => $procs,
		    "flows" => $flows,
		    "thisUser" => $thisUser
		));
	}




	/**
	 * @Route("/backend/process/start/flow/{fid}", name="backend_start_process")
	 */
	public function startAction(Request $request,$fid) {
		$this->get ( "session" )->set ( "module_id",$this->moduleId);
		$this->get ( "session" )->set ( "module_name",$this->moduleName);
		$userData = $this->get ( "session" )->get ( "userData" );
		
		
		$objectId = $this -> getDoctrine() -> getRepository('AppBundle:Flow') -> findOneByMd5Id($fid);
		$object = $this -> getDoctrine() -> getRepository('AppBundle:Flow') -> findOneBy(array("flowId" => $objectId));
				
		$steps = $this -> getDoctrine() -> getRepository('AppBundle:Step') -> findBy(array("flow" => $objectId));
		
		$mp = Kaans::getModulePermission($this->moduleId, $this->get("session")->get("userModules"));
		
		$em = $this -> getDoctrine() -> getManager();	
		$thisUser = $em->getRepository ( 'AppBundle:User' )->findOneBy(array('userId'=>$userData['id']));
		$procs = $this -> getDoctrine() -> getRepository('AppBundle:ProcessList') -> processStarter($thisUser->getOrganization()->getOrganizationId(),true);
		$flows = $this -> getDoctrine() -> getRepository('AppBundle:ProcessList') -> processStarter($thisUser->getOrganization()->getOrganizationId());
			
		
			
		return $this->render ( '@App/Backend/StartProcess/start.html.twig', array (				
		    "permits" => $mp,
		    "procs" => $procs,
		    "thisUser" => $thisUser,
		    "flow"=>$object,
		    "steps"=>$steps
		));
	}

}
