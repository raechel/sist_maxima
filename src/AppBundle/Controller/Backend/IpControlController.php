<?php

namespace AppBundle\Controller\Backend;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use AppBundle\Entity\IpControl;
use AppBundle\Helper\UserHelper;
use AppBundle\Helper\UtilsHelper;
use AppBundle\Helper\UrlHelper;
use AppBundle\Helper\MailHelper;
use AppBundle\Repository\Kaans;
use Symfony\Component\HttpFoundation\JsonResponse;


class IpControlController extends Controller {
	
    
    private $moduleId = 14;
	private $moduleName = "Gestión de IPs";
	
    

    /**
     * @Route("/backend/ip_control", name="backend_ipcontrol")
     */
	public function indexAction(Request $request) {
		
		$this->get("session")->set("module_id", $this->moduleId);	
		$this->get ( "session" )->set ( "module_name",$this->moduleName);	
		$userData = $this->get ( "session" )->get ( "userData" );
		
		
		if($request->get('generate_block'))
		{
			$start = $request->get("from_4");
			$end   = $request->get("to_4");
			for($i=$start;$i<=$end;$i++)
			{
				if($i == 1)
				{
					continue;
				}
				
				$ip = $request->get("from_1").".".$request->get("from_2").".".$request->get("from_3").".".$i;
				$object = $this->getDoctrine()->getManager()->getRepository ( 'AppBundle:IpControl' )->findOneByIpNumber($ip);
				
				if(!$object)
				{
					
					$ipcontrol = new IpControl();
					$ipcontrol->setIpNumber($ip);
					$ipcontrol->setCreatedAt(new \DateTime);
					$ipcontrol->setCreatedBy($userData['id']);
					$em = $this->getDoctrine()->getManager();
					$em->persist($ipcontrol);
					$em->flush();
					
					
				}
			}
			
			$this->addFlash ( 'success_message', $this->getParameter ( 'exito_actualizar' ) );
			
		}
		
		
		
		$em = $this->getDoctrine()->getManager();
		$list = $em->getRepository ( 'AppBundle:IpControl' )->findAll();
		
		
		$mp = Kaans::getModulePermission($this->moduleId, $this->get("session")->get("userModules"));		
		
		return $this->render ( '@App/Backend/IpControl/index.html.twig', array (				
				"list" => $list,				
		        "permits" => $mp,		        		        		        
		        'userData'=>$userData		        
		) );
	}
	
	/**
	 * @Route("/backend/ip_control/edit/{id}", name="backend_ipcontrol_edit")
	 */
	public function editAction(Request $request) {
		
		$userData = $this->get ( "session" )->get ( "userData" );
		$md5 = $request->get ( "id" );
		
		$objectId = $this->getDoctrine()->getManager()->getRepository ( 'AppBundle:Client' )->findOneSupplyByMd5Id($md5);
		$object   = $this->getDoctrine()->getManager()->getRepository ( 'AppBundle:Supply' )->findOneBy(array('supplyId'=>$objectId['supply_id']));
		
		$thisUser = $this->getDoctrine()->getManager()->getRepository ( 'AppBundle:User' )->findOneBy(array('userId'=>$userData['id']));
		
		if ($object) {
					
			$form = $this->createForm ( new SupplyType (), $object );
			$form->handleRequest ( $request );
			if ($form->isSubmitted ()) {
				if ($form->isValid ()) {
									
					// Set updated By
					//$object->setCreatedBy($editor);
					//$object->setCreatedAt(new \DateTime());			
					$em = $this->getDoctrine()->getManager();
					$em->persist($object);
					$em->flush();
				
					$this->addFlash ( 'success_message', $this->getParameter ( 'exito_actualizar' ) );
				} else {
					// Error validation
					$this->addFlash ( 'error_message', $this->getParameter ( 'error_form' ) );
				}
			}

			$mp = Kaans::getModulePermission($this->moduleId, $this->get("session")->get("userModules"));
			
			return $this->render ( '@App/Backend/IpControl/edit.html.twig', array(
				"form" => $form->createView (),
				"edit" => true,
				'userInfo'=>$thisUser,
				'object'=>$object,					
				"permits" => $mp					
			) );
		} else {
			$this->addFlash ( 'error_message', $this->getParameter ( 'error_editar' ) );
		}
		return $this->redirectToRoute ( "backend_ipcontrol");
	}
	
	
	
	/* public function returnPDFResponseFromHTML($html,$user,$cycle_number){
        //set_time_limit(30); uncomment this line according to your needs
        // If you are not in a controller, retrieve of some way the service container and then retrieve it
        //$pdf = $this->container->get("white_october.tcpdf")->create('vertical', PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
        //if you are in a controlller use :
        $pdf = $this->get("white_october.tcpdf")->create('vertical', PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
        $pdf->SetAuthor('Kaans - Plataforma Educativa');
		
		$subject = 'reporte_académico_'.$user->getFirstName()."_".$user->getLastName()."_".$cycle_number;
        $pdf->SetTitle($subject);
        $pdf->SetSubject($subject);
		$pdf->SetHeaderData("", "", "", "Reporte Académico de ".$user->getFirstName()." ".$user->getLastName());
				
        $pdf->setFontSubsetting(true);
        $pdf->SetFont('helvetica', '', 10, '', true);
        $pdf->SetMargins(15,20,15, true);
        $pdf->AddPage();
        
        $filename = $subject;
        
        $pdf->writeHTMLCell($w = 0, $h = 0, $x = '', $y = '', $html, $border = 0, $ln = 1, $fill = 0, $reseth = true, $align = '', $autopadding = true);
        $pdf->Output($filename.".pdf",'I'); // This will output the PDF as a response directly
    }*/




	
	/**
	 * @Route("/backend/ip_control/delete/{id}", name="backend_ipcontrol_delete")
	 */
	public function deleteAction(Request $request) {
			
		$md5 = $request->get ( "id" );		
		$object   = $this->getDoctrine()->getManager()->getRepository ( 'AppBundle:IpControl' )->findOneByIpControlId($md5);
		
		if ($object) {
			// Can't delete yourself
			
			$em = $this->getDoctrine ()->getManager ();			
			$em->remove( $object );
			$em->flush ();
			
			$this->addFlash ( 'success_message', $this->getParameter ( 'exito_eliminar' ) );
			
		} else {
			$this->addFlash ( 'error_message', $this->getParameter ( 'error_eliminar' ) );
		}
		
		return $this->redirectToRoute ( "backend_ipcontrol");
	}



	
	public function base64_url_encode($input) {
	 return strtr(base64_encode($input), '+/=', '._-');
	}
}
