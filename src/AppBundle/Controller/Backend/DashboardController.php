<?php

namespace AppBundle\Controller\Backend;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use AppBundle\Entity\Agenda;
use AppBundle\Form\AgendaType;

use AppBundle\Repository\Kaans;
use AppBundle\Helper\UrlHelper;

/**
 * Board controller.
 */
class DashboardController extends Controller {
	
	private $moduleId = 1;
	private $moduleName = "Reporte de Montos";


	/**
	 * @Route("/backend/dashboard", name="backend_dashboard")
	 */
	public function indexAction(Request $request) {
		$this->get ( "session" )->set ( "module_id",$this->moduleId);
		$this->get ( "session" )->set ( "module_name",$this->moduleName);
		$userData = $this->get ( "session" )->get ( "userData" );
		
		$from = $request->get("from","");
		$to = $request->get("to","");
		
		$em = $this->getDoctrine()->getManager();
		
		$mp = Kaans::getModulePermission($this->moduleId, $this->get("session")->get("userModules"));
		
		
		$payedListAmount = $em->getRepository ( 'AppBundle:PaymentInvoice' )->getInvoicesGroupedByStatus(2,'sum',$from,$to);
		$report01 = array();
		for($i=1;$i<=12;$i++)
		{
			$report01['keys'][$i] = strtoupper(substr(Kaans::getMonthName($i),0,3));
			$report01['values'][$i] = 0;			
		}		
		foreach($payedListAmount as $item)
		{
			$tempRes = ($item['sum_amount'] - $item['sum_discount']);			
			$report01['values'][$item['month']] = $tempRes;			
		}
		$values01 = "[".implode(",",$report01['values'])."]";
		$keys01   = "['".implode("','",$report01['keys'])."']";
		
		
		
		$pendingListAmount = $em->getRepository ( 'AppBundle:PaymentInvoice' )->getInvoicesGroupedByStatus(1,'sum',$from,$to);
		$report02 = array();
		for($i=1;$i<=12;$i++)
		{
			$report02['keys'][$i] = strtoupper(substr(Kaans::getMonthName($i),0,3));
			$report02['values'][$i] = 0;			
		}		
		foreach($pendingListAmount as $item)
		{
			$tempRes = ($item['sum_amount'] - $item['sum_discount']);			
			$report02['values'][$item['month']] = $tempRes;			
		}
		$values02 = "[".implode(",",$report02['values'])."]";
		$keys02   = "['".implode("','",$report02['keys'])."']";
		
		
		
		
		$payedList = $em->getRepository ( 'AppBundle:PaymentInvoice' )->getInvoicesGroupedByStatus(2,'count',$from,$to);
		$report1 = array();
		for($i=1;$i<=12;$i++)
		{
			$report1['keys'][$i] = strtoupper(substr(Kaans::getMonthName($i),0,3));
			$report1['values'][$i] = 0;			
		}		
		foreach($payedList as $item)
		{			
			$report1['values'][$item['month']] = $item['count'];			
		}
		$values1 = "[".implode(",",$report1['values'])."]";
		$keys1   = "['".implode("','",$report1['keys'])."']";
		
		
		
		
		
		$pendingList = $em->getRepository ( 'AppBundle:PaymentInvoice' )->getInvoicesGroupedByStatus(1,'count',$from,$to);
		$report2 = array();
		for($i=1;$i<=12;$i++)
		{
			$report2['keys'][$i] = strtoupper(substr(Kaans::getMonthName($i),0,3));
			$report2['values'][$i] = 0;			
		}		
		foreach($pendingList as $item)
		{			
			$report2['values'][$item['month']] = $item['count'];			
		}
		$values2 = "[".implode(",",$report2['values'])."]";
		$keys2   = "['".implode("','",$report2['keys'])."']";
		
		
		$newClientList = $em->getRepository ( 'AppBundle:PaymentInvoice' )->getClientsGroupedByStatus();
		$report3 = array();
		for($i=1;$i<=12;$i++)
		{
			$report3['keys'][$i] = strtoupper(substr(Kaans::getMonthName($i),0,3));
			$report3['values'][$i] = 0;			
		}		
		foreach($newClientList as $item)
		{			
			$report3['values'][$item['month']] = $item['count'];			
		}
		$values3 = "[".implode(",",$report3['values'])."]";
		$keys3   = "['".implode("','",$report3['keys'])."']";
		
		
		//---
		
		
		$payedCustomList = $em->getRepository ( 'AppBundle:PaymentInvoice' )->getInvoicesGroupedByStatus(2,'sum',$from,$to,"Instalacion");
		$report4 = array();
		for($i=1;$i<=12;$i++)
		{
			$report4['keys'][$i] = strtoupper(substr(Kaans::getMonthName($i),0,3));
			$report4['values'][$i] = 0;			
		}		
		foreach($payedCustomList as $item)
		{			
			$report4['values'][$item['month']] = $item['sum_amount'];			
		}
		$values4 = "[".implode(",",$report4['values'])."]";
		$keys4   = "['".implode("','",$report4['keys'])."']";
		
		
		
		$pendingCustomList = $em->getRepository ( 'AppBundle:PaymentInvoice' )->getInvoicesGroupedByStatus(1,'sum',$from,$to,"Instalacion");
		$report5 = array();
		for($i=1;$i<=12;$i++)
		{
			$report5['keys'][$i] = strtoupper(substr(Kaans::getMonthName($i),0,3));
			$report5['values'][$i] = 0;			
		}		
		foreach($pendingCustomList as $item)
		{			
			$report5['values'][$item['month']] = $item['sum_amount'];			
		}
		$values5 = "[".implode(",",$report5['values'])."]";
		$keys5   = "['".implode("','",$report5['keys'])."']";
		
		
		
		
		return $this->render ( '@App/Backend/Dashboard/index.html.twig', array (				
		    "permits" => $mp,
		    "report01" => array('values'=>$values01,'keys'=>$keys01),
		    "report02" => array('values'=>$values02,'keys'=>$keys02),
		    "report1" => array('values'=>$values1,'keys'=>$keys1),
		    "report2" => array('values'=>$values2,'keys'=>$keys2),
		    "report3" => array('values'=>$values3,'keys'=>$keys3),
		    "report4" => array('values'=>$values4,'keys'=>$keys4),
		    "report5" => array('values'=>$values5,'keys'=>$keys5),
		    "from" => $from,
		    "to" => $to
		));
	}
	



}
