<?php

namespace AppBundle\Controller\Backend;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use AppBundle\Entity\Department;
use AppBundle\Form\DepartmentType;
use AppBundle\Repository\Kaans;
use AppBundle\Helper\UrlHelper;

/**
 * Board controller.
 */
class DepartmentController extends Controller {
	
	private $moduleId = 4;
	private $moduleName = "Mi Empresa";


	/**
	 * @Route("/backend/department", name="backend_department")
	 */
	public function indexAction(Request $request) {
			
		$this->get ( "session" )->set ( "module_id",$this->moduleId);		
		$this->get ( "session" )->set ( "module_name",$this->moduleName);
		$userData = $this->get ( "session" )->get ( "userData" );
		
		$object = new Department();
		$form = $this -> createForm(new DepartmentType(), $object);
		$form -> handleRequest($request);
		
		$userInfo = $this -> getDoctrine() -> getRepository('AppBundle:User') -> findOneByUserId($userData['id']);
		
				// Validar formulario
		if ($form -> isSubmitted()) {
			if ($form -> isValid()) {
				
				
				
				$object -> setCreatedAt(new \DateTime());
				$object -> setCreatedBy($userData["id"]);
				$object -> setOrganization($userInfo->getOrganization());
				$em = $this -> getDoctrine() -> getManager();				
				$em -> persist($object);
				$em -> flush();

				$this -> addFlash('success_message', $this -> getParameter('exito'));
				return $this -> redirectToRoute("backend_department");
			} else {
				$this -> addFlash('error_message', $this -> getParameter('error_form'));
			}
		}
		
		
		$list = $this -> getDoctrine() -> getRepository('AppBundle:Department') -> getList($userData, $userInfo->getOrganization()->getOrganizationId());
		$posChart = $this -> getDoctrine() -> getRepository('AppBundle:Position') -> getPositionChart($userInfo->getOrganization()->getOrganizationId());
		
		$orgId = $userInfo->getOrganization()->getOrganizationId();
		$orgName = $userInfo->getOrganization();
		
		$chartString = "";
		$chartString .= "[{v: 'O$orgId', f:'<h3>$orgName</h3>'},'',''],";
		foreach($posChart as $chart)
		{
			//{v:'Mike', f:'Mike<div style="color:red; font-style:italic">President</div>'}
			$who = (strlen($chart['employees'])>0 ? $chart['employees'] : '<i>No asignado</i>');
			$whoList = explode(",",$who);
			$whoFinal = implode("<br>",$whoList);
			$chartName = str_replace(" ","<br>",$chart['name']);
			 
			if(strlen($chart['parent_position_id'])>0)
			{
				$chartString .= "[{v: '$chart[position_id]', f:'<b style=\"color:#45aaf2;font-size:9px;\">$chartName</b><p>$whoFinal</p>'},'$chart[parent_position_id]','$chart[description]'],";
			} else {
				//$chartString .= "[{v: '$chart[department_id]', f:'<b>$chart[department_name]</b>'},'O$orgId','$chart[description]'],";
				//$chartString .= "[{v: '$chart[position_id]', f:'<b style=\"color:#45aaf2\">$chart[name]</b><p>$whoFinal</p>'},'$chart[department_id]','$chart[description]'],";
				$chartString .= "[{v: '$chart[position_id]', f:'<b style=\"color:#45aaf2;font-size:9px;\">$chartName</b><p>$whoFinal</p>'},'O$orgId','$chart[description]'],";
			}
		}
		
		//echo $chartString;exit;
		
		$chartStringFinal = rtrim($chartString,","); 
		
		
		 /*[{v:'Mike', f:'Mike<div style="color:red; font-style:italic">President</div>'},
           '', 'The President'],
          [{v:'Jim', f:'Jim<div style="color:red; font-style:italic">Vice President</div>'},
           'Mike', 'VP'],
          ['Alice', 'Mike', ''],
          ['Bob', 'Jim', ''],
          ['Carol', 'Bob', '']
		*/
		
		
		
		
		$mp = Kaans::getModulePermission($this->moduleId, $this->get("session")->get("userModules"));		
		
		return $this->render ( '@App/Backend/Department/index.html.twig', array (				
		    "permits" => $mp,
		    "form"    => $form -> createView(),
		    "list"    => $list,
		    'posChart' => $chartStringFinal
		));
	}






	/**
	 * @Route("/backend/department/edit/{id}", name="backend_department_edit"), requirements={"id": "\w+"})
	 */
	public function editAction(Request $request) {
			
		$md5 = $request -> get("id");
		$objectId = $this -> getDoctrine() -> getRepository('AppBundle:Department') -> findOneByMd5Id($md5);
		$object = $this -> getDoctrine() -> getRepository('AppBundle:Department') -> findOneBy(array("departmentId" => $objectId));

		if ($object) {

			$form = $this -> createForm(new DepartmentType(), $object);
			$form -> handleRequest($request);

			if ($form -> isSubmitted()) {
				if ($form -> isValid()) {

					$userData = $this -> get("session") -> get("userData");

					$em = $this -> getDoctrine() -> getManager();
					$em -> persist($object);
					$em -> flush();
					$this -> addFlash('success_message', $this -> getParameter('exito_actualizar'));					
				} else {
					return $this -> render('@App/Backend/Department/edit.html.twig', array("form" => $form -> createView()));
				}
			} else {
				return $this -> render('@App/Backend/Department/edit.html.twig', array("form" => $form -> createView()));
					
			}
		} else {
			$this -> addFlash('error_message', $this -> getParameter('error_editar'));
			
		}
		
		
		return $this -> render('@App/Backend/Department/edit.html.twig', array("form" => $form -> createView()));
		
	}




	/**
	 * @Route("/backend/department/delete/{id}", name="backend_department_delete")
	 */
	public function deleteAction(Request $request) {
		$md5Id = $request -> get("id");
		$objectInfo = $this -> getDoctrine() -> getRepository('AppBundle:Department') -> findOneByMd5Id($md5Id);
		$object = $this -> getDoctrine() -> getRepository('AppBundle:Department') -> findOneBy(array("departmentId" => $objectInfo['department_id']));
		if ($object) {
			
			$em = $this -> getDoctrine() -> getManager();
			// Eliminar
			$em -> remove($object);
			$em -> flush();

			$this -> addFlash('success_message', $this -> getParameter('exito_eliminar'));
		} else {
			$this -> addFlash('error_message', $this -> getParameter('error_eliminar'));
		}

		return $this -> redirectToRoute("backend_department");
	}


}
