<?php

namespace AppBundle\Controller\Backend;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use AppBundle\Entity\Flow;
use AppBundle\Entity\FlowDepartment;
use AppBundle\Form\FlowType;
use AppBundle\Repository\Kaans;
use AppBundle\Helper\UrlHelper;

/**
 * Board controller. 
 */
class MocksController extends Controller {
	
	private $moduleId = 10;
	private $moduleName = "Agenda de trabajo";


	/**
	 * @Route("/backend/mock/one", name="backend_mock_one")
	 */
	public function indexAction(Request $request) {
						
		$this->get ( "session" )->set ( "module_id",10);		
		$this->get ( "session" )->set ( "module_name","Agenda de Trabajo");
		$userData = $this->get ( "session" )->get ( "userData" );		

		$mp = Kaans::getModulePermission($this->moduleId, $this->get("session")->get("userModules"));
			
		return $this->render ( '@App/Backend/Mocks/index1.html.twig', array (				
		    "permits" => $mp
		));
	}


	/**
	 * @Route("/backend/mock/two", name="backend_mock_two")
	 */
	public function mockTwoAction(Request $request) {
			
		$this->get ( "session" )->set ( "module_id",11);		
		$this->get ( "session" )->set ( "module_name","Información de la Empresa");
		$userData = $this->get ( "session" )->get ( "userData" );		

		$mp = Kaans::getModulePermission($this->moduleId, $this->get("session")->get("userModules"));
			
		return $this->render ( '@App/Backend/Mocks/index2.html.twig', array (				
		    "permits" => $mp
		));
	}

	
	/**
	 * @Route("/backend/mock/three", name="backend_mock_three")
	 */
	public function mockThreeAction(Request $request) {
			
		$this->get ( "session" )->set ( "module_id",12);		
		$this->get ( "session" )->set ( "module_name","Documentos de la Empresa");
		$userData = $this->get ( "session" )->get ( "userData" );		

		$mp = Kaans::getModulePermission($this->moduleId, $this->get("session")->get("userModules"));
			
		return $this->render ( '@App/Backend/Mocks/index4.html.twig', array (				
		    "permits" => $mp
		));
	}
	
	

	
	/**
	 * @Route("/backend/mock/four", name="backend_mock_four")
	 */
	public function mockFourAction(Request $request) {
			
		$this->get ( "session" )->set ( "module_id",13);		
		$this->get ( "session" )->set ( "module_name","Asistente Virtual");
		$userData = $this->get ( "session" )->get ( "userData" );		

		$mp = Kaans::getModulePermission($this->moduleId, $this->get("session")->get("userModules"));
			
		return $this->render ( '@App/Backend/Mocks/index4.html.twig', array (				
		    "permits" => $mp
		));
	}
	
	
	
	/**
	 * @Route("/backend/mock/five", name="backend_mock_five")
	 */
	public function mockFiveAction(Request $request) {
			
		$this->get ( "session" )->set ( "module_id",14);		
		$this->get ( "session" )->set ( "module_name","Email");
		$userData = $this->get ( "session" )->get ( "userData" );		

		$mp = Kaans::getModulePermission($this->moduleId, $this->get("session")->get("userModules"));
			
		return $this->render ( '@App/Backend/Mocks/index5.html.twig', array (				
		    "permits" => $mp
		));
	}

}
