<?php

namespace AppBundle\Controller\Backend;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use AppBundle\Entity\Flow;
use AppBundle\Entity\FlowDepartment;
use AppBundle\Form\FlowType;
use AppBundle\Repository\Kaans;
use AppBundle\Helper\UrlHelper;

/**
 * Board controller. 
 */
class FlowController extends Controller {
	
	private $moduleId = 6;
	private $moduleName = "Flujos";


	/**
	 * @Route("/backend/flow/process/{pid}", name="backend_flow")
	 */
	public function indexAction(Request $request, $pid) {
			
		$this->get ( "session" )->set ( "module_id",$this->moduleId);		
		$this->get ( "session" )->set ( "module_name",$this->moduleName);
		$userData = $this->get ( "session" )->get ( "userData" );
		
		$object = new Flow();
		$form = $this -> createForm(new FlowType(), $object);
		$form -> handleRequest($request);
		
		$userInfo = $this -> getDoctrine() -> getRepository('AppBundle:User') -> findOneByUserId($userData['id']);
		$processInfo = $this -> getDoctrine() -> getRepository('AppBundle:ProcessList') -> findOneByMd5Id($pid);
		$processList = $this -> getDoctrine() -> getRepository('AppBundle:ProcessList') -> findOneByProcessListId($processInfo['process_list_id']);
		
				// Validar formulario
		if ($form -> isSubmitted()) {
			if ($form -> isValid()) {
				
				
				$object -> setCreatedAt(new \DateTime());
				$object -> setCreatedBy($userData["id"]);
				$object -> setOrganization($userInfo->getOrganization());
				$object -> setProcessList($processList);
				$em = $this -> getDoctrine() -> getManager();				
				$em -> persist($object);
				$em -> flush();
					
				$visibleTo = $request->get("visible_to_departments");			
				foreach($visibleTo as $visible)
				{
										
					$department = $this -> getDoctrine() -> getRepository('AppBundle:Department') -> findOneByDepartmentId($visible);
					$flowDepartment = new FlowDepartment();
					$flowDepartment->setDepartment($department);
					$flowDepartment->setFlow($object);
					$em -> persist($flowDepartment);
					$em -> flush();	
				}
		

				$this -> addFlash('success_message', $this -> getParameter('exito'));
				return $this -> redirectToRoute("backend_flow",array('pid'=>$pid));
			} else {
				$this -> addFlash('error_message', $this -> getParameter('error_form'));
			}
		}
		
		$departments = $this -> getDoctrine() -> getRepository('AppBundle:Department') -> getList($userData, $userInfo->getOrganization()->getOrganizationId());
		$currentDepartments = $this -> getDoctrine() -> getRepository('AppBundle:Flow') -> getFlowDepartmentsByOrganization($userInfo->getOrganization()->getOrganizationId());
		
		$list = $this -> getDoctrine() -> getRepository('AppBundle:Flow') -> getFlowsByPidAndOrg($processList->getProcessListId(),$userInfo->getOrganization()->getOrganizationId());
	 
		$mp = Kaans::getModulePermission($this->moduleId, $this->get("session")->get("userModules"));
		
		
		return $this->render ( '@App/Backend/Flow/index.html.twig', array (				
		    "permits" => $mp,
		    "form"    => $form -> createView(),
		    "list"    => $list,
		    'processList' => $processList,
		    'pid'=>$pid,
		    'departments'=>$departments,
		    'currentDepartments'=>$currentDepartments
		));
	}






	/**
	 * @Route("/backend/flow/process/{pid}/edit/{id}", name="backend_flow_edit"), requirements={"id": "\w+"})
	 */
	public function editAction(Request $request,$pid) {
			
		$md5 = $request -> get("id");
		$objectId = $this -> getDoctrine() -> getRepository('AppBundle:Flow') -> findOneByMd5Id($md5);
		$object = $this -> getDoctrine() -> getRepository('AppBundle:Flow') -> findOneBy(array("flowId" => $objectId));

		$userData = $this->get ( "session" )->get ( "userData" );
		$userInfo = $this -> getDoctrine() -> getRepository('AppBundle:User') -> findOneByUserId($userData['id']);
		$departments = $this -> getDoctrine() -> getRepository('AppBundle:Department') -> getList($userData, $userInfo->getOrganization()->getOrganizationId());
		
		

		if ($object) {

			$form = $this -> createForm(new FlowType(), $object);
			$form -> handleRequest($request);

			if ($form -> isSubmitted()) {
				if ($form -> isValid()) {
					$em = $this -> getDoctrine() -> getManager();
					
					$currentDepartments = $this -> getDoctrine() -> getRepository('AppBundle:FlowDepartment') -> findByFlow($object);
					foreach($currentDepartments as $current)
					{
						$deleteThis = $this -> getDoctrine() -> getRepository('AppBundle:FlowDepartment') -> findOneByFlowDepartmentId($current->getFlowDepartmentId());
						$em -> remove($deleteThis);
						$em -> flush();	
					}	
					
						
					$visibleTo = $request->get("visible_to_departments");
					
					foreach($visibleTo as $visible)
					{
											
						$department = $this -> getDoctrine() -> getRepository('AppBundle:Department') -> findOneByDepartmentId($visible);
						$flowDepartment = new FlowDepartment();
						$flowDepartment->setDepartment($department);
						$flowDepartment->setFlow($object);
						$em -> persist($flowDepartment);
						$em -> flush();	
					}

					
					$em -> persist($object);
					$em -> flush();
					$this -> addFlash('success_message', $this -> getParameter('exito_actualizar'));					
				} else {
					
					$currentDepartments = $this -> getDoctrine() -> getRepository('AppBundle:FlowDepartment') -> findByFlow($object);
					$arr = array();
					foreach($currentDepartments as $cur)
					{
						$arr[] = $cur->getDepartment()->getDepartmentId();
					}
															
					return $this -> render('@App/Backend/Flow/edit.html.twig', 
						array(
							"form" => $form -> createView(),
							'pid'=>$pid,
							'departments'=>$departments,
							'currentDepartments' => $arr
						));
				}
			} else {
				
				$currentDepartments = $this -> getDoctrine() -> getRepository('AppBundle:FlowDepartment') -> findByFlow($object);
				$arr = array();
				foreach($currentDepartments as $cur)
				{
					$arr[] = $cur->getDepartment()->getDepartmentId();
				}
					
				return $this -> render('@App/Backend/Flow/edit.html.twig',
					 array(
					 	"form" => $form -> createView(),
					 	'pid'=>$pid,
					 	'departments'=>$departments,
					 	'currentDepartments' => $arr
					));
					
			}
		} else {
			$this -> addFlash('error_message', $this -> getParameter('error_editar'));
			
		}
		
		$currentDepartments = $this -> getDoctrine() -> getRepository('AppBundle:FlowDepartment') -> findByFlow($object);
		$arr = array();
		foreach($currentDepartments as $cur)
		{
			$arr[] = $cur->getDepartment()->getDepartmentId();
		}
					
		return $this -> render('@App/Backend/Flow/edit.html.twig', 
			array(
				"form" => $form -> createView(),
				'pid'=>$pid,
				'departments'=>$departments,
				'currentDepartments' => $arr			
			));
		
	}




	/**
	 * @Route("/backend/flow/process/{pid}/delete/{id}", name="backend_flow_delete")
	 */
	public function deleteAction(Request $request,$pid) {
		$md5Id = $request -> get("id");
		$objectInfo = $this -> getDoctrine() -> getRepository('AppBundle:Flow') -> findOneByMd5Id($md5Id);
		$object = $this -> getDoctrine() -> getRepository('AppBundle:Flow') -> findOneBy(array("flowId" => $objectInfo['flow_id']));
		if ($object) {
			
			$em = $this -> getDoctrine() -> getManager();
			
			$flowDepts = $this -> getDoctrine() -> getRepository('AppBundle:FlowDepartment') -> findBy(array("flow" => $objectInfo['flow_id']));
			foreach($flowDepts as $fd)
			{
				$fdept = $this -> getDoctrine() -> getRepository('AppBundle:FlowDepartment') -> findOneBy(array("flowDepartmentId" => $fd->getFlowDepartmentId()));
				// Eliminar
				$em -> remove($fdept);
				$em -> flush();
			}
									
			// Eliminar
			$em -> remove($object);
			$em -> flush();

			$this -> addFlash('success_message', $this -> getParameter('exito_eliminar'));
		} else {
			$this -> addFlash('error_message', $this -> getParameter('error_eliminar'));
		}

		return $this -> redirectToRoute("backend_flow",array('pid'=>$pid));
	}


}
