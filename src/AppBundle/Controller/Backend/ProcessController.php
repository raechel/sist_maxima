<?php

namespace AppBundle\Controller\Backend;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use AppBundle\Entity\ProcessList;
use AppBundle\Form\ProcessListType;
use AppBundle\Repository\Kaans;
use AppBundle\Helper\UrlHelper;

/**
 * Board controller.
 */
class ProcessController extends Controller {
	
	private $moduleId = 6;
	private $moduleName = "Procesos";


	/**
	 * @Route("/backend/process", name="backend_process")
	 */
	public function indexAction(Request $request) {
			
		$this->get ( "session" )->set ( "module_id",$this->moduleId);		
		$this->get ( "session" )->set ( "module_name",$this->moduleName);
		$userData = $this->get ( "session" )->get ( "userData" );
		
		$object = new ProcessList();
		$form = $this -> createForm(new ProcessListType(), $object);
		$form -> handleRequest($request);
		
		$userInfo = $this -> getDoctrine() -> getRepository('AppBundle:User') -> findOneByUserId($userData['id']);
		
				// Validar formulario
		if ($form -> isSubmitted()) {
			if ($form -> isValid()) {

				$object -> setCreatedAt(new \DateTime());
				$object -> setCreatedBy($userData["id"]);
				$object -> setOrganization($userInfo->getOrganization());
				$em = $this -> getDoctrine() -> getManager();				
				$em -> persist($object);
				$em -> flush();

				$this -> addFlash('success_message', $this -> getParameter('exito'));
				return $this -> redirectToRoute("backend_process");
			} else {
				$this -> addFlash('error_message', $this -> getParameter('error_form'));
			}
		}
		
		
		$list = $this -> getDoctrine() -> getRepository('AppBundle:ProcessList') -> getList($userInfo->getOrganization()->getOrganizationId()); 
		$mp = Kaans::getModulePermission($this->moduleId, $this->get("session")->get("userModules"));
		
		
		return $this->render ( '@App/Backend/Process/index.html.twig', array (				
		    "permits" => $mp,
		    "form"    => $form -> createView(),
		    "list"    => $list,
		    "userInfo" => $userInfo
		));
	}






	/**
	 * @Route("/backend/Process/edit/{id}", name="backend_process_edit"), requirements={"id": "\w+"})
	 */
	public function editAction(Request $request) {
			
		$md5 = $request -> get("id");
		$objectId = $this -> getDoctrine() -> getRepository('AppBundle:ProcessList') -> findOneByMd5Id($md5);
		$object = $this -> getDoctrine() -> getRepository('AppBundle:ProcessList') -> findOneBy(array("processListId" => $objectId));

		if ($object) {

			$form = $this -> createForm(new ProcessListType(), $object);
			$form -> handleRequest($request);

			if ($form -> isSubmitted()) {
				if ($form -> isValid()) {

					$userData = $this -> get("session") -> get("userData");

					$em = $this -> getDoctrine() -> getManager();
					$em -> persist($object);
					$em -> flush();
					$this -> addFlash('success_message', $this -> getParameter('exito_actualizar'));					
				} else {
					return $this -> render('@App/Backend/Process/edit.html.twig', array("form" => $form -> createView()));
				}
			} else {
				return $this -> render('@App/Backend/Process/edit.html.twig', array("form" => $form -> createView()));
					
			}
		} else {
			$this -> addFlash('error_message', $this -> getParameter('error_editar'));
			
		}
		
		
		return $this -> render('@App/Backend/Process/edit.html.twig', array("form" => $form -> createView()));
		
	}




	/**
	 * @Route("/backend/Process/delete/{id}", name="backend_process_delete")
	 */
	public function deleteAction(Request $request) {
		$md5Id = $request -> get("id");
		$objectInfo = $this -> getDoctrine() -> getRepository('AppBundle:ProcessList') -> findOneByMd5Id($md5Id);
		$object = $this -> getDoctrine() -> getRepository('AppBundle:ProcessList') -> findOneBy(array("processListId" => $objectInfo['process_list_id']));
		if ($object) {
			
			$em = $this -> getDoctrine() -> getManager();
			// Eliminar
			$em -> remove($object);
			$em -> flush();

			$this -> addFlash('success_message', $this -> getParameter('exito_eliminar'));
		} else {
			$this -> addFlash('error_message', $this -> getParameter('error_eliminar'));
		}

		return $this -> redirectToRoute("backend_process");
	}


}
