<?php

namespace AppBundle\Controller\Backend;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use AppBundle\Entity\Position;
use AppBundle\Form\PositionType;
use AppBundle\Repository\Kaans;
use AppBundle\Helper\UrlHelper;

/**
 * Controller.
 */
class PositionController extends Controller {
	
	private $moduleId = 4;
	private $moduleName = "Puesto";


	/**
	 * @Route("/backend/Position/department/{did}", name="backend_position")
	 */
	public function indexAction(Request $request, $did) {
			
		$this->get ( "session" )->set ( "module_id",$this->moduleId);		
		$this->get ( "session" )->set ( "module_name",$this->moduleName);
		$userData = $this->get ( "session" )->get ( "userData" );
		$em = $this -> getDoctrine() -> getManager();	
		
		$object = new Position();
		$form = $this -> createForm(new PositionType(), $object);
		$form -> handleRequest($request);
		
		$thisUser = $em->getRepository ( 'AppBundle:User' )->findOneBy(array('userId'=>$userData['id']));
		$departmentInfo = $this -> getDoctrine() -> getRepository('AppBundle:Department') -> findOneByMd5Id($did);
		$department = $this -> getDoctrine() -> getRepository('AppBundle:Department') -> findOneBy(array("departmentId" => $departmentInfo['department_id']));
		$organization = $this -> getDoctrine() -> getRepository('AppBundle:Organization') -> findOneBy(array("organizationId" => $thisUser->getOrganization()->getOrganizationId()));
				// Validar formulario
		if ($form -> isSubmitted()) {
			if ($form -> isValid()) {

				
				$object -> setCreatedAt(new \DateTime());
				$object -> setCreatedBy($userData["id"]);
				$object -> setDepartment($department);
				$object -> setOrganization($organization);
				
							
				$em -> persist($object);
				$em -> flush();

				$this -> addFlash('success_message', $this -> getParameter('exito'));
				return $this -> redirectToRoute("backend_position",array('did'=>$did));
			} else {
				$this -> addFlash('error_message', $this -> getParameter('error_form'));
			}
		}
		
		
		
		$list = $this -> getDoctrine() -> getRepository('AppBundle:Position') -> getList($departmentInfo['department_id']);
		$listPos = $this -> getDoctrine() -> getRepository('AppBundle:Position') -> getList($departmentInfo['department_id'],true);
		 
		$mp = Kaans::getModulePermission($this->moduleId, $this->get("session")->get("userModules"));
		
		
		return $this->render ( '@App/Backend/Position/index.html.twig', array (				
		    "permits" => $mp,
		    "form"    => $form -> createView(),
		    "list"    => $list,
		    "listPos" => $listPos,
		    'department' => $department,
		    'did' => $did
		));
	}






	/**
	 * @Route("/backend/Position/department/{did}/edit/{id}", name="backend_position_edit"), requirements={"id": "\w+"})
	 */
	public function editAction(Request $request, $did) {
			
		$md5 = $request -> get("id");
		$objectId = $this -> getDoctrine() -> getRepository('AppBundle:Position') -> findOneByMd5Id($md5);
		$object = $this -> getDoctrine() -> getRepository('AppBundle:Position') -> findOneBy(array("positionId" => $objectId));

		$departmentInfo = $this -> getDoctrine() -> getRepository('AppBundle:Department') -> findOneByMd5Id($did);		
		$list = $this -> getDoctrine() -> getRepository('AppBundle:Position') -> getList($departmentInfo['department_id']);
		$listPos = $this -> getDoctrine() -> getRepository('AppBundle:Position') -> getList($departmentInfo['department_id'],true);
		
		if ($object) {

			$form = $this -> createForm(new PositionType(), $object);
			$form -> handleRequest($request);

			if ($form -> isSubmitted()) {
				if ($form -> isValid()) {

					$userData = $this -> get("session") -> get("userData");

					$em = $this -> getDoctrine() -> getManager();
					$em -> persist($object);
					$em -> flush();
					$this -> addFlash('success_message', $this -> getParameter('exito_actualizar'));					
				} else {
					return $this -> render('@App/Backend/Position/edit.html.twig', 
					array(
						"form" => $form -> createView(),
						"did"  => $did,
						'list'=>$list,
						'listPos'=>$listPos
					));
				}
			} else {
				return $this -> render('@App/Backend/Position/edit.html.twig', 
				array(
					"form" => $form -> createView(),
					"did"  => $did,
					'list'=>$list,
					'listPos'=>$listPos
				));
					
			}
		} else {
			$this -> addFlash('error_message', $this -> getParameter('error_editar'));
			
		}
		
		return $this -> render('@App/Backend/Position/edit.html.twig', array(
			   "form" => $form -> createView(),
			   'did'=>$did, 
			   'list'=>$list,
			   'listPos'=>$listPos
			));
		
	}




	/**
	 * @Route("/backend/Position/department/{did}/delete/{id}", name="backend_position_delete")
	 */
	public function deleteAction(Request $request, $did) {
		$md5Id = $request -> get("id");
		$objectInfo = $this -> getDoctrine() -> getRepository('AppBundle:Position') -> findOneByMd5Id($md5Id);
		$object = $this -> getDoctrine() -> getRepository('AppBundle:Position') -> findOneBy(array("positionId" => $objectInfo['position_id']));
		if ($object) {
			
			$em = $this -> getDoctrine() -> getManager();
			// Eliminar
			$em -> remove($object);
			$em -> flush();

			$this -> addFlash('success_message', $this -> getParameter('exito_eliminar'));
		} else {
			$this -> addFlash('error_message', $this -> getParameter('error_eliminar'));
		}

		return $this -> redirectToRoute("backend_position",array('did'=>$did));
	}


}
