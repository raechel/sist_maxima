<?php

namespace AppBundle\Controller\Backend;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use AppBundle\Entity\Business;
use AppBundle\Form\BusinessType;
use AppBundle\Repository\Kaans;
use AppBundle\Helper\UrlHelper;

/**
 * Board controller.
 */
class BusinessController extends Controller {
	
	private $moduleId = 2;
	private $moduleName = "Clientes y Proveedores";


	/**
	 * @Route("/backend/business", name="backend_business")
	 */
	public function indexAction(Request $request) {
			
		$this->get ( "session" )->set ( "module_id",$this->moduleId);		
		$this->get ( "session" )->set ( "module_name",$this->moduleName);
		$userData = $this->get ( "session" )->get ( "userData" );
		
		$object = new Business();
		$form = $this -> createForm(new BusinessType(), $object);
		$form -> handleRequest($request);
		
		$userInfo = $this -> getDoctrine() -> getRepository('AppBundle:User') -> findOneByUserId($userData['id']);
		
				// Validar formulario
		if ($form -> isSubmitted()) {
			if ($form -> isValid()) {

				$object -> setCreatedAt(new \DateTime());
				$object -> setCreatedBy($userData["id"]);
				$object -> setOrganization($userInfo->getOrganization());
				$em = $this -> getDoctrine() -> getManager();				
				$em -> persist($object);
				$em -> flush();

				$this -> addFlash('success_message', $this -> getParameter('exito'));
				return $this -> redirectToRoute("backend_business");
			} else {
				$this -> addFlash('error_message', $this -> getParameter('error_form'));
			}
		}
		
		
		$list = $this -> getDoctrine() -> getRepository('AppBundle:Business') -> findAll(); 
		$mp = Kaans::getModulePermission($this->moduleId, $this->get("session")->get("userModules"));
		
		
		return $this->render ( '@App/Backend/Business/index.html.twig', array (				
		    "permits" => $mp,
		    "form"    => $form -> createView(),
		    "list"    => $list
		));
	}






	/**
	 * @Route("/backend/business/edit/{id}", name="backend_business_edit"), requirements={"id": "\w+"})
	 */
	public function editAction(Request $request) {
			
		$md5 = $request -> get("id");
		$objectId = $this -> getDoctrine() -> getRepository('AppBundle:Business') -> findOneByMd5Id($md5);
		$object = $this -> getDoctrine() -> getRepository('AppBundle:Business') -> findOneBy(array("businessId" => $objectId));

		if ($object) {

			$form = $this -> createForm(new BusinessType(), $object);
			$form -> handleRequest($request);

			if ($form -> isSubmitted()) {
				if ($form -> isValid()) {

					$userData = $this -> get("session") -> get("userData");

					$em = $this -> getDoctrine() -> getManager();
					$em -> persist($object);
					$em -> flush();
					$this -> addFlash('success_message', $this -> getParameter('exito_actualizar'));					
				} else {
					return $this -> render('@App/Backend/Business/edit.html.twig', array("form" => $form -> createView()));
				}
			} else {
				return $this -> render('@App/Backend/Business/edit.html.twig', array("form" => $form -> createView()));
					
			}
		} else {
			$this -> addFlash('error_message', $this -> getParameter('error_editar'));
			
		}
		
		
		return $this -> render('@App/Backend/Business/edit.html.twig', array("form" => $form -> createView()));
		
	}




	/**
	 * @Route("/backend/business/delete/{id}", name="backend_business_delete")
	 */
	public function deleteAction(Request $request) {
		$md5Id = $request -> get("id");
		$objectInfo = $this -> getDoctrine() -> getRepository('AppBundle:Business') -> findOneByMd5Id($md5Id);
		$object = $this -> getDoctrine() -> getRepository('AppBundle:Business') -> findOneBy(array("businessId" => $objectInfo['business_id']));
		if ($object) {
			
			$em = $this -> getDoctrine() -> getManager();
			// Eliminar
			$em -> remove($object);
			$em -> flush();

			$this -> addFlash('success_message', $this -> getParameter('exito_eliminar'));
		} else {
			$this -> addFlash('error_message', $this -> getParameter('error_eliminar'));
		}

		return $this -> redirectToRoute("backend_business");
	}


}
