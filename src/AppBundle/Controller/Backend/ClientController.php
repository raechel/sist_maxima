<?php

namespace AppBundle\Controller\Backend;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use AppBundle\Entity\Client;
use AppBundle\Form\ClientType;
use AppBundle\Entity\ClientAgreement;
use AppBundle\Entity\PaymentInvoice;
use AppBundle\Form\ClientAgreementType;
use AppBundle\Helper\UserHelper;
use AppBundle\Helper\UtilsHelper;
use AppBundle\Helper\UrlHelper;
use AppBundle\Helper\MailHelper;
use AppBundle\Repository\Kaans;
use Symfony\Component\HttpFoundation\JsonResponse;


class ClientController extends Controller {
	
    
    private $moduleId = 3;
	private $moduleName = "Gestión de Clientes";
	
    


    /**
     * @Route("/backend/client/manager", name="backend_client_manager")
     */
	public function indexManagerAction(Request $request) {
		
		$this->get("session")->set("module_id", 4);	
		$this->get ( "session" )->set ( "module_name",$this->moduleName);	
		$userData = $this->get ( "session" )->get ( "userData" );
		$em = $this->getDoctrine()->getManager();
		
		$results = array();
		if($request->get("search"))
		{
			$results = $em->getRepository ( 'AppBundle:Client' )->getResults($request->get('search'));
		}
		
		
		$thisUser = $em->getRepository ( 'AppBundle:User' )->findOneBy(array('userId'=>$userData['id']));		
		$mp = Kaans::getModulePermission($this->moduleId, $this->get("session")->get("userModules"));		
		
		return $this->render ( '@App/Backend/Client/manager_index.html.twig', array (					
		        "permits" => $mp,
		        'userInfo'=>$thisUser,		        		        
		        'userData'=>$userData,
		        "results"=>$results		        
		) );
	}


    /**
     * @Route("/backend/client", name="backend_client")
     */
	public function indexAction(Request $request) {
		
		$this->get("session")->set("module_id", $this->moduleId);	
		$this->get ( "session" )->set ( "module_name",$this->moduleName);	
		$userData = $this->get ( "session" )->get ( "userData" );
		
		$object = new Client ();
		$form = $this->createForm ( new ClientType (), $object );
		$form->handleRequest ( $request );
		$em = $this->getDoctrine()->getManager();

		$thisUser = $em->getRepository ( 'AppBundle:User' )->findOneBy(array('userId'=>$userData['id']));
		
		// Validar formulario
		if ($form->isSubmitted ()) {
			if ($form->isValid ()) {
				

				if ($error) {
					// Add error flash
					$this->addFlash('error_message', $error);
				}
				else {
					//$user = $this->create($user, $this->getUser());					
					$em = $this->getDoctrine()->getManager();
					$organization = $this->getDoctrine()->getManager()->getRepository ( 'AppBundle:Organization' )->findOneBy(array('organizationId'=>$userData['organization_id']));
					$object->setOrganization($organization);															    				
				    $object->setCreatedBy($this->getUser());
				    $object->setCreatedAt(new \DateTime());			
				    $em->persist($object);
				    $em->flush();
					
					$this->addFlash ( 'success_message', $this->getParameter ( 'exito' ) );
					return $this->redirectToRoute ( "backend_client");
				}
			}
			else {
				$this->addFlash ( 'error_message', $this->getParameter ( 'error_form' ).$form->getErrorsAsString() );
			}
		}
		
		
		
		$list = $em->getRepository ( 'AppBundle:Client' )->findBy (array(
			'organization'=>$userData['organization_id'],
			'statusId'=>1
		));
		//$paginator = $this->get ( 'knp_paginator' );
		
		//$pagination = $paginator->paginate ( $queryUser, $request->query->getInt ( 'page', 1 ), $this->getParameter ( "number_of_rows" ) );
		
		$mp = Kaans::getModulePermission($this->moduleId, $this->get("session")->get("userModules"));		
		
		return $this->render ( '@App/Backend/Client/index.html.twig', array (
				"form" => $form->createView (),
				"list" => $list,				
		        "permits" => $mp,
		        'userInfo'=>$thisUser,		        		        
		        'userData'=>$userData		        
		) );
	}
	
	/**
	 * @Route("/backend/client/edit/{id}", name="backend_client_edit")
	 */
	public function editAction(Request $request) {
		
		$userData = $this->get ( "session" )->get ( "userData" );
		$md5 = $request->get ( "id" );
		
		$objectId = $this->getDoctrine()->getManager()->getRepository ( 'AppBundle:Client' )->findOneByMd5Id($md5);
		$object   = $this->getDoctrine()->getManager()->getRepository ( 'AppBundle:Client' )->findOneBy(array('clientId'=>$objectId,'statusId'=>"1"));
		
		$thisUser = $this->getDoctrine()->getManager()->getRepository ( 'AppBundle:User' )->findOneBy(array('userId'=>$userData['id']));
		
		if ($object) {
					
			$form = $this->createForm ( new ClientType (), $object );
			$form->handleRequest ( $request );
			if ($form->isSubmitted ()) {
				if ($form->isValid ()) {
									
					// Set updated By
					//$object->setCreatedBy($editor);
					//$object->setCreatedAt(new \DateTime());			
					$em = $this->getDoctrine()->getManager();
					$em->persist($object);
					$em->flush();
				
					$this->addFlash ( 'success_message', $this->getParameter ( 'exito_actualizar' ) );
				} else {
					// Error validation
					$this->addFlash ( 'error_message', $this->getParameter ( 'error_form' ) );
				}
			}

			$mp = Kaans::getModulePermission($this->moduleId, $this->get("session")->get("userModules"));
			
			return $this->render ( '@App/Backend/Client/edit.html.twig', array(
				"form" => $form->createView (),
				"edit" => true,
				'userInfo'=>$thisUser,
				'object'=>$object,					
				"permits" => $mp					
			) );
		} else {
			$this->addFlash ( 'error_message', $this->getParameter ( 'error_editar' ) );
		}
		return $this->redirectToRoute ( "backend_client");
	}



	/**
	 * @Route("/backend/client/agreement/edit/{id}", name="backend_client_agreement_edit")
	 */
	public function editAgAction(Request $request) {
		
		$userData = $this->get ( "session" )->get ( "userData" );
		$md5 = $request->get ( "id" );
		
		$objectId = $this->getDoctrine()->getManager()->getRepository ( 'AppBundle:Client' )->findOneClientAgreementByMd5Id($md5);
		$object   = $this->getDoctrine()->getManager()->getRepository ( 'AppBundle:ClientAgreement' )->findOneByClientAgreementId($objectId['client_agreement_id']);
				
		$thisUser = $this->getDoctrine()->getManager()->getRepository ( 'AppBundle:User' )->findOneBy(array('userId'=>$userData['id']));
		
		if ($object) {
					
			$form = $this->createForm ( new ClientAgreementType (), $object );
			$form->handleRequest ( $request );
			if ($form->isSubmitted ()) {
				if ($form->isValid ()) {
									
					// Set updated By
					//$object->setCreatedBy($editor);
					//$object->setCreatedAt(new \DateTime());	
					$object->setUpdatedBy($userData['id']);
				    $object->setUpdatedAt(new \DateTime());				
					$em = $this->getDoctrine()->getManager();
					$em->persist($object);
					$em->flush();
				
					$this->addFlash ( 'success_message', $this->getParameter ( 'exito_actualizar' ) );
				} else {
					// Error validation
					$this->addFlash ( 'error_message', $this->getParameter ( 'error_form' ) );
				}
			}

			$services = $this->getDoctrine()->getManager()->getRepository ( 'AppBundle:Service' )->findBy(array(
				"organization"=>$userData['organization_id'],
				"statusId"=>1
			));
		
			$mp = Kaans::getModulePermission($this->moduleId, $this->get("session")->get("userModules"));
			
			return $this->render ( '@App/Backend/Client/agreement_edit.html.twig', array(
				"form" => $form->createView (),
				"edit" => true,
				'userInfo'=>$thisUser,
				'object'=>$object,
				'clientId'=>$object->getClient()->getClientId(),					
				"permits" => $mp,
				'services'=>$services					
			) );
		} else {
			$this->addFlash ( 'error_message', $this->getParameter ( 'error_editar' ) );
		}
		return $this->redirectToRoute ( "backend_client_edit",array(
			"id" => $md5
		));
	}
	
	
	
	 public function returnPDFResponseFromHTML($html,$user,$cycle_number){
        //set_time_limit(30); uncomment this line according to your needs
        // If you are not in a controller, retrieve of some way the service container and then retrieve it
        //$pdf = $this->container->get("white_october.tcpdf")->create('vertical', PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
        //if you are in a controlller use :
        $pdf = $this->get("white_october.tcpdf")->create('vertical', PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
        $pdf->SetAuthor('Kaans - Plataforma Educativa');
		
		$subject = 'reporte_académico_'.$user->getFirstName()."_".$user->getLastName()."_".$cycle_number;
        $pdf->SetTitle($subject);
        $pdf->SetSubject($subject);
		$pdf->SetHeaderData("", "", "", "Reporte Académico de ".$user->getFirstName()." ".$user->getLastName());
				
        $pdf->setFontSubsetting(true);
        $pdf->SetFont('helvetica', '', 10, '', true);
        $pdf->SetMargins(15,20,15, true);
        $pdf->AddPage();
        
        $filename = $subject;
        
        $pdf->writeHTMLCell($w = 0, $h = 0, $x = '', $y = '', $html, $border = 0, $ln = 1, $fill = 0, $reseth = true, $align = '', $autopadding = true);
        $pdf->Output($filename.".pdf",'I'); // This will output the PDF as a response directly
    }



	/**
	 * @Route("/backend/ipcontrol/clean/{id}/cid/{cid}", name="backend_ipcontrol_clean")
	 */
	public function ipCleanAction(Request $request, $id,$cid) {
		

		$supply = $this->getDoctrine()->getManager()->getRepository ( 'AppBundle:IpControl' )->findOneByIpControlId($id);
		if($supply)
		{
						
			$agreementEntity = $this->getDoctrine()->getManager()->getRepository ( 'AppBundle:ClientAgreement' )->findOneByClientAgreementId($agreement);			
			$supply->setClientAgreement(null);
			$supply->setAssignedAt(null);			
			
			$em = $this->getDoctrine()->getManager();
			$em->persist($supply);
			$em->flush();
			
			$this->addFlash ( 'success_message', $this->getParameter ( 'exito' ) );
			
			
		} else {
			$this->addFlash ( 'error_message', $this->getParameter ( 'error_eliminar' ) );	
		}	
			
		return $this->redirectToRoute ( "backend_client_view",array("id"=>$cid));		
		
	}	

	/**
	 * @Route("/backend/supply/clean/{id}/cid/{cid}", name="backend_supply_clean")
	 */
	public function supplyCleanAction(Request $request, $id, $cid) {
		
		$ent = $this->getDoctrine()->getManager()->getRepository ( 'AppBundle:Client' )->findOneSupplyByMd5Id($id);
		$supply = $this->getDoctrine()->getManager()->getRepository ( 'AppBundle:Supply' )->findOneBySupplyId($ent['supply_id']);
		if($supply)
		{
			
			
			$agreementEntity = $this->getDoctrine()->getManager()->getRepository ( 'AppBundle:ClientAgreement' )->findOneByClientAgreementId($agreement);
			$supply->setClient(null);
			$supply->setClientAgreement(null);
			$supply->setAssignedAt(null);
			$supply->setAssignedBy(null);
			
			$em = $this->getDoctrine()->getManager();
			$em->persist($supply);
			$em->flush();
			
			$this->addFlash ( 'success_message', $this->getParameter ( 'exito' ) );
			
			
		} else {
			$this->addFlash ( 'error_message', $this->getParameter ( 'error_eliminar' ) );	
		}	
			
		return $this->redirectToRoute ( "backend_client_view",array("id"=>$cid));		
		
	}	
	
	
	public function pastInvoice($start,$clientAgreement,$organization,$invoiceStatus,$invoiceType,$isCustom = false)
	{
		
		$em = $this->getDoctrine()->getManager();
		
		$object = new PaymentInvoice();		
		$object->setClient($clientAgreement->getClient());		
		$object->setOrganization($organization);		
		$object->setClientAgreement($clientAgreement);		
		$object->setInvoiceStatus($invoiceStatus);
		
		if($isCustom)
		{
			$object->setCustomInvoice("Instalación");
			$object->setAmount($isCustom);
			$object->setInvoiceType($invoiceType);
		} else {
			$object->setDiscount($clientAgreement->getDiscount());
			$object->setAmount($clientAgreement->getService()->getAmount());
			$object->setService($clientAgreement->getService());	
			$object->setInvoiceType($invoiceType);
		}
		$object->setCreatedAt(new \DateTime($start));
		
		$em->persist($object);
		$em->flush();
			
	}
	

	/**
	 * @Route("/backend/client/view/{id}", name="backend_client_view")
	 */
	public function viewAction(Request $request) {
		
		$md5 = $request->get ( "id" );
		$userData = $this->get ( "session" )->get ( "userData" );
		
		$userId = $this->getDoctrine()->getManager()->getRepository ( 'AppBundle:Client' )->findOneByMd5Id($md5);
		$user   = $this->getDoctrine()->getManager()->getRepository ( 'AppBundle:Client' )->findOneByClientId($userId);		
		$em = $this->getDoctrine()->getManager();
		
		if($request->get('statusChange'))
		{
			$error = false;
			$agreement = $request->get('current_client_agreement');
			$newStatus = $request->get('newStatus');
			
			if($agreement || strlen($newStatus) > 0)
			{
				$agreementEntity = $this->getDoctrine()->getManager()->getRepository ( 'AppBundle:ClientAgreement' )->findOneByClientAgreementId($agreement);
				
				if($newStatus == 3 || $newStatus == 4)
				{
					$supply = $this->getDoctrine()->getManager()->getRepository ( 'AppBundle:Supply' )->findByClientAgreement($agreementEntity);
					$ipControl = $this->getDoctrine()->getManager()->getRepository ( 'AppBundle:IpControl' )->findByClientAgreement($agreementEntity);
					
					if($supply || $ipControl)
					{
						$this->addFlash('error_message', "Antes de desactivar esta cuenta, debe eliminar la asignación de suministros y de IPs.");
						$error = true;
					}
				}	
				
				//Si el cliente ya esta activado y quieren pasarlo a Esperando Activacion
				if($agreementEntity == 1 && $newStatus == 2)
				{
					$this->addFlash('error_message', "El cliente ya se encuentra activado y no puede pasarse de nuevo al estado Esperando Activación");
						$error = true;
				}	
				
				if(!$error)
				{			
							
					$agreementEntity->setStatusId($newStatus);
					$agreementEntity->setUpdatedAt(new \DateTime());
					$agreementEntity->setUpdatedBy($userData['id']);
					$em->persist($agreementEntity);
					$em->flush();
					
					$this->addFlash ( 'success_message', $this->getParameter ( 'exito' ) );
				}
			}
		}
		

		if($request->get('registerSupply'))
		{
			$values = $request->get('registerSupply');
			$supply = $values['supply'];
			$agreement = $values['agreement'];
			
			$supply = $this->getDoctrine()->getManager()->getRepository ( 'AppBundle:Supply' )->findOneBySupplyId($supply);
			$agreementEntity = $this->getDoctrine()->getManager()->getRepository ( 'AppBundle:ClientAgreement' )->findOneByClientAgreementId($agreement);
			$supply->setClient($user);
			$supply->setClientAgreement($agreementEntity);
			$supply->setAssignedAt(new \DateTime());
			$supply->setAssignedBy($userData['id']);
			$em->persist($supply);
			$em->flush();
			
			$this->addFlash ( 'success_message', $this->getParameter ( 'exito' ) );
		}
		
		
		if($request->get('registerIp'))
		{
			$values = $request->get('registerIp');
			$ip = $values['ip'];
			$agreement = $values['agreement'];
			
			$check = $this->getDoctrine()->getManager()->getRepository ( 'AppBundle:IpControl' )->findOneByClientAgreement($agreement);
			if($check)
			{
				$this->addFlash ( 'error_message', "Esta cuenta ya tiene una IP asignada, primero debe quitar la asignación de IP actual para asignar una nueva");
			} else {
			
				$ipobj = $this->getDoctrine()->getManager()->getRepository ( 'AppBundle:IpControl' )->findOneByIpControlId($ip);
				$agreementEntity = $this->getDoctrine()->getManager()->getRepository ( 'AppBundle:ClientAgreement' )->findOneByClientAgreementId($agreement);			
				$ipobj->setClientAgreement($agreementEntity);
				$ipobj->setAssignedAt(new \DateTime());			
				$em->persist($ipobj);
				$em->flush();
				
				$this->addFlash ( 'success_message', $this->getParameter ( 'exito' ) );
			}
		}
		
		if ($user) {
				
			
		} else {
			return $this->redirectToRoute ( "backend_client" );
		}
		
		
		$object = new ClientAgreement ();
		$form = $this->createForm ( new ClientAgreementType (), $object );
		$form->handleRequest ( $request );
		

		$thisUser = $em->getRepository ( 'AppBundle:User' )->findOneBy(array('userId'=>$userData['id']));
		
		// Validar formulario
		if ($form->isSubmitted ()) {
			if ($form->isValid ()) {
				

				if ($error) {
					// Add error flash
					$this->addFlash('error_message', $error);
				}
				else {
					//$user = $this->create($user, $this->getUser());
													
					$object->setClient($user);
					$object->setStatusId(2);															    				
				    $object->setCreatedBy($userData['id']);
				    $object->setCreatedAt(new \DateTime());			
				    $em->persist($object);
				    $em->flush();
					
					
					
					/*$clientAgreement = $this->getDoctrine()->getManager()->getRepository ( 'AppBundle:ClientAgreement' )->findOneBy(array(
						"clientAgreement"=>$object
					));*/
					$organization = $this->getDoctrine()->getManager()->getRepository ( 'AppBundle:Organization' )->findOneBy(array('organizationId'=>$userData['organization_id']));
					$invoiceStatus = $this->getDoctrine()->getManager()->getRepository ( 'AppBundle:PaymentInvoiceStatus' )->findOneBy(array('invoiceStatusId'=>1));
					if($request->get('installation_price'))
					{
						if($request->get('installation_price') != "")
						{
							$this->pastInvoice($object->getAgreementDate(),$object,$organization,$invoiceStatus,2,$request->get('installation_price'));
						}
					}
					
					//FACTURA DEL MES	
					$this->pastInvoice($object->getAgreementDate(),$object,$organization,$invoiceStatus,1);				
					
					//FACTURAS RETROACTIVAS										
					$start = $object->getAgreementDate();
					$thisDate = $start;					
					$date1 = new \DateTime($start);
					$date2 = new \DateTime(date("Y-m-d"));
					$diff =  $date1->diff($date2);	
					$months = $diff->y * 12 + $diff->m + $diff->d / 30;					
					if($request->get('past_invoices'))
					{	
						for($i=1;$i<=$months;$i++)
						{
						   $thisDate = date('Y-m-d', strtotime("+1 months", strtotime($thisDate)));
						   $this->pastInvoice($thisDate,$object,$organization,$invoiceStatus,3);
						}
					}
					
					
					$this->addFlash ( 'success_message', $this->getParameter ( 'exito' ) );
					return $this->redirectToRoute ( "backend_client_view",array("id"=>$md5));
				}
			}
			else {
				$this->addFlash ( 'error_message', $this->getParameter ( 'error_form' ).$form->getErrorsAsString() );
			}
		}
		
		$list = $em->getRepository ( 'AppBundle:ClientAgreement' )->findBy (array(
			'client'=>$user->getClientId()			
		));
		
		
		$agreement = $this->getDoctrine()->getManager()->getRepository ( 'AppBundle:ClientAgreement' )->findBy(array(
			"client"=>$user->getClientId(),
			"statusId"=>array(1,2,3)
		));
		$services = $this->getDoctrine()->getManager()->getRepository ( 'AppBundle:Service' )->findBy(array(
			"organization"=>$userData['organization_id'],
			"statusId"=>1
		));
		
		$mp = Kaans::getModulePermission($this->moduleId, $this->get("session")->get("userModules"));
			
		/*$invoices = $em->getRepository ( 'AppBundle:PaymentInvoice' )->findBy (array(
			'organization'=>$userData['organization_id'],
			'client'=>$user->getClientId()			
		));*/
		
		$invoices = $em->getRepository ( 'AppBundle:PaymentInvoice' )->getInvoices($userData,0,$user->getClientId());
		
		$supplies = $em->getRepository ( 'AppBundle:Supply' )->findBy (array(
			'organization'=>$userData['organization_id']			
		));
		
		$ips = $em->getRepository ( 'AppBundle:IpControl' )->findBy(array(),array("ipNumber"=>"ASC"));
		
			
		return $this->render ( '@App/Backend/Client/view.html.twig', array (				
			"client" => $user,				
	        "permits" => $mp,
	        "list" => $invoices,
	        "agreement" => $agreement,
	        "form" => $form->createView(),
	        "services" => $services,
	        "supplies"=>$supplies,
	        "ips"=>$ips
	        
		));
		
		
		
		
	}



	
	/**
	 * @Route("/backend/client/delete/{id}", name="backend_client_delete")
	 */
	public function deleteAction(Request $request) {
			
		$md5 = $request->get ( "id" );
		$objectId = $this->getDoctrine()->getManager()->getRepository ( 'AppBundle:Client' )->findOneByMd5Id($md5);
		$object   = $this->getDoctrine()->getManager()->getRepository ( 'AppBundle:Client' )->findOneByClientId($objectId);
		
		$ags   = $this->getDoctrine()->getManager()->getRepository ( 'AppBundle:ClientAgreement' )->findBy(
			array('client'=>$object,'statusId'=>array(1,2,3))
		);
		foreach($ags as $check)
		{
			$this->addFlash ( 'error_message', "Antes de eliminar a este cliente, debe eliminar manualmente a las cuentas que tiene asignadas" );
			return $this->redirectToRoute ( "backend_client");
		}		
		
		if ($object) {
			// Can't delete yourself
			
			$em = $this->getDoctrine ()->getManager ();
			$object->setStatusId(0);
			$em->persist( $object );
			$em->flush ();
			
			$this->addFlash ( 'success_message', $this->getParameter ( 'exito_eliminar' ) );
			
		} else {
			$this->addFlash ( 'error_message', $this->getParameter ( 'error_eliminar' ) );
		}
		
		return $this->redirectToRoute ( "backend_client");
	}
	
	
	
	
	/**
	 * @Route("/backend/client/agreement/delete/{id}", name="backend_client_agreement_delete")
	 */
	public function deleteAgAction(Request $request) {
			
		$md5 = $request->get ( "id" );
		$objectId = $this->getDoctrine()->getManager()->getRepository ( 'AppBundle:Client' )->findOneClientAgreementByMd5Id($md5);
		$object   = $this->getDoctrine()->getManager()->getRepository ( 'AppBundle:ClientAgreement' )->findOneByClientAgreementId($objectId['client_agreement_id']);
		$userData = $this->get ( "session" )->get ( "userData" );
		
		
		$check1 = $this->getDoctrine()->getManager()->getRepository ( 'AppBundle:IpControl' )->findOneByClientAgreement($object);
		if($check1)
		{
			$this->addFlash ( 'error_message', "Antes de eliminar, debe quitar la asignación de IP");
			return $this->redirectToRoute ( "backend_client_view",array("id"=>md5($object->getClient()->getClientId())));
		}		
		$check2 = $this->getDoctrine()->getManager()->getRepository ( 'AppBundle:Supply' )->findOneByClientAgreement($object);
		if($check2)
		{
			$this->addFlash ( 'error_message', "Antes de eliminar, debe quitar la asignación de los suministros");
			return $this->redirectToRoute ( "backend_client_view",array("id"=>md5($object->getClient()->getClientId())));
		}
		
		
		if ($object) {
			// Can't delete yourself
			
			$em = $this->getDoctrine ()->getManager ();
			$object->setStatusId(0);
			$object->setUpdatedBy($userData['id']);
			$object->setUpdatedAt(new \DateTime());
			$em->persist( $object );
			$em->flush ();
			
			$this->addFlash ( 'success_message', $this->getParameter ( 'exito_eliminar' ) );
			
		} else {
			$this->addFlash ( 'error_message', $this->getParameter ( 'error_eliminar' ) );
		}
		
		return $this->redirectToRoute ( "backend_client_view",array("id"=>md5($object->getClient()->getClientId())));
	}


	/**
	 * Encodes password, sets created at and by and stores in db
	 * 
	 * @param User $user the entity to be created
	 * @param User $creator the user who creates a user
	 * @return User user saved in db
	 */
	public function create($user, $creator) {
		

	    //return $user;
	}

	/**
	 * Validates the information of a new user
	 *
	 * @param User $user possible new user
	 * @return string error message. NULL if no error
	 */
	public function validateNewUser($user) {
		$em = $this->getDoctrine()->getManager();

	    // Check for duplicate email
	    $original = $em->getRepository('AppBundle:User')->findOneByEmail($user->getEmail());

	    if ($original) {
	        // Email already in database
	        return $this->getParameter('usuario_existente');
	    }

	    return null;
	}

	 

	/**	 
	 * @Route("/backend/user/recovery", name="backend_user_password_recovery")
	 */
	 public function passwordRecovery(Request $request)
	 {
	 		
		return $this->render ( '@App/Backend/login.html.twig', array (
			
		));
		
	 }	
	 
	/**
	 * @Route("/backend/user/password/change", name="backend_user_password_change")
	 */
	 public function passwordChange(Request $request)
	 {
	 	$md5 = $request->get('id');
		
		$values = $request->get('pass');
		$oldPass = $values['old'];
		$pass_raw = $values['new'];
		
		$userId = $this->getDoctrine()->getManager()->getRepository ( 'AppBundle:User' )->findOneByMd5Id($md5);
		$usercheck   = $this->getDoctrine()->getManager()->getRepository ( 'AppBundle:User' )->findOneBy(array('userId'=>$userId));
				
		$userHelper = $this->get('user.helper');
		$oldPassword = $userHelper->encodePassword($usercheck, $oldPass);
		$pass = $userHelper->encodePassword($usercheck, $pass_raw);
			
		
		$user   = $this->getDoctrine()->getManager()->getRepository ( 'AppBundle:User' )->findOneBy(array('userId'=>$userId,'password'=>$oldPassword));
		
		if($user)
		{
			
			$em = $this->getDoctrine()->getManager();				
			$user->setPassword($pass);
			$em->persist($user);
			$em->flush();
			$this->addFlash ( 'success_message', "La contraseña ha sido cambiada correctamente." );	
		} else {
			$this->addFlash ( 'error_message', "La contraseña actual no es correcta." );
		}
		

				
		return $this->redirectToRoute ( "backend_user_edit" ,array("id"=>$md5));	
	 }	
	 
	/**	 
	 * @Route("/backend/user/recovery/app", name="backend_user_password_recovery_app")
	 */
	 public function passwordRecoveryAppCheck(Request $request)
	 {

	 	$response = new JsonResponse();
		$data = $this->get("request")->getContent();
		if(!empty($data)) {
		    $params = json_decode($data, true);
		}		
		
		$email = $params['_email'];
		$user = $this->getDoctrine ()->getRepository ( 'AppBundle:User' )->findOneBy(array('email'=>$email));
		if($user)
		{
				
			$utils = new UtilsHelper();
			$userHelper = $this->get('user.helper');		
						
			$pass = $utils->randomChars(6);			
			$user->setPassword($userHelper->encodePassword($user, $pass));
			
			$em = $this->getDoctrine()->getManager();								    
		    $em->persist($user);
		    $em->flush();
			
			$mailHelper = new MailHelper();
			$mailHelper->recoverPassword($user,$pass);
			
			$response->setData(array('status' => 'success'));
				
		} else {		
			$response->setData(array('status' => 'error'));
		}
		
	 	return $response;
		
	 }
	 
	/**	 
	 * @Route("/backend/user/recovery/check", name="backend_user_password_recovery_check")
	 */
	 public function passwordRecoveryCheck(Request $request)
	 {
	 	
		
		$email = $request->get("_email");
		$user = $this->getDoctrine ()->getRepository ( 'AppBundle:User' )->findOneBy(array('email'=>$email));
		if($user)
		{
				
			$utils = new UtilsHelper();
			$userHelper = $this->get('user.helper');		
						
			$pass = $utils->randomChars(6);			
			$user->setPassword($userHelper->encodePassword($user, $pass));
			
			$em = $this->getDoctrine()->getManager();								    
		    $em->persist($user);
		    $em->flush();
			
			$mailHelper = new MailHelper();
			$mailHelper->recoverPassword($user,$pass);
			
			$this->addFlash('success_message', "Hemos enviado un email con instrucciones a $email,<br>por favor verifique su bandeja de entrada.");
				
		} else {		
			$this->addFlash('error_message', "Email incorrecto, intente de nuevo.");
		}
		
	 	return $this->redirectToRoute ( "backend_user_password_recovery" );	
	 }

	/**
	 * Validates the information of a editing user
	 *
	 * @param User $user user to edit
	 * @param string $oldEmail the email before editing
	 * @return string error message. NULL if no error
	 */
	public function validateEditUser($user, $oldEmail) {
		$em = $this->getDoctrine()->getManager();

	    // Check for duplicate email
	    $original = null;
	    if ($user->getEmail() != $oldEmail) {
	    	// Look if duplicate
		    $original = $em->getRepository('AppBundle:User')->findOneByEmail($user->getEmail());
	    }

	    if ($original) {
	        // Email already in database
	        return $this->getParameter('usuario_existente');
	    }

	    return null;
	}
	
	public function base64_url_encode($input) {
	 return strtr(base64_encode($input), '+/=', '._-');
	}
}
