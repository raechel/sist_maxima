<?php
namespace AppBundle\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;


class InvoiceGenerateCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this
            // the name of the command (the part after "app/console")
            ->setName('invoice:generate')

            // the short description shown while running "php app/console list"
            ->setDescription('Generates invoices for the payment control platform')

            // the full command description shown when running the command with
            // the "--help" option
            ->setHelp("No parameters are needed, just run and have fun")
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        // outputs multiple lines to the console (adding "\n" at the end of each line)
        $output->writeln(array(
            'Invoice control init',
            '============',
            ''
        ));

        // outputs a message followed by a "\n"
        $output->writeln('Checking for agreements dates due TODAY:');
		$output->writeln('-----------------');
        
        $em = $this->getContainer()->get('doctrine.orm.entity_manager');
        $items = $em->getRepository('AppBundle:Client')->getTodaysAgreement();
	
        // outputs a message without adding a "\n" at the end of the line
        $total = 0;
        foreach ($items as $key => $item) {
        	
			
            $output->write('Checking agreement for account: ');
            $output->writeln($item['account_name']);
			$output->writeln("Client: ".$item['client_name']."'");            
            $output->writeln("Amount Invoice: ".$item['amount']);            
			$output->writeln('');
			
            // Try to update
            // Helper to enconde password
            //$staffHelper = $this->getContainer()->get('user.helper');
            
            if(strlen($item['amount']) > 0 && strlen($item['account_name']) > 0)
            {
	            try {
	                // Update
	                //$encoded = $staffHelper->encodePassword($staff, $staff->getCitizenId());
	                $check = $em->getRepository("AppBundle:Client")->checkIfInvoiceExists($item['client_agreement_id']);
	                if($check['count_result'] == 0)
					{					
	                	$res = $em->getRepository('AppBundle:Client')->generateInvoice($item['client_id'],$item['service_id'],$item['organization_id'],$item['client_agreement_id'],$item['amount'],$item['discount']);	
	                	$output->writeln('<info>INVOICE GENERATED WITH ID: '.$res.'</info>');
					} else {
						$output->writeln("<error> IGNORING INVOICE GENERATION - Already exists</error>");	
					}
	            } catch (\Exception $e) {
	                // Error
	                $output->writeln("<error> Exception updating: $e</error>");
	            }
				
			} else {
				$output->writeln("<error> Exception updating: --".print_r($item,true)."</error>");
			}
        }

        // outputs a message followed by a "\n"
        $output->writeln('-----------------');
        $output->writeln("<info>Total agreements due today: $total...</info>");        
    }
}