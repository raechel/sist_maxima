<?php 
namespace AppBundle\Twig;

class AppExtension extends \Twig_Extension
{
    public function getFilters()
    {
        return array(            
            new \Twig_SimpleFilter('slugify', array($this, 'slugify')),
            new \Twig_SimpleFilter('format_date', array($this, 'formatDate')),
        	new \Twig_SimpleFilter('md5', array($this, 'md5')),
        	new \Twig_SimpleFilter('wrap', array($this, 'wrap')),
        	new \Twig_SimpleFilter('labels', array($this, 'labels')),
        	new \Twig_SimpleFilter('extract_image', array($this, 'extract_image')),
        	new \Twig_SimpleFilter('extract_short', array($this, 'extract_short')),
        	new \Twig_SimpleFilter('extract_text', array($this, 'extract_text')),
        	new \Twig_SimpleFilter('extract_likes', array($this, 'extract_likes')),
			new \Twig_SimpleFilter('light_color', array($this, 'light_color')),
			new \Twig_SimpleFilter('client_code', array($this, 'client_code'))
        );
    }


    public function getName()
    {
        return 'app_extension';
    }
    
    /**
     * 
     * @param \DateTime $date
     * @return string
     */
    public function formatDate(\DateTime $date) {
                                          
        $format_date = sprintf("%d de %s %s - %s", $date->format("d"), self::getEsMonth($date->format("n")), $date->format("Y"), $date->format("h:i"));
        
        return $format_date;
        
    }
	
	public function light_color($hex)
	{
		
		$steps = '-20';
		// Steps should be between -255 and 255. Negative = darker, positive = lighter
	    $steps = max(-255, min(255, $steps));
	
	    // Normalize into a six character long hex string
	    $hex = str_replace('#', '', $hex);
	    if (strlen($hex) == 3) {
	        $hex = str_repeat(substr($hex,0,1), 2).str_repeat(substr($hex,1,1), 2).str_repeat(substr($hex,2,1), 2);
	    }
	
	    // Split into three parts: R, G and B
	    $color_parts = str_split($hex, 2);
	    $return = '#';
	
	    foreach ($color_parts as $color) {
	        $color   = hexdec($color); // Convert to decimal
	        $color   = max(0,min(255,$color + $steps)); // Adjust color
	        $return .= str_pad(dechex($color), 2, '0', STR_PAD_LEFT); // Make two char hex code
	    }
	
	    return $return;
					
	}


	public function extract_likes($notes)
	{
		
	    $count = 0;
		
		foreach($notes as $note)
		{
			if($note['type'] == 'like')
			{
				$count++;
			}
		}	 
		 
      	return $count;
		
	}
	
	
	
	public function client_code($clientId)
	{
		
	    $arr = str_split($clientId);
		$result = implode("-",$arr);
		 
      	return $result;
		
	}
	
	

	public function extract_text($text)
	{
		
		$content = preg_replace("/<img[^>]+\>/i", "", $text); 
		 
      	return $content;
		
	}
    

	public function extract_short($text, $limit = 100)
	{
		
		if (str_word_count($text, 0) > $limit) {
          $words = str_word_count($text, 2);
          $pos = array_keys($words);
          $text = substr($text, 0, $pos[$limit]) . '...';
      	}
		
		$content = preg_replace("/<img[^>]+\>/i", "", $text); 
		$clean = strip_tags($content);
      	return $clean;
		
	}
    
	
	public function extract_image($string)
	{
		
		preg_match('%<img.*?src=["\'](.*?)["\'].*?/>%i', $string, $matches);
		$imgSrc = $matches[1];
		return $imgSrc;
	}
    
    /**
     *
     * @param string $text
     * @return string
     */
    public function slugify($text)
    {
    
        $titleUrl = self::replaceAccents($text);
    
        // trim and lowercase
        $xtext = strtolower(trim($titleUrl, '-'));
    
        return $xtext;
    }
    
    /**
     *
     * @param string $text
     * @param int $quantity
     * @return string
     */
    public function wrap($text, $quantity = 15)
    {        
    
    	// trim and lowercase
    	$xtext = substr($text, 0, $quantity);
    
    	return $xtext;
    }
    
    public function labels($string) {
    	
    	$array_string = explode(",", $string);
    	$labels = "";
    	foreach ($array_string as $item) 
    		$labels .= "<label class='label label-primary font_size_20'>$item</label>&nbsp;"; 
    	
    	
    	return $labels;
    }
    
    /**
     *
     * @param string $text
     * @return string
     */
    public function md5($text)
    {        
    	return md5($text);
    }
    
    public static function getEsMonth($month) {
        $months =  array(
            "1" => "Enero",
            "2" => "Febrero",
            "3" => "Marzo",
            "4" => "Abril",
            "5" => "Mayo",
            "6" => "Junio",
            "7" => "Julio",
            "8" => "Agosto",
            "9" => "Septiembre",
            "10" => "Octubre",
            "11" => "Noviembre",
            "12" => "Diciembre"
        );
    
        return ($months[$month]);
    }
    
    public static function getEsMonthAbrv($month) {
        $months =  array(
            "1" => "Ene",
            "2" => "Feb",
            "3" => "Mar",
            "4" => "Abr",
            "5" => "May",
            "6" => "Jun",
            "7" => "Jul",
            "8" => "Ago",
            "9" => "Sep",
            "10" => "Oct",
            "11" => "Nov",
            "12" => "Dic"
        );
    
        return ($months[$month]);
    }
    
    public static function getEsDay($day) {
        $days =  array(
            "1" => "Lunes",
            "2" => "Martes",
            "3" => "Miercoles",
            "4" => "Jueves",
            "5" => "Viernes",
            "6" => "Sabado",
            "7" => "Domingo"
    
        );
    
        return ($days[$day]);
    }
    
    static public function replaceAccents($str) {
        $a = array('�', '�', '�', '�', '�', '�', '�', '�', '�', '�', ' ', ',', '�', '�', '.', '�', '?', '�', ';', '"', '#', '�', '�', ':', '/', '\'');
        $b = array('A', 'E', 'I', 'O', 'U', 'a', 'e', 'i', 'o', 'u', '-', '-', 'n', 'n', '',  '',  '',  '', '',  '',  '',  '',  '',  '-', '-', '-');
        return str_replace($a, $b, $str);
    }
    
    public function getFunctions()
    {
    	return array(
    			'file_exists' => new \Twig_Function_Function('file_exists'),
    	);
    }
}

?>