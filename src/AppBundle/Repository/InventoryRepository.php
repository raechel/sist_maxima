<?php

namespace AppBundle\Repository;

use Doctrine\ORM\EntityRepository;


class InventoryRepository extends EntityRepository
{
	
	public function findOneByMd5Id($md5Id) {
		$query = "
			SELECT
		    u.inventory_id 
		FROM
		    inventory u
		WHERE
		    md5(u.inventory_id) = :md5Id
		;
		";
		
		
		$res = $this->getEntityManager ()->getConnection ()->prepare ( $query );
		$res->bindValue ( 'md5Id', $md5Id, \PDO::PARAM_STR );
		
		$res->execute ();
		
		return $res->fetch ();
	}
	

	public function findOneSellByMd5Id($md5Id) {
		$query = "
			SELECT
		    u.inventory_sell_id 
		FROM
		    inventory_sell u
		WHERE
		    md5(u.inventory_sell_id) = :md5Id
		;
		";
		
		
		$res = $this->getEntityManager ()->getConnection ()->prepare ( $query );
		$res->bindValue ( 'md5Id', $md5Id, \PDO::PARAM_STR );
		
		$res->execute ();
		
		return $res->fetch ();
	}
	
		
	public function findOnePurchaseByMd5Id($md5Id) {
		$query = "
			SELECT
		    u.inventory_purchase_id 
		FROM
		    inventory_purchase u
		WHERE
		    md5(u.inventory_purchase_id) = :md5Id
		;
		";
		
		
		$res = $this->getEntityManager ()->getConnection ()->prepare ( $query );
		$res->bindValue ( 'md5Id', $md5Id, \PDO::PARAM_STR );
		
		$res->execute ();
		
		return $res->fetch ();
	}
	
	
	
	public function getInventoryStatus($userData, $pid = false, $limit = false) {
		$query = "
			select 
				m.name as measure_type,
				(select sum(quantity) from inventory_sell where inventory_id = i.inventory_id) as total_selled,
				(select sum(quantity) from inventory_purchase where inventory_id = i.inventory_id) as total_purchased,
				i.* 
				
					from inventory i, measure_type m
						where i.organization_id = :orgId
						and m.measure_type_id = i.measure_type_id
		
		";
		
		if($pid)
		{
			$query .= " and i.inventory_id = '$pid' ";
		}
		
		if($limit)
		{
			$query .= " limit $limit ";
		}
		
		$res = $this->getEntityManager ()->getConnection ()->prepare ( $query );
		$res->bindValue ( 'orgId', $userData['organization_id'], \PDO::PARAM_STR );
		
		$res->execute ();
		if($pid)
		{
			return $res->fetch ();
		} else {
			return $res->fetchAll ();	
		}
		
	}
	
	

	public function getSellHistory($userData,$from,$to)
	{
		$query = "select i.name as product_name, si.* from inventory_sell si, inventory i
			where i.inventory_id = si.inventory_id 
			and si.organization_id = :orgId ";
			
		if(strlen($from)>0 && strlen($to)>0)
		{	
			$query .= " and date(si.created_at) between '$from' and '$to' ";
		}
		
		
		$res = $this->getEntityManager ()->getConnection ()->prepare ( $query );
		$res->bindValue ( 'orgId', $userData['organization_id'], \PDO::PARAM_STR );
		
		$res->execute ();
		return $res->fetchAll();
		
		
	}
	
	
	public function reportBestDaySell($userData)
	{
		$query = "SELECT
								
			(SELECT sum(sell_price) as total_sell FROM inventory_sell s
			WHERE organization_id = :orgId AND year(created_at) = year(now())
			AND dayofweek(created_at) = 2) as 'LUNES',
			
			(SELECT sum(sell_price) as total_sell FROM inventory_sell s
			WHERE organization_id = :orgId AND year(created_at) = year(now()) 
			AND dayofweek(created_at) = 3) as 'MARTES',
			
			(SELECT sum(sell_price) as total_sell FROM inventory_sell s
			WHERE organization_id = :orgId AND year(created_at) = year(now())
			AND dayofweek(created_at) = 4) as 'MIERCOLES',
			
			(SELECT sum(sell_price) as total_sell FROM inventory_sell s
			WHERE organization_id = :orgId AND year(created_at) = year(now())
			AND dayofweek(created_at) = 5) as 'JUEVES',
			
			(SELECT sum(sell_price) as total_sell FROM inventory_sell s
			WHERE organization_id = :orgId AND year(created_at) = year(now())
			AND dayofweek(created_at) = 6) as 'VIERNES',
			
			(SELECT sum(sell_price) as total_sell FROM inventory_sell s
			WHERE organization_id = :orgId AND year(created_at) = year(now())
			AND dayofweek(created_at) = 7) as 'SABADO',
			
			(SELECT sum(sell_price) as total_sell FROM inventory_sell s
			WHERE organization_id = :orgId AND year(created_at) = year(now())
			AND dayofweek(created_at) = 1) as 'DOMINGO'
							
		";
		
		
		$res = $this->getEntityManager ()->getConnection ()->prepare ( $query );
		$res->bindValue ( 'orgId', $userData['organization_id'], \PDO::PARAM_STR );
		
		$res->execute ();
		return $res->fetch();
		
		
	}
	
	
	
	public function inventoryMainReport($userData,$month)
	{
		$query = "select 

					sum(sell_price * quantity) as net_sell_price,
					sum(purchase_price * quantity) as net_purchase_price,
					(SELECT sum(money_amount) FROM inventory_purchase 
						WHERE month(created_at) = month(s.created_at) 
							AND year(created_at) = year(s.created_at)
							AND purchase_type <> 2 ) as net_purchase 
					
					from inventory_sell s
					
					where s.organization_id = :orgId
					and month(s.created_at) = :month
					and year(s.created_at) = year(now())";
					
		$res = $this->getEntityManager ()->getConnection ()->prepare ( $query );
		$res->bindValue ( 'orgId', $userData['organization_id'], \PDO::PARAM_STR );
		$res->bindValue ( 'month', $month, \PDO::PARAM_STR );
		
		$res->execute ();
		return $res->fetch();					
	}
	
	
	public function reportBestHours($userData)
	{
			
		$query = "select 

					hour(created_at) as hour,
					sum(sell_price) as sell_prices
					
					from inventory_sell
					
					where year(created_at) = year(now())
					and month(created_at) = month(now())
					and organization_id = :orgId
					
					
					group by hour(created_at)
					order by hour(created_at) ASC ";
		
		$res = $this->getEntityManager ()->getConnection ()->prepare ( $query );
		$res->bindValue ( 'orgId', $userData['organization_id'], \PDO::PARAM_STR );		
		
		$res->execute ();
		return $res->fetchAll();
	}
	
	
	
	public function reportUtilitiesYear($userData,$month)
	{
		$query = "SELECT 

					sum(i.sell_price - i.purchase_price) as difference 
					
					FROM inventory_sell i
					
					WHERE year(created_at) = year(now())
					AND month(created_at) = :month
					AND organization_id = :orgId
							";
							
		$res = $this->getEntityManager ()->getConnection ()->prepare ( $query );
		$res->bindValue ( 'orgId', $userData['organization_id'], \PDO::PARAM_STR );
		$res->bindValue ( 'month', $month, \PDO::PARAM_STR );
		
		$res->execute ();
		return $res->fetch();
											
	}
	
	
	
	public function getTopSells($userData) {
		$query = "
			select sum(quantity) as selled, n.name as inventory_name, m.name as measure_type
				from inventory_sell i, inventory n, measure_type m
					where n.inventory_id = i.inventory_id
					and m.measure_type_id = n.measure_type_id
					and i.organization_id = :orgId
					and month(i.created_at) = month(now())
					and year(i.created_at) = year(now())
						group by i.inventory_id
						order by selled desc
							limit 10
		
		";
		
		
		$res = $this->getEntityManager ()->getConnection ()->prepare ( $query );
		$res->bindValue ( 'orgId', $userData['organization_id'], \PDO::PARAM_STR );
		
		$res->execute ();
		return $res->fetchAll ();	
		
		
	}
	
}	
	
?>	