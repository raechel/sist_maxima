<?php

namespace AppBundle\Repository;

class Kaans {

	/**
	 *
	 * Enter description here ...
	 * @param unknown_type $array
	 */
	public static function printr($array) {
		echo "<pre>";
		print_r($array);
		echo "</pre>";
	}
	
	public static function checkPm($moduleId, $pm = 'viewm') {
		$checkModuleView = false;
		foreach ($userModules as $module) {
			if ($module["mcid"] == $moduleId && $pm == 1) {
				$checkModuleView = true;
			}
		}
		return $checkModuleView;
	}
	
	
	public static function getMonthName($month)
	{
		 $list = array('Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio',
       		'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre');
		
		 return $list[$month-1];	
			
	}
	
	public static function getModulePermission($moduleId, $userModules) {
	    
	    foreach ($userModules as $module) {
	        if ($module["mcid"] == $moduleId) {
	            return $module;
	        }
	    }
	    return false;
	}


	/**
	 *
	 * Enter description here ...
	 * @param unknown_type $files
	 * @param unknown_type $thumbnail
	 * @param unknown_type $path
	 * @param unknown_type $onlyThisExtensions
	 */
	public function uploadImages($files, $thumbnail = false, $path, $onlyThisExtensions = false)
	{
		try {
			if (is_array($files)) {
				foreach ($files as $picture) {
					if (!$picture["error"]) {
						// si no hay error en la subida realizar el upload de la imagen

						// obtener data
						$size = $picture["size"];
						$type = $picture["type"];
						$name = $picture["name"];
						$tmp =  $picture['tmp_name'];
						$file_name = substr($name, 0, strlen($name) - 4);
						$ext = substr($name, strlen($name) - 3, strlen($name));
						$prefix = substr(md5($name . time()),0, 5);
						$xpath = $path . $name . "-" . $prefix . "." . $ext;
						// Validar extenciones
						if ($onlyThisExtensions) {
							if (!in_array($ext, $onlyThisExtensions))
							return false;
						}
						// subir imagen
						if (copy($tmp, $xpath))
						$rsImages[] = $file_name . " - " . $prefix . "." . $ext;
						else
						$rsImages[] = "error";

						if ($thumbnail) {
							$thumb = new Thumbnail($xpath);
							$thumb->size_width(800);
							$thumb->save($xpath);
						}

					}
				}

				if (isset($rsImages))
				return $rsImages;
				else
				return false;

			}
		} catch (Exception $ex) {

			Doctrine::getTable('BackendErrorLog')->saveErrorLog(array(
			'description' => $ex->getMessage(),
			'modules' => "EbClosion:$modelName::uploadImages",
			'backend_action_log_id' => 5,
			));
			return false;
		}
	}

	/**
	 *
	 * @param string $text
	 * @return string
	 */
	static public function slugify($text)
	{
		
		$titleUrl = EbClosion::replaceAccents($text);

		// replace all non letters or digits by -
		//$xtext = preg_replace('/\W+/', '-', $titleUrl);

		// trim and lowercase
		$xtext = strtolower(trim($titleUrl, '-'));

		return $xtext;
	} 

	static public function sendPush($registrationIds,$body,$title)
	{
    	
     	$msg = array(
			 'body' 	=> $body,
			 'title'	=> $title,
             'icon'	=> 'myicon',/*Default Icon*/
             'sound' => 'default'/*Default sound*/
        );
		  
		$fields = array(
			'to'		=> $registrationIds,
			'notification'	=> $msg
		);
		
	    $key = 'AAAAk1np9UU:APA91bHz-_MYA5zFTRbyxN8deNs6F1MecvBQpWqAa8JRlKCkKSIPUkB7puoHh0y2z7RPkjSBis3Lv2X1aay-moZDJ5VOqSAyYMRNEF3RGni1nday4Kpllu_XHTlG5snvAwdjNsz_6rDi';
		$headers = array(
			'Authorization: key='.$key,
			'Content-Type: application/json'
		);
		
		#Send Reponse To FireBase Server	
		$ch = curl_init();
		curl_setopt( $ch,CURLOPT_URL, 'https://fcm.googleapis.com/fcm/send' );
		curl_setopt( $ch,CURLOPT_POST, true );
		curl_setopt( $ch,CURLOPT_HTTPHEADER, $headers );
		curl_setopt( $ch,CURLOPT_RETURNTRANSFER, true );
		curl_setopt( $ch,CURLOPT_SSL_VERIFYPEER, false );
		curl_setopt( $ch,CURLOPT_POSTFIELDS, json_encode( $fields ) );
		$result = curl_exec($ch );
		curl_close( $ch );
		 
		#Echo Result Of FireBase Server
		return $result;
	}


}
