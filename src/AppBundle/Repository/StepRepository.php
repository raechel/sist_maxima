<?php

namespace AppBundle\Repository;

use Doctrine\ORM\EntityRepository;


class StepRepository extends EntityRepository
{
	
	public function findOneReqByMd5Id($md5Id) {
		$query = "
			SELECT
		    u.step_requirement_id 
		FROM
		    step_requirement u
		WHERE
		    md5(u.step_requirement_id) = :md5Id
		;
		";
		
		
		$res = $this->getEntityManager ()->getConnection ()->prepare ( $query );
		$res->bindValue ( 'md5Id', $md5Id, \PDO::PARAM_STR );
		
		$res->execute ();
		
		return $res->fetch ();
	}
	
	
	
	public function findOneByMd5Id($md5Id) {
		$query = "
			SELECT
		    u.step_id
		FROM
		    step u
		WHERE
		    md5(u.step_id) = '$md5Id'
		;
		";
		
		
		$res = $this->getEntityManager ()->getConnection ()->prepare ( $query );
		$res->bindValue ( 'md5Id', $md5Id, \PDO::PARAM_STR );
		
		$res->execute ();
		
		return $res->fetch ();
	}
	

	
	public function getStepsReqsByStepId($md5Id) {
		$query = "
			SELECT
				(select name from department where department_id = sr.department_id) as department_name,
				(select name from position where position_id = sr.position_id) as position_name,
				sr.is_personal,
		    	sr.*
			FROM
			    step_requirement sr
			WHERE
			    md5(sr.step_id) = :md5Id			
			;
			";
			
		
		$res = $this->getEntityManager ()->getConnection ()->prepare ( $query );
		$res->bindValue ( 'md5Id', $md5Id, \PDO::PARAM_STR );
		
		$res->execute ();
		
		return $res->fetchAll ();
	}
	
	
	
	public function getStepsByFlowId($md5Id) {
		$query = "
			SELECT
		    	u.step_id as stepId,
		    	u.name,
		    	u.description,
		    	u.flow_id as flowId,
		    	u.organization_id as organizationId,
		    	u.created_at as createdAt,
		    	u.created_by as createdBy
			FROM
			    step u, flow f
			WHERE
			    f.flow_id = $md5Id
			    AND f.flow_id = u.flow_id 
			;
			";
			
		
		$res = $this->getEntityManager ()->getConnection ()->prepare ( $query );
		$res->bindValue ( 'md5Id', $md5Id, \PDO::PARAM_STR );
		
		$res->execute ();
		
		return $res->fetchAll ();
	}
	
	
	
}

?>	

