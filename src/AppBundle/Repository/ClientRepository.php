<?php

namespace AppBundle\Repository;

use Doctrine\ORM\EntityRepository;
use AppBundle\Entity\User;
use Symfony\Bridge\Doctrine\Security\User\UserLoaderInterface;
use Doctrine\ORM\Query\Expr\Join;

class ClientRepository extends EntityRepository
{


	public function getResults($term) {
		$query = "
			SELECT
		    *
			FROM client u
				WHERE ( name like '%".$term."%' 
		    		OR phone like '%".$term."%' ) 
		;
		";
		$res = $this->getEntityManager ()->getConnection ()->prepare ( $query );
		//$res->bindValue ( 'md5Id', $md5Id, \PDO::PARAM_STR );
		
		$res->execute ();
		
		return $res->fetchAll ();
	}
	
	//Se busca si existe una cuenta que se tenga que cobrar el DIA de hoy
	public function getTodaysAgreement() {
		$query = "
			SELECT
		    c.name as client_name,
		    c.client_id,
		    s.service_id,		    
		    s.amount,
		    c.organization_id,		    
		    u.*
			FROM client_agreement u, client c, service s
				WHERE day(agreement_date) = day(now())
				AND c.client_id = u.client_id  
				AND s.service_id = u.service_id
				AND u.status_id = 1
				
			;";
		$res = $this->getEntityManager ()->getConnection ()->prepare ( $query );		
		$res->execute ();
		
		return $res->fetchAll ();
	}
	
	
	public function checkIfInvoiceExists($agreementId) {
		
		$query = "
			select count(*) as count_result from payment_invoice 
			where day(created_at) = day(now()) 
			and month(created_at) = month(now())
			and client_agreement_id = '$agreementId'
			;";
			
		$res = $this->getEntityManager ()->getConnection ()->prepare ( $query );		
		$res->execute ();
		
		return $res->fetch ();
	}
	
	
	public function generateInvoice($clientId,$serviceId,$organizationId,$agreementId,$amount,$discount)
	{
		$now = date("Y-m-d H:i:s");
		$query = "
			INSERT INTO payment_invoice (client_id, service_id, organization_id, invoice_status_id, client_agreement_id, amount, discount, created_at)
				VALUES ('$clientId', '$serviceId', '$organizationId', '1', '$agreementId', '$amount','$discount','$now');
			;";
		$res = $this->getEntityManager ()->getConnection ()->prepare ( $query );
		
		
		$result = $res->execute ();
		$lastId = $this->getEntityManager ()->getConnection()->lastInsertId();
		
		return $lastId;
	}
	






	public function findOneSupplyByMd5Id($md5Id)
	{
		$query = "
			SELECT
		    u.supply_id
		FROM
		    supply u
		WHERE
		    md5(u.supply_id) = :md5Id
		;
		";
		$res = $this->getEntityManager ()->getConnection ()->prepare ( $query );
		$res->bindValue ( 'md5Id', $md5Id, \PDO::PARAM_STR );
		
		$res->execute ();
		
		return $res->fetch ();		
	}

	public function findOneServiceByMd5Id($md5Id)
	{
		$query = "
			SELECT
		    u.service_id
		FROM
		    service u
		WHERE
		    md5(u.service_id) = :md5Id
		;
		";
		$res = $this->getEntityManager ()->getConnection ()->prepare ( $query );
		$res->bindValue ( 'md5Id', $md5Id, \PDO::PARAM_STR );
		
		$res->execute ();
		
		return $res->fetch ();		
	}
	
	
	public function findOneClientAgreementByMd5Id($md5Id)
	{
		$query = "
			SELECT
		    u.client_agreement_id
		FROM
		    client_agreement u
		WHERE
		    md5(u.client_agreement_id) = :md5Id
		;
		";
		$res = $this->getEntityManager ()->getConnection ()->prepare ( $query );
		$res->bindValue ( 'md5Id', $md5Id, \PDO::PARAM_STR );
		
		$res->execute ();
		
		return $res->fetch ();		
	}

	public function findOneByMd5Id($md5Id) {
		$query = "
			SELECT
		    u.client_id
		FROM
		    client u
		WHERE
		    md5(u.client_id) = :md5Id
		;
		";
		$res = $this->getEntityManager ()->getConnection ()->prepare ( $query );
		$res->bindValue ( 'md5Id', $md5Id, \PDO::PARAM_STR );
		
		$res->execute ();
		
		return $res->fetch ();
	}
	
	
}
