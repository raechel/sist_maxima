<?php

namespace AppBundle\Repository;

use Doctrine\ORM\EntityRepository;


class DepartmentRepository extends EntityRepository
{
	
	public function findOneByMd5Id($md5Id) {
		$query = "
			SELECT
		    u.department_id
		FROM
		    department u
		WHERE
		    md5(u.department_id) = :md5Id
		;
		";
		
		
		$res = $this->getEntityManager ()->getConnection ()->prepare ( $query );
		$res->bindValue ( 'md5Id', $md5Id, \PDO::PARAM_STR );
		
		$res->execute ();
		
		return $res->fetch ();
	}


	public function getOrganigram($userData, $organizationId) {
		$query = "
			SELECT
			u.department_id as departmentId,
			(select count(*) from user where department_id = u.department_id and status = 'ACTIVO' group by department_id) as total_users,
		    u.*
		FROM
		    department u
		WHERE
		    u.organization_id = :id
		;
		";
		
		
		$res = $this->getEntityManager ()->getConnection ()->prepare ( $query );
		$res->bindValue ( 'id', $organizationId, \PDO::PARAM_STR );
		
		$res->execute ();
		
		return $res->fetchAll ();
	}


	public function getList($userData, $organizationId) {
		$query = "
			SELECT
			(select count(*) from position where department_id = u.department_id) as position_count,
			u.department_id as departmentId,
			(select count(*) from user where department_id = u.department_id and status = 'ACTIVO' group by department_id) as total_users,
		    u.*
		FROM
		    department u
		WHERE
		    u.organization_id = :id
		;
		";
		
		
		$res = $this->getEntityManager ()->getConnection ()->prepare ( $query );
		$res->bindValue ( 'id', $organizationId, \PDO::PARAM_STR );
		
		$res->execute ();
		
		return $res->fetchAll ();
	}
	
}

?>	

