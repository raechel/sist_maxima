<?php

namespace AppBundle\Repository;

use Doctrine\ORM\EntityRepository;


class BusinessRepository extends EntityRepository
{
	
	public function findOneByMd5Id($md5Id) {
		$query = "
			SELECT
		    u.business_id
		FROM
		    business u
		WHERE
		    md5(u.business_id) = :md5Id
		;
		";
		
		
		$res = $this->getEntityManager ()->getConnection ()->prepare ( $query );
		$res->bindValue ( 'md5Id', $md5Id, \PDO::PARAM_STR );
		
		$res->execute ();
		
		return $res->fetch ();
	}
	
}

?>	

