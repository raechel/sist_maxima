<?php

namespace AppBundle\Repository;

use Doctrine\ORM\EntityRepository;


class PositionRepository extends EntityRepository
{
	
	public function findOneByMd5Id($md5Id) {
		$query = "
			SELECT
		    u.position_id
		FROM
		    position u
		WHERE
		    md5(u.position_id) = :md5Id
		;
		";
		
		
		$res = $this->getEntityManager ()->getConnection ()->prepare ( $query );
		$res->bindValue ( 'md5Id', $md5Id, \PDO::PARAM_STR );
		
		$res->execute ();
		
		return $res->fetch ();
	}
	
	
	
	public function getPositionChart($org)
	{
		$query = "select 
					(select group_concat(concat(first_name,' ',last_name)) from user where position_id = p.position_id) as employees,
		            p.position_id,
					p.parent_position_id,
					p.name,
					p.description,
					d.department_id,
					d.name as department_name,
					(select name from position where position_id = p.parent_position_id) as parent
					
						from  position p, department d
							where p.organization_id = :org
							and d.department_id = p.department_id; ";

		$res = $this->getEntityManager ()->getConnection ()->prepare ( $query );
		$res->bindValue ( 'org', $org, \PDO::PARAM_STR );
		
		$res->execute ();
		
		return $res->fetchAll ();							
							
	}
	
	
	
	public function getList($department, $getAll = false) {
		$query = "
			SELECT 
				d.name as departmentName,
				(select dd.name from position pp, department dd where pp.position_id = p.parent_position_id and dd.department_id = pp.department_id) as parent_department,
				(select name from position where position_id = p.parent_position_id) as parent,
				(select count(position_id) from user where position_id = p.position_id) as user_count,
				(select count(position_id) from step_requirement where position_id = p.position_id) as req_count,
				p.position_id as positionId,				
				p.* 
					FROM position p, department d
						WHERE p.department_id = d.department_id ";
		
		if(!$getAll)
		{			 
			$query .= " AND p.department_id = :dep 	;";
		}
		
		$res = $this->getEntityManager ()->getConnection ()->prepare ( $query );
		if(!$getAll)
		{	
			$res->bindValue ( 'dep', $department, \PDO::PARAM_STR );
		}	
		
		$res->execute ();
		
		return $res->fetchAll ();
	}
	
}

?>	

