<?php

namespace AppBundle\Repository;

use Doctrine\ORM\EntityRepository;


class PaymentInvoiceRepository extends EntityRepository
{
	
	public function findOneInvoiceByMd5Id($md5Id) {
		$query = "
			SELECT
		    u.invoice_id
		FROM
		    payment_invoice u
		WHERE
		    md5(u.invoice_id) = :md5Id
		;
		";
		$res = $this->getEntityManager ()->getConnection ()->prepare ( $query );
		$res->bindValue ( 'md5Id', $md5Id, \PDO::PARAM_STR );
		
		$res->execute ();
		
		return $res->fetch ();
	}
		
	
	public function getLastCutDate($selectedUser)
	{
		
		$query = "SELECT * FROM payment_invoice_cut WHERE user_id = :userId ORDER BY payment_invoice_cut_id DESC limit 1";
		//and payed_at > '2019-04-11 20:06:37'
				
		$res = $this->getEntityManager ()->getConnection ()->prepare ( $query );
		$res->bindValue ( 'userId', $selectedUser, \PDO::PARAM_STR );		
		$res->execute ();
		
		return $res->fetch();
	}
	
	
	public function reportCutForInvoices($invoiceIds,$cutId)
	{
		if($cutId != "" && $invoiceIds != "")
		{
			$query = "UPDATE payment_invoice SET payment_invoice_cut_id = $cutId WHERE invoice_id IN ($invoiceIds); ";
			
			$res = $this->getEntityManager ()->getConnection ()->prepare ( $query );			
			$res->execute ();
		}		
	}
	
	public function getReportedCutDetails($cutId)
	{
		
		$query = "SELECT u.first_name, u.last_name, c.name as client_name, ca.account_name, month(pi.created_at) as created_at_month, year(pi.created_at) as created_at_year,  pi.* 
			FROM payment_invoice pi, client c, client_agreement ca, user u
				WHERE c.client_id = pi.client_id 					 
					AND ca.client_agreement_id = pi.client_agreement_id 
					AND invoice_status_id = 2
					AND u.user_id = pi.payment_by ";
		
			$query .= " AND payment_invoice_cut_id = $cutId ";
		
				
				
		$res = $this->getEntityManager ()->getConnection ()->prepare ( $query );			
		$res->execute ();
		
		return $res->fetchAll();
	}
	
	
	public function getPendingAmountDetails($selectedUser,$lastDate)
	{
		
		$query = "SELECT c.name as client_name, ca.account_name, month(pi.created_at) as created_at_month, year(pi.created_at) as created_at_year, pi.* 
			FROM payment_invoice pi, client c, client_agreement ca
				WHERE c.client_id = pi.client_id 
					AND payment_by = :userId 
					AND ca.client_agreement_id = pi.client_agreement_id 
					AND invoice_status_id = 2 ";
		if($lastDate != '')
		{
			$query .= " AND payed_at > '$lastDate' ";
		}
				
				
		$res = $this->getEntityManager ()->getConnection ()->prepare ( $query );
		$res->bindValue ( 'userId', $selectedUser, \PDO::PARAM_STR );		
		$res->execute ();
		
		return $res->fetchAll();
	}
	
	public function getPendingAmount($selectedUser,$lastDate)
	{
		
		$query = "SELECT (sum(if(amount,amount,0)) - sum(if(discount,discount,0))) as pending_amount FROM payment_invoice WHERE payment_by = :userId AND payment_invoice_cut_id = 0  ";
		if($lastDate != '')
		{
			$query .= " AND payed_at > '$lastDate' ";
		}
				
		$res = $this->getEntityManager ()->getConnection ()->prepare ( $query );
		$res->bindValue ( 'userId', $selectedUser, \PDO::PARAM_STR );		
		$res->execute ();
		
		return $res->fetch();
	}
	
	public function getInvoices($userData,$filter,$clientId = false)
	{
			
		$real_filter = $filter;
		if($filter == 2)
		{
			$real_filter = 1;
		} 	
			
		$query = "SELECT

					datediff(date(now()),p.created_at) as days_difference,
					c.name as client_name,
					ca.account_name,
					(select s.name from service s where s.service_id = p.service_id) as service_name,
					si.name as invoice_name,
					si.name as status_name,
					p.*
					
					FROM 
					
						payment_invoice p, 
						client c, 
						client_agreement ca, 
						
						payment_invoice_status si
						
							WHERE 1 = 1
								AND p.organization_id = :orgId
								AND c.client_id = p.client_id
								
								AND ca.client_agreement_id = p.client_agreement_id
								AND si.invoice_status_id = p.invoice_status_id
								
				 ";				
		
		if($filter == 3)
		{
			$query .= " AND p.invoice_status_id = 2 ";
		}
						
		if($filter == 1)
		{
			$query .= " AND datediff(date(now()),p.created_at) < 31 AND p.invoice_status_id = 1 ";
		}				
		
		if($filter == 2)
		{
			$query .= " AND p.invoice_status_id = 1  AND datediff(date(now()),p.created_at) > 31 ";
		}											
		
		
		if($clientId)
		{
			$query .= " AND p.client_id = $clientId ";
		}									
		
		if($filter == 2)
		{
			$query .= " LIMIT 500 ";
		}					
											
		$res = $this->getEntityManager ()->getConnection ()->prepare ( $query );
		$res->bindValue ( 'orgId', $userData['organization_id'], \PDO::PARAM_STR );
			
		
		$res->execute ();
		
		return $res->fetchAll ();
		
	}
	
	
	
	
	
	public function getInvoicesGroupedByStatus($status,$type = 'count', $from, $to, $custom = '') {
		$query = "
			select 

			month(created_at) as month, ";
			
		if($type == 'count')
		{	
			$query .= " count(*) as count ";
		} else {
			$query .= " sum(amount) as sum_amount, sum(discount) as sum_discount ";
		}
			
		$query .=" from payment_invoice 
			
			where invoice_status_id = :status ";
		
		if(strlen($from) == 0 && strlen($to) == 0)
		{
			$query .= " and year(created_at) = year(now()) ";
		} else {
			$query .= " and date(created_at) BETWEEN '$from' AND '$to' ";
		}
		
		if($custom == '')
		{
			$query .= " and custom_invoice is null ";
		}
		
		if($custom == 'Instalacion')
		{
			$query .= " and custom_invoice like '%Instalacion%' ";
		}
			
		$query .= "	group by month(created_at)
		;
		";
		
		if($type != 'count')
		{
			//echo $query;exit;
		}
		
		if($custom == 'Instalacion')
		{
			//echo $query;exit;	
		}
		
		$res = $this->getEntityManager ()->getConnection ()->prepare ( $query );
		$res->bindValue ( 'status', $status, \PDO::PARAM_STR );
		
		$res->execute ();
		
		return $res->fetchAll ();
	}
	
	
	public function getClientsGroupedByStatus() {
		$query = "
			select 

			month(t.created_at) as month,
			count(t.client_agreement_id) as count 
			
			from client_agreement t, client c
			
			where year(t.created_at) = year(now())
			and c.client_id = t.client_id
			and c.status_id = 1
			group by month(t.created_at)
		;
		";
		$res = $this->getEntityManager ()->getConnection ()->prepare ( $query );
		//$res->bindValue ( 'status', $status, \PDO::PARAM_STR );
		
		$res->execute ();
		
		return $res->fetchAll ();
	}
	
	
}

?>	

