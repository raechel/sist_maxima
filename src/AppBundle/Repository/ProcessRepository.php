<?php

namespace AppBundle\Repository;

use Doctrine\ORM\EntityRepository;


class ProcessRepository extends EntityRepository
{
	
	public function findOneByMd5Id($md5Id) {
		$query = "
			SELECT
		    u.process_list_id
		FROM
		    process_list u
		WHERE
		    md5(u.process_list_id) = :md5Id
		;
		";
		
		
		$res = $this->getEntityManager ()->getConnection ()->prepare ( $query );
		$res->bindValue ( 'md5Id', $md5Id, \PDO::PARAM_STR );
		
		$res->execute ();
		
		return $res->fetch ();
	}
	
	
	public function processStarter($orgId,$depId,$isGrouped = false)
	{
		$query = "SELECT 
					(select group_concat(department_id) from flow_department where flow_id = f.flow_id) as visible_for,
					find_in_set(:depId,(select group_concat(department_id) from flow_department where flow_id = f.flow_id)) as process_has_visibles,
					(select count(step_id) from step where flow_id = f.flow_id) as steps_count,
					p.name as process_name,
					f.name as flow_name,
					f.name,
					f.flow_id,
					f.description,
					p.process_list_id,
					d.name as owner_department_name					
						FROM process_list p, flow f, department d
							where f.process_list_id = p.process_list_id
								and d.department_id = p.department_id
								and p.organization_id = :orgId ";
		
		if($isGrouped)
		{
			$query .= " group by d.department_id "; 
		}						
		
		$res = $this->getEntityManager ()->getConnection ()->prepare ( $query );
		$res->bindValue ( 'orgId', $orgId, \PDO::PARAM_STR );
		$res->bindValue ( 'depId', $depId, \PDO::PARAM_STR );
		
		$res->execute ();
		
		return $res->fetchAll ();								
	}
	
	
	public function getList($orgId) {
		$query = "
			select 

			pl.process_list_id as processListId,
			pl.name,
			pl.description,
			pl.organization_id as organizationId,
			pl.department_id as departmentId,
			d.name as department,
			pl.created_at as createdAt,
			pl.created_by as createdBy,
			(select count(flow_id) from flow where process_list_id = pl.process_list_id) as processCount
			
			from process_list pl,department d
			
			where pl.organization_id = :orgId
			and d.department_id = pl.department_id
			
		;
		";
		
		
		$res = $this->getEntityManager ()->getConnection ()->prepare ( $query );
		$res->bindValue ( 'orgId', $orgId, \PDO::PARAM_STR );
		
		$res->execute ();
		
		return $res->fetchAll ();
	}
		
	
	

}

?>	

