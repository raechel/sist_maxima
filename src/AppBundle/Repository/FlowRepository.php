<?php

namespace AppBundle\Repository;

use Doctrine\ORM\EntityRepository;


class FlowRepository extends EntityRepository
{
	
	public function findOneByMd5Id($md5Id) {
		$query = "
			SELECT
		    u.flow_id
		FROM
		    flow u
		WHERE
		    md5(u.flow_id) = :md5Id
		;
		";
		
		
		$res = $this->getEntityManager ()->getConnection ()->prepare ( $query );
		$res->bindValue ( 'md5Id', $md5Id, \PDO::PARAM_STR );
		
		$res->execute ();
		
		return $res->fetch ();
	}
	
	
	public function getFlowsByPidAndOrg($pid,$orgId)
	{
		$query = "
			select 

				f.flow_id as flowId,
				f.name,
				f.description,
				f.organization_id as organizationId,
				f.process_list_id as processListId,
				f.created_at as createdAt,
				f.created_by as createdBy,
				(select count(step_id) from step where flow_id = f.flow_id) as stepCount
				
				from flow f 
				
				where f.process_list_id = :pid and f.organization_id = :orgId
		;
		";
		
		
		$res = $this->getEntityManager ()->getConnection ()->prepare ( $query );
		$res->bindValue ( 'pid', $pid, \PDO::PARAM_STR );
		$res->bindValue ( 'orgId', $orgId, \PDO::PARAM_STR );
		$res->execute ();
		
		return $res->fetchAll ();
	}
	
	public function getFlowDepartmentsByOrganization($id) {
		$query = "
			select fd.flow_id, d.name from flow_department fd, flow f, department d
				where f.flow_id = fd.flow_id
				and d.department_id = fd.department_id
				and f.organization_id = :id
		;
		";
		
		
		$res = $this->getEntityManager ()->getConnection ()->prepare ( $query );
		$res->bindValue ( 'id', $id, \PDO::PARAM_STR );
		
		$res->execute ();
		
		return $res->fetchAll ();
	}
	
}

?>	

