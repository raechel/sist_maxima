<?php

namespace AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use AppBundle\Entity\ClientAgreement;

class PaymentInvoiceType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
    	
        $builder->add('amount')
        ->add('discount')
        ->add('createdAt','text')
        ->add('client')
		->add('invoiceType')
		->add('service')
		->add('invoiceStatus')
        /*->add('service','entity',array(
	        	'mapped' => true,
	            'class' => 'AppBundle:Service',
	            'choice_value' => 'serviceId',
	            /*'choice_label' => 'name',*/
	            /*'empty_data' => false,
	            'empty_value' => "- Seleccione una opción -",
	            'required' => true,
	            'query_builder' => function(\Doctrine\ORM\EntityRepository $er) use ($options)
	            {
	            	
	                return $er->createQueryBuilder('u')->Where("u.organization in (".$options['organizationId'].")");
	                /*->orderBy("u.clientAgreementId", "ASC");*/
	            /*}
		))        */
        ->add('clientAgreement');        
    }
    
    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\Entity\PaymentInvoice',
            'clientAgreementList'=>null,
            'organizationId'=>null            
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'appbundle_paymentinvoice';
    }


}
