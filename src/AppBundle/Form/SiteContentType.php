<?php

namespace AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\FileType;

class SiteContentType extends AbstractType {

	/**
	 *
	 * {@inheritdoc}
	 *
	 */
	public function buildForm(FormBuilderInterface $builder, array $options) {
		$builder
            ->add ( 'title' )
            ->add ( 'description' )
            ->add ( 'content' )
            ->add ( 'imageFile', 'file' )
            ->add ( 'published', 'choice', 
                array(
                        'choices' => array(
                                "1" => "SI",
                                "0" => "NO"
                        )
                ))
            ->add ('publicationDate',
                'date',
                array(
                    'widget' => 'single_text',
                    'format' => 'y/M/d'
                )
            )
        ;
	}

	/**
	 *
	 * {@inheritdoc}
	 *
	 */
	public function configureOptions(OptionsResolver $resolver) {
		$resolver
->setDefaults ( array (
				'data_class' => 'AppBundle\Entity\SiteContent' 
		) );
	}

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'site_content';
    }


}
