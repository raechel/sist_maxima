<?php

namespace AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class UserType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('firstName')
            ->add('lastName')
            ->add('email')
			->add('username')
			->add('userRole')						
			->add('avatarPath','file',array('required'=>false,'data_class'=>null))
            ->add('password', 'password',array('always_empty'=>false,'required'=>false))
            ->add('status', 'choice', array('choices' => array("ACTIVO" => "ACTIVO", "INACTIVO" => "INACTIVO")))            		
            ->add('country')
        ;
    }
    
    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\Entity\User'
        ));
    }
	
	
	 /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'appbundle_user';
    }

	
}


