<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * PaymentInvoice
 */
class PaymentInvoice
{
    /**
     * @var integer
     */
    private $invoiceId;

    /**
     * @var string
     */
    private $amount;

    /**
     * @var string
     */
    private $discount;

     /**
     * @var string
     */
    private $voucherNumber;

    /**
     * @var \DateTime
     */
    private $createdAt;

    /**
     * @var \AppBundle\Entity\Client
     */
    private $client;

    /**
     * @var \AppBundle\Entity\Service
     */
    private $service;

    /**
     * @var \AppBundle\Entity\Organization
     */
    private $organization;

    /**
     * @var \AppBundle\Entity\ClientAgreement
     */
    private $clientAgreement;

    /**
     * @var \AppBundle\Entity\PaymentInvoiceStatus
     */
    private $invoiceStatus;


    /**
     * Get invoiceId
     *
     * @return integer 
     */
    public function getInvoiceId()
    {
        return $this->invoiceId;
    }

    /**
     * Set amount
     *
     * @param string $amount
     * @return PaymentInvoice
     */
    public function setAmount($amount)
    {
        $this->amount = $amount;
    
        return $this;
    }

    /**
     * Get amount
     *
     * @return string 
     */
    public function getAmount()
    {
        return $this->amount;
    }

    /**
     * Set voucherNumber
     *
     * @param string $voucherNumber
     * @return PaymentInvoice
     */
    public function setVoucherNumber($voucherNumber)
    {
        $this->voucherNumber = $voucherNumber;
    
        return $this;
    }

    /**
     * Get voucherNumber
     *
     * @return string 
     */
    public function getVoucherNumber()
    {
        return $this->voucherNumber;
    }

    /**
     * Set discount
     *
     * @param string $discount
     * @return PaymentInvoice
     */
    public function setDiscount($discount)
    {
        $this->discount = $discount;
    
        return $this;
    }

    /**
     * Get discount
     *
     * @return string 
     */
    public function getDiscount()
    {
        return $this->discount;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     * @return PaymentInvoice
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;
    
        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime 
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set client
     *
     * @param \AppBundle\Entity\Client $client
     * @return PaymentInvoice
     */
    public function setClient(\AppBundle\Entity\Client $client = null)
    {
        $this->client = $client;
    
        return $this;
    }

    /**
     * Get client
     *
     * @return \AppBundle\Entity\Client 
     */
    public function getClient()
    {
        return $this->client;
    }

    /**
     * Set service
     *
     * @param \AppBundle\Entity\Service $service
     * @return PaymentInvoice
     */
    public function setService(\AppBundle\Entity\Service $service = null)
    {
        $this->service = $service;
    
        return $this;
    }

    /**
     * Get service
     *
     * @return \AppBundle\Entity\Service 
     */
    public function getService()
    {
        return $this->service;
    }

    /**
     * Set organization
     *
     * @param \AppBundle\Entity\Organization $organization
     * @return PaymentInvoice
     */
    public function setOrganization(\AppBundle\Entity\Organization $organization = null)
    {
        $this->organization = $organization;
    
        return $this;
    }

    /**
     * Get organization
     *
     * @return \AppBundle\Entity\Organization 
     */
    public function getOrganization()
    {
        return $this->organization;
    }

    /**
     * Set clientAgreement
     *
     * @param \AppBundle\Entity\ClientAgreement $clientAgreement
     * @return PaymentInvoice
     */
    public function setClientAgreement(\AppBundle\Entity\ClientAgreement $clientAgreement = null)
    {
        $this->clientAgreement = $clientAgreement;
    
        return $this;
    }

    /**
     * Get clientAgreement
     *
     * @return \AppBundle\Entity\ClientAgreement 
     */
    public function getClientAgreement()
    {
        return $this->clientAgreement;
    }

    /**
     * Set invoiceStatus
     *
     * @param \AppBundle\Entity\PaymentInvoiceStatus $invoiceStatus
     * @return PaymentInvoice
     */
    public function setInvoiceStatus(\AppBundle\Entity\PaymentInvoiceStatus $invoiceStatus = null)
    {
        $this->invoiceStatus = $invoiceStatus;
    
        return $this;
    }

    /**
     * Get invoiceStatus
     *
     * @return \AppBundle\Entity\PaymentInvoiceStatus 
     */
    public function getInvoiceStatus()
    {
        return $this->invoiceStatus;
    }
    /**
     * @var integer
     */
    private $invoiceType;

    /**
     * @var string
     */
    private $customInvoice;


    /**
     * Set invoiceType
     *
     * @param integer $invoiceType
     * @return PaymentInvoice
     */
    public function setInvoiceType($invoiceType)
    {
        $this->invoiceType = $invoiceType;
    
        return $this;
    }

    /**
     * Get invoiceType
     *
     * @return integer 
     */
    public function getInvoiceType()
    {
        return $this->invoiceType;
    }

    /**
     * Set customInvoice
     *
     * @param string $customInvoice
     * @return PaymentInvoice
     */
    public function setCustomInvoice($customInvoice)
    {
        $this->customInvoice = $customInvoice;
    
        return $this;
    }

    /**
     * Get customInvoice
     *
     * @return string 
     */
    public function getCustomInvoice()
    {
        return $this->customInvoice;
    }
    /**
     * @var integer
     */
    private $paymentBy;

    /**
     * @var \DateTime
     */
    private $payedAt;


    /**
     * Set paymentBy
     *
     * @param integer $paymentBy
     * @return PaymentInvoice
     */
    public function setPaymentBy($paymentBy)
    {
        $this->paymentBy = $paymentBy;
    
        return $this;
    }

    /**
     * Get paymentBy
     *
     * @return integer 
     */
    public function getPaymentBy()
    {
        return $this->paymentBy;
    }

    /**
     * Set payedAt
     *
     * @param \DateTime $payedAt
     * @return PaymentInvoice
     */
    public function setPayedAt($payedAt)
    {
        $this->payedAt = $payedAt;
    
        return $this;
    }

    /**
     * Get payedAt
     *
     * @return \DateTime 
     */
    public function getPayedAt()
    {
        return $this->payedAt;
    }
    /**
     * @var \AppBundle\Entity\PaymentInvoiceCut
     */
    private $paymentInvoiceCut;


    /**
     * Set paymentInvoiceCut
     *
     * @param \AppBundle\Entity\PaymentInvoiceCut $paymentInvoiceCut
     * @return PaymentInvoice
     */
    public function setPaymentInvoiceCut(\AppBundle\Entity\PaymentInvoiceCut $paymentInvoiceCut = null)
    {
        $this->paymentInvoiceCut = $paymentInvoiceCut;
    
        return $this;
    }

    /**
     * Get paymentInvoiceCut
     *
     * @return \AppBundle\Entity\PaymentInvoiceCut 
     */
    public function getPaymentInvoiceCut()
    {
        return $this->paymentInvoiceCut;
    }
}
