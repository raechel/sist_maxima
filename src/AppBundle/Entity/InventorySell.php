<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * InventorySell
 */
class InventorySell
{
    /**
     * @var integer
     */
    private $inventorySellId;

    /**
     * @var string
     */
    private $sellPrice;

    /**
     * @var string
     */
    private $purchasePrice;

    /**
     * @var integer
     */
    private $quantity;

    /**
     * @var \DateTime
     */
    private $createdAt;

    /**
     * @var \AppBundle\Entity\User
     */
    private $createdBy;

    /**
     * @var \AppBundle\Entity\Inventory
     */
    private $inventory;

    /**
     * @var \AppBundle\Entity\Organization
     */
    private $organization;


    /**
     * Get inventorySellId
     *
     * @return integer 
     */
    public function getInventorySellId()
    {
        return $this->inventorySellId;
    }

    /**
     * Set sellPrice
     *
     * @param string $sellPrice
     * @return InventorySell
     */
    public function setSellPrice($sellPrice)
    {
        $this->sellPrice = $sellPrice;
    
        return $this;
    }

    /**
     * Get sellPrice
     *
     * @return string 
     */
    public function getSellPrice()
    {
        return $this->sellPrice;
    }

    /**
     * Set purchasePrice
     *
     * @param string $purchasePrice
     * @return InventorySell
     */
    public function setPurchasePrice($purchasePrice)
    {
        $this->purchasePrice = $purchasePrice;
    
        return $this;
    }

    /**
     * Get purchasePrice
     *
     * @return string 
     */
    public function getPurchasePrice()
    {
        return $this->purchasePrice;
    }

    /**
     * Set quantity
     *
     * @param integer $quantity
     * @return InventorySell
     */
    public function setQuantity($quantity)
    {
        $this->quantity = $quantity;
    
        return $this;
    }

    /**
     * Get quantity
     *
     * @return integer 
     */
    public function getQuantity()
    {
        return $this->quantity;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     * @return InventorySell
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;
    
        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime 
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set createdBy
     *
     * @param \AppBundle\Entity\User $createdBy
     * @return InventorySell
     */
    public function setCreatedBy(\AppBundle\Entity\User $createdBy = null)
    {
        $this->createdBy = $createdBy;
    
        return $this;
    }

    /**
     * Get createdBy
     *
     * @return \AppBundle\Entity\User 
     */
    public function getCreatedBy()
    {
        return $this->createdBy;
    }

    /**
     * Set inventory
     *
     * @param \AppBundle\Entity\Inventory $inventory
     * @return InventorySell
     */
    public function setInventory(\AppBundle\Entity\Inventory $inventory = null)
    {
        $this->inventory = $inventory;
    
        return $this;
    }

    /**
     * Get inventory
     *
     * @return \AppBundle\Entity\Inventory 
     */
    public function getInventory()
    {
        return $this->inventory;
    }

    /**
     * Set organization
     *
     * @param \AppBundle\Entity\Organization $organization
     * @return InventorySell
     */
    public function setOrganization(\AppBundle\Entity\Organization $organization = null)
    {
        $this->organization = $organization;
    
        return $this;
    }

    /**
     * Get organization
     *
     * @return \AppBundle\Entity\Organization 
     */
    public function getOrganization()
    {
        return $this->organization;
    }
    /**
     * @var string
     */
    private $discountTotal;

    /**
     * @var string
     */
    private $invoiceNumber;


    /**
     * Set discountTotal
     *
     * @param string $discountTotal
     * @return InventorySell
     */
    public function setDiscountTotal($discountTotal)
    {
        $this->discountTotal = $discountTotal;
    
        return $this;
    }

    /**
     * Get discountTotal
     *
     * @return string 
     */
    public function getDiscountTotal()
    {
        return $this->discountTotal;
    }

    /**
     * Set invoiceNumber
     *
     * @param string $invoiceNumber
     * @return InventorySell
     */
    public function setInvoiceNumber($invoiceNumber)
    {
        $this->invoiceNumber = $invoiceNumber;
    
        return $this;
    }

    /**
     * Get invoiceNumber
     *
     * @return string 
     */
    public function getInvoiceNumber()
    {
        return $this->invoiceNumber;
    }
    /**
     * @var string
     */
    private $grandTotal;


    /**
     * Set grandTotal
     *
     * @param string $grandTotal
     * @return InventorySell
     */
    public function setGrandTotal($grandTotal)
    {
        $this->grandTotal = $grandTotal;
    
        return $this;
    }

    /**
     * Get grandTotal
     *
     * @return string 
     */
    public function getGrandTotal()
    {
        return $this->grandTotal;
    }
}
