<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * FlowDepartment
 */
class FlowDepartment
{
    /**
     * @var integer
     */
    private $flowDepartmentId;

    /**
     * @var \AppBundle\Entity\Flow
     */
    private $flow;

    /**
     * @var \AppBundle\Entity\Department
     */
    private $department;


    /**
     * Get flowDepartmentId
     *
     * @return integer 
     */
    public function getFlowDepartmentId()
    {
        return $this->flowDepartmentId;
    }

    /**
     * Set flow
     *
     * @param \AppBundle\Entity\Flow $flow
     * @return FlowDepartment
     */
    public function setFlow(\AppBundle\Entity\Flow $flow = null)
    {
        $this->flow = $flow;
    
        return $this;
    }

    /**
     * Get flow
     *
     * @return \AppBundle\Entity\Flow 
     */
    public function getFlow()
    {
        return $this->flow;
    }

    /**
     * Set department
     *
     * @param \AppBundle\Entity\Department $department
     * @return FlowDepartment
     */
    public function setDepartment(\AppBundle\Entity\Department $department = null)
    {
        $this->department = $department;
    
        return $this;
    }

    /**
     * Get department
     *
     * @return \AppBundle\Entity\Department 
     */
    public function getDepartment()
    {
        return $this->department;
    }
}
