<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Flow
 */
class Flow
{
    /**
     * @var integer
     */
    private $flowId;

    /**
     * @var string
     */
    private $name;

    /**
     * @var string
     */
    private $description;

    /**
     * @var \DateTime
     */
    private $createdAt;

    /**
     * @var integer
     */
    private $createdBy;

    /**
     * @var \AppBundle\Entity\Organization
     */
    private $organization;

    /**
     * @var \AppBundle\Entity\ProcessList
     */
    private $processList;


	/**
     * Get string version
     *
     * @return string 
     */
    public function __toString()
    {
        return $this->name;
    }
	
	
	
    /**
     * Get flowId
     *
     * @return integer 
     */
    public function getFlowId()
    {
        return $this->flowId;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return Flow
     */
    public function setName($name)
    {
        $this->name = $name;
    
        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set description
     *
     * @param string $description
     * @return Flow
     */
    public function setDescription($description)
    {
        $this->description = $description;
    
        return $this;
    }

    /**
     * Get description
     *
     * @return string 
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     * @return Flow
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;
    
        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime 
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set createdBy
     *
     * @param integer $createdBy
     * @return Flow
     */
    public function setCreatedBy($createdBy)
    {
        $this->createdBy = $createdBy;
    
        return $this;
    }

    /**
     * Get createdBy
     *
     * @return integer 
     */
    public function getCreatedBy()
    {
        return $this->createdBy;
    }

    /**
     * Set organization
     *
     * @param \AppBundle\Entity\Organization $organization
     * @return Flow
     */
    public function setOrganization(\AppBundle\Entity\Organization $organization = null)
    {
        $this->organization = $organization;
    
        return $this;
    }

    /**
     * Get organization
     *
     * @return \AppBundle\Entity\Organization 
     */
    public function getOrganization()
    {
        return $this->organization;
    }

    /**
     * Set processList
     *
     * @param \AppBundle\Entity\ProcessList $processList
     * @return Flow
     */
    public function setProcessList(\AppBundle\Entity\ProcessList $processList = null)
    {
        $this->processList = $processList;
    
        return $this;
    }

    /**
     * Get processList
     *
     * @return \AppBundle\Entity\ProcessList 
     */
    public function getProcessList()
    {
        return $this->processList;
    }
}
