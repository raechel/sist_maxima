<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Supply
 */
class Supply
{
    /**
     * @var integer
     */
    private $supplyId;

    /**
     * @var string
     */
    private $name;

    /**
     * @var string
     */
    private $description;

    /**
     * @var integer
     */
    private $organizationId;

    /**
     * @var integer
     */
    private $clientAgreementId;

    /**
     * @var integer
     */
    private $clientId;

    /**
     * @var \DateTime
     */
    private $assignedAt;

    /**
     * @var integer
     */
    private $assignedBy;


    /**
     * toString
     *
     * @return string 
     */
    public function __toString()
    {
        
        return $this->name;
		  
    }	

    /**
     * Get supplyId
     *
     * @return integer 
     */
    public function getSupplyId()
    {
        return $this->supplyId;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return Supply
     */
    public function setName($name)
    {
        $this->name = $name;
    
        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set description
     *
     * @param string $description
     * @return Supply
     */
    public function setDescription($description)
    {
        $this->description = $description;
    
        return $this;
    }

    /**
     * Get description
     *
     * @return string 
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set organizationId
     *
     * @param integer $organizationId
     * @return Supply
     */
    public function setOrganizationId($organizationId)
    {
        $this->organizationId = $organizationId;
    
        return $this;
    }

    /**
     * Get organizationId
     *
     * @return integer 
     */
    public function getOrganizationId()
    {
        return $this->organizationId;
    }

    /**
     * Set clientAgreementId
     *
     * @param integer $clientAgreementId
     * @return Supply
     */
    public function setClientAgreementId($clientAgreementId)
    {
        $this->clientAgreementId = $clientAgreementId;
    
        return $this;
    }

    /**
     * Get clientAgreementId
     *
     * @return integer 
     */
    public function getClientAgreementId()
    {
        return $this->clientAgreementId;
    }

    /**
     * Set clientId
     *
     * @param integer $clientId
     * @return Supply
     */
    public function setClientId($clientId)
    {
        $this->clientId = $clientId;
    
        return $this;
    }

    /**
     * Get clientId
     *
     * @return integer 
     */
    public function getClientId()
    {
        return $this->clientId;
    }

    /**
     * Set assignedAt
     *
     * @param \DateTime $assignedAt
     * @return Supply
     */
    public function setAssignedAt($assignedAt)
    {
        $this->assignedAt = $assignedAt;
    
        return $this;
    }

    /**
     * Get assignedAt
     *
     * @return \DateTime 
     */
    public function getAssignedAt()
    {
        return $this->assignedAt;
    }

    /**
     * Set assignedBy
     *
     * @param integer $assignedBy
     * @return Supply
     */
    public function setAssignedBy($assignedBy)
    {
        $this->assignedBy = $assignedBy;
    
        return $this;
    }

    /**
     * Get assignedBy
     *
     * @return integer 
     */
    public function getAssignedBy()
    {
        return $this->assignedBy;
    }
    /**
     * @var \AppBundle\Entity\ClientAgreement
     */
    private $clientAgreement;

    /**
     * @var \AppBundle\Entity\Organization
     */
    private $organization;

    /**
     * @var \AppBundle\Entity\Client
     */
    private $client;


    /**
     * Set clientAgreement
     *
     * @param \AppBundle\Entity\ClientAgreement $clientAgreement
     * @return Supply
     */
    public function setClientAgreement(\AppBundle\Entity\ClientAgreement $clientAgreement = null)
    {
        $this->clientAgreement = $clientAgreement;
    
        return $this;
    }

    /**
     * Get clientAgreement
     *
     * @return \AppBundle\Entity\ClientAgreement 
     */
    public function getClientAgreement()
    {
        return $this->clientAgreement;
    }

    /**
     * Set organization
     *
     * @param \AppBundle\Entity\Organization $organization
     * @return Supply
     */
    public function setOrganization(\AppBundle\Entity\Organization $organization = null)
    {
        $this->organization = $organization;
    
        return $this;
    }

    /**
     * Get organization
     *
     * @return \AppBundle\Entity\Organization 
     */
    public function getOrganization()
    {
        return $this->organization;
    }

    /**
     * Set client
     *
     * @param \AppBundle\Entity\Client $client
     * @return Supply
     */
    public function setClient(\AppBundle\Entity\Client $client = null)
    {
        $this->client = $client;
    
        return $this;
    }

    /**
     * Get client
     *
     * @return \AppBundle\Entity\Client 
     */
    public function getClient()
    {
        return $this->client;
    }
    /**
     * @var string
     */
    private $fieldOne;

    /**
     * @var string
     */
    private $fieldTwo;

    /**
     * @var string
     */
    private $fieldThree;


    /**
     * Set fieldOne
     *
     * @param string $fieldOne
     * @return Supply
     */
    public function setFieldOne($fieldOne)
    {
        $this->fieldOne = $fieldOne;
    
        return $this;
    }

    /**
     * Get fieldOne
     *
     * @return string 
     */
    public function getFieldOne()
    {
        return $this->fieldOne;
    }

    /**
     * Set fieldTwo
     *
     * @param string $fieldTwo
     * @return Supply
     */
    public function setFieldTwo($fieldTwo)
    {
        $this->fieldTwo = $fieldTwo;
    
        return $this;
    }

    /**
     * Get fieldTwo
     *
     * @return string 
     */
    public function getFieldTwo()
    {
        return $this->fieldTwo;
    }

    /**
     * Set fieldThree
     *
     * @param string $fieldThree
     * @return Supply
     */
    public function setFieldThree($fieldThree)
    {
        $this->fieldThree = $fieldThree;
    
        return $this;
    }

    /**
     * Get fieldThree
     *
     * @return string 
     */
    public function getFieldThree()
    {
        return $this->fieldThree;
    }
}
