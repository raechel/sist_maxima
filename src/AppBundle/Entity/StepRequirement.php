<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * StepRequirement
 */
class StepRequirement
{
    /**
     * @var integer
     */
    private $stepRequirementId;

    /**
     * @var string
     */
    private $name;

    /**
     * @var string
     */
    private $description;

    /**
     * @var \DateTime
     */
    private $createdAt;

    /**
     * @var integer
     */
    private $createdBy;

    /**
     * @var \AppBundle\Entity\Department
     */
    private $department;

    /**
     * @var \AppBundle\Entity\Organization
     */
    private $organization;

    /**
     * @var \AppBundle\Entity\Position
     */
    private $position;

    /**
     * @var \AppBundle\Entity\Step
     */
    private $step;

    /**
     * @var \AppBundle\Entity\RequirementType
     */
    private $requirementType;


    /**
     * Get stepRequirementId
     *
     * @return integer 
     */
    public function getStepRequirementId()
    {
        return $this->stepRequirementId;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return StepRequirement
     */
    public function setName($name)
    {
        $this->name = $name;
    
        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set description
     *
     * @param string $description
     * @return StepRequirement
     */
    public function setDescription($description)
    {
        $this->description = $description;
    
        return $this;
    }

    /**
     * Get description
     *
     * @return string 
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     * @return StepRequirement
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;
    
        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime 
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set createdBy
     *
     * @param integer $createdBy
     * @return StepRequirement
     */
    public function setCreatedBy($createdBy)
    {
        $this->createdBy = $createdBy;
    
        return $this;
    }

    /**
     * Get createdBy
     *
     * @return integer 
     */
    public function getCreatedBy()
    {
        return $this->createdBy;
    }

    /**
     * Set department
     *
     * @param \AppBundle\Entity\Department $department
     * @return StepRequirement
     */
    public function setDepartment(\AppBundle\Entity\Department $department = null)
    {
        $this->department = $department;
    
        return $this;
    }

    /**
     * Get department
     *
     * @return \AppBundle\Entity\Department 
     */
    public function getDepartment()
    {
        return $this->department;
    }

    /**
     * Set organization
     *
     * @param \AppBundle\Entity\Organization $organization
     * @return StepRequirement
     */
    public function setOrganization(\AppBundle\Entity\Organization $organization = null)
    {
        $this->organization = $organization;
    
        return $this;
    }

    /**
     * Get organization
     *
     * @return \AppBundle\Entity\Organization 
     */
    public function getOrganization()
    {
        return $this->organization;
    }

    /**
     * Set position
     *
     * @param \AppBundle\Entity\Position $position
     * @return StepRequirement
     */
    public function setPosition(\AppBundle\Entity\Position $position = null)
    {
        $this->position = $position;
    
        return $this;
    }

    /**
     * Get position
     *
     * @return \AppBundle\Entity\Position 
     */
    public function getPosition()
    {
        return $this->position;
    }

    /**
     * Set step
     *
     * @param \AppBundle\Entity\Step $step
     * @return StepRequirement
     */
    public function setStep(\AppBundle\Entity\Step $step = null)
    {
        $this->step = $step;
    
        return $this;
    }

    /**
     * Get step
     *
     * @return \AppBundle\Entity\Step 
     */
    public function getStep()
    {
        return $this->step;
    }

    /**
     * Set requirementType
     *
     * @param \AppBundle\Entity\RequirementType $requirementType
     * @return StepRequirement
     */
    public function setRequirementType(\AppBundle\Entity\RequirementType $requirementType = null)
    {
        $this->requirementType = $requirementType;
    
        return $this;
    }

    /**
     * Get requirementType
     *
     * @return \AppBundle\Entity\RequirementType 
     */
    public function getRequirementType()
    {
        return $this->requirementType;
    }
    /**
     * @var integer
     */
    private $weight;


    /**
     * Set weight
     *
     * @param integer $weight
     * @return StepRequirement
     */
    public function setWeight($weight)
    {
        $this->weight = $weight;
    
        return $this;
    }

    /**
     * Get weight
     *
     * @return integer 
     */
    public function getWeight()
    {
        return $this->weight;
    }
    /**
     * @var integer
     */
    private $isPersonal;


    /**
     * Set isPersonal
     *
     * @param integer $isPersonal
     * @return StepRequirement
     */
    public function setIsPersonal($isPersonal)
    {
        $this->isPersonal = $isPersonal;
    
        return $this;
    }

    /**
     * Get isPersonal
     *
     * @return integer 
     */
    public function getIsPersonal()
    {
        return $this->isPersonal;
    }
    /**
     * @var integer
     */
    private $bossMustApprove;


    /**
     * Set bossMustApprove
     *
     * @param integer $bossMustApprove
     * @return StepRequirement
     */
    public function setBossMustApprove($bossMustApprove)
    {
        $this->bossMustApprove = $bossMustApprove;
    
        return $this;
    }

    /**
     * Get bossMustApprove
     *
     * @return integer 
     */
    public function getBossMustApprove()
    {
        return $this->bossMustApprove;
    }
}
