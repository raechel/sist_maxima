<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * UserStudent
 */
class UserStudent
{
    /**
     * @var integer
     */
    private $userStudentId;

    /**
     * @var \DateTime
     */
    private $createdAt;

    /**
     * @var \AppBundle\Entity\User
     */
    private $user;

    /**
     * @var \AppBundle\Entity\User
     */
    private $student;


    /**
     * Get userStudentId
     *
     * @return integer 
     */
    public function getUserStudentId()
    {
        return $this->userStudentId;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     * @return UserStudent
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;
    
        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime 
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set user
     *
     * @param \AppBundle\Entity\User $user
     * @return UserStudent
     */
    public function setUser(\AppBundle\Entity\User $user = null)
    {
        $this->user = $user;
    
        return $this;
    }

    /**
     * Get user
     *
     * @return \AppBundle\Entity\User 
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Set student
     *
     * @param \AppBundle\Entity\User $student
     * @return UserStudent
     */
    public function setStudent(\AppBundle\Entity\User $student = null)
    {
        $this->student = $student;
    
        return $this;
    }

    /**
     * Get student
     *
     * @return \AppBundle\Entity\User 
     */
    public function getStudent()
    {
        return $this->student;
    }
}
