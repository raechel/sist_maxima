<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Position
 */
class Position
{
    /**
     * @var integer
     */
    private $positionId;

    /**
     * @var string
     */
    private $name;

    /**
     * @var string
     */
    private $description;

    /**
     * @var integer
     */
    private $weight;

    /**
     * @var \DateTime
     */
    private $createdAt;

    /**
     * @var integer
     */
    private $createdBy;

    /**
     * @var \AppBundle\Entity\organization
     */
    private $Organization;

    /**
     * @var \AppBundle\Entity\department
     */
    private $Department;


    /**
     * Get string version
     *
     * @return string 
     */
    public function __toString()
    {
        return $this->name;
    }
	
	
    /**
     * Get positionId
     *
     * @return integer 
     */
    public function getPositionId()
    {
        return $this->positionId;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return Position
     */
    public function setName($name)
    {
        $this->name = $name;
    
        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set description
     *
     * @param string $description
     * @return Position
     */
    public function setDescription($description)
    {
        $this->description = $description;
    
        return $this;
    }

    /**
     * Get description
     *
     * @return string 
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set weight
     *
     * @param integer $weight
     * @return Position
     */
    public function setWeight($weight)
    {
        $this->weight = $weight;
    
        return $this;
    }

    /**
     * Get weight
     *
     * @return integer 
     */
    public function getWeight()
    {
        return $this->weight;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     * @return Position
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;
    
        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime 
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set createdBy
     *
     * @param integer $createdBy
     * @return Position
     */
    public function setCreatedBy($createdBy)
    {
        $this->createdBy = $createdBy;
    
        return $this;
    }

    /**
     * Get createdBy
     *
     * @return integer 
     */
    public function getCreatedBy()
    {
        return $this->createdBy;
    }

    /**
     * Set Organization
     *
     * @param \AppBundle\Entity\organization $organization
     * @return Position
     */
    public function setOrganization(\AppBundle\Entity\organization $organization = null)
    {
        $this->Organization = $organization;
    
        return $this;
    }

    /**
     * Get Organization
     *
     * @return \AppBundle\Entity\organization 
     */
    public function getOrganization()
    {
        return $this->Organization;
    }

    /**
     * Set Department
     *
     * @param \AppBundle\Entity\department $department
     * @return Position
     */
    public function setDepartment(\AppBundle\Entity\department $department = null)
    {
        $this->Department = $department;
    
        return $this;
    }

    /**
     * Get Department
     *
     * @return \AppBundle\Entity\department 
     */
    public function getDepartment()
    {
        return $this->Department;
    }
    /**
     * @var integer
     */
    private $parentPositionId;


    /**
     * Set parentPositionId
     *
     * @param integer $parentPositionId
     * @return Position
     */
    public function setParentPositionId($parentPositionId)
    {
        $this->parentPositionId = $parentPositionId;
    
        return $this;
    }

    /**
     * Get parentPositionId
     *
     * @return integer 
     */
    public function getParentPositionId()
    {
        return $this->parentPositionId;
    }
}
