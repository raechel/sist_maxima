<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * PaymentInvoiceStatus
 */
class PaymentInvoiceStatus
{
    /**
     * @var integer
     */
    private $invoiceStatusId;

    /**
     * @var string
     */
    private $name;

    /**
     * @var string
     */
    private $description;


    /**
     * toString
     *
     * @return string 
     */
    public function __toString()
    {
        
        return $this->name;
		  
    }
	

    /**
     * Get invoiceStatusId
     *
     * @return integer 
     */
    public function getInvoiceStatusId()
    {
        return $this->invoiceStatusId;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return PaymentInvoiceStatus
     */
    public function setName($name)
    {
        $this->name = $name;
    
        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set description
     *
     * @param string $description
     * @return PaymentInvoiceStatus
     */
    public function setDescription($description)
    {
        $this->description = $description;
    
        return $this;
    }

    /**
     * Get description
     *
     * @return string 
     */
    public function getDescription()
    {
        return $this->description;
    }
}
