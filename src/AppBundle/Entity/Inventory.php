<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Inventory
 */
class Inventory
{
    /**
     * @var integer
     */
    private $inventoryId;

    /**
     * @var string
     */
    private $name;

    /**
     * @var string
     */
    private $description;

    /**
     * @var string
     */
    private $imagePath;

    /**
     * @var string
     */
    private $purchasePrice;

    /**
     * @var string
     */
    private $sellPrice;

    /**
     * @var integer
     */
    private $isActive;

    /**
     * @var \DateTime
     */
    private $createdAt;

    /**
     * @var integer
     */
    private $createdBy;

    /**
     * @var \AppBundle\Entity\Organization
     */
    private $organization;

    /**
     * @var \AppBundle\Entity\MeasureType
     */
    private $measureType;


    /**
     * toString
     *
     * @return string 
     */
    public function __toString()
    {
        
        return $this->name;
		  
    }
	

    /**
     * Get inventoryId
     *
     * @return integer 
     */
    public function getInventoryId()
    {
        return $this->inventoryId;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return Inventory
     */
    public function setName($name)
    {
        $this->name = $name;
    
        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set description
     *
     * @param string $description
     * @return Inventory
     */
    public function setDescription($description)
    {
        $this->description = $description;
    
        return $this;
    }

    /**
     * Get description
     *
     * @return string 
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set imagePath
     *
     * @param string $imagePath
     * @return Inventory
     */
    public function setImagePath($imagePath)
    {
        $this->imagePath = $imagePath;
    
        return $this;
    }

    /**
     * Get imagePath
     *
     * @return string 
     */
    public function getImagePath()
    {
        return $this->imagePath;
    }

    /**
     * Set purchasePrice
     *
     * @param string $purchasePrice
     * @return Inventory
     */
    public function setPurchasePrice($purchasePrice)
    {
        $this->purchasePrice = $purchasePrice;
    
        return $this;
    }

    /**
     * Get purchasePrice
     *
     * @return string 
     */
    public function getPurchasePrice()
    {
        return $this->purchasePrice;
    }

    /**
     * Set sellPrice
     *
     * @param string $sellPrice
     * @return Inventory
     */
    public function setSellPrice($sellPrice)
    {
        $this->sellPrice = $sellPrice;
    
        return $this;
    }

    /**
     * Get sellPrice
     *
     * @return string 
     */
    public function getSellPrice()
    {
        return $this->sellPrice;
    }

    /**
     * Set isActive
     *
     * @param integer $isActive
     * @return Inventory
     */
    public function setIsActive($isActive)
    {
        $this->isActive = $isActive;
    
        return $this;
    }

    /**
     * Get isActive
     *
     * @return integer 
     */
    public function getIsActive()
    {
        return $this->isActive;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     * @return Inventory
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;
    
        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime 
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set createdBy
     *
     * @param integer $createdBy
     * @return Inventory
     */
    public function setCreatedBy($createdBy)
    {
        $this->createdBy = $createdBy;
    
        return $this;
    }

    /**
     * Get createdBy
     *
     * @return integer 
     */
    public function getCreatedBy()
    {
        return $this->createdBy;
    }

    /**
     * Set organization
     *
     * @param \AppBundle\Entity\Organization $organization
     * @return Inventory
     */
    public function setOrganization(\AppBundle\Entity\Organization $organization = null)
    {
        $this->organization = $organization;
    
        return $this;
    }

    /**
     * Get organization
     *
     * @return \AppBundle\Entity\Organization 
     */
    public function getOrganization()
    {
        return $this->organization;
    }

    /**
     * Set measureType
     *
     * @param \AppBundle\Entity\MeasureType $measureType
     * @return Inventory
     */
    public function setMeasureType(\AppBundle\Entity\MeasureType $measureType = null)
    {
        $this->measureType = $measureType;
    
        return $this;
    }

    /**
     * Get measureType
     *
     * @return \AppBundle\Entity\MeasureType 
     */
    public function getMeasureType()
    {
        return $this->measureType;
    }
    /**
     * @var string
     */
    private $customCode;


    /**
     * Set customCode
     *
     * @param string $customCode
     * @return Inventory
     */
    public function setCustomCode($customCode)
    {
        $this->customCode = $customCode;
    
        return $this;
    }

    /**
     * Get customCode
     *
     * @return string 
     */
    public function getCustomCode()
    {
        return $this->customCode;
    }
}
