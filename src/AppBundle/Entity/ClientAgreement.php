<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * ClientAgreement
 */
class ClientAgreement
{
    /**
     * @var integer
     */
    private $clientAgreementId;

    /**
     * @var string
     */
    private $accountName;

    /**
     * @var \DateTime
     */
    private $createdAt;

    /**
     * @var integer
     */
    private $createdBy;

    /**
     * @var \AppBundle\Entity\Service
     */
    private $service;

    /**
     * @var \AppBundle\Entity\Client
     */
    private $client;


    /**
     * Get clientAgreementId
     *
     * @return integer 
     */
    public function getClientAgreementId()
    {
        return $this->clientAgreementId;
    }


    /**
     * toString
     *
     * @return string 
     */
    public function __toString()
    {
        
        return $this->accountName;
		  
    }	
	
    /**
     * Set accountName
     *
     * @param string $accountName
     * @return ClientAgreement
     */
    public function setAccountName($accountName)
    {
        $this->accountName = $accountName;
    
        return $this;
    }

    /**
     * Get accountName
     *
     * @return string 
     */
    public function getAccountName()
    {
        return $this->accountName;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     * @return ClientAgreement
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;
    
        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime 
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set createdBy
     *
     * @param integer $createdBy
     * @return ClientAgreement
     */
    public function setCreatedBy($createdBy)
    {
        $this->createdBy = $createdBy;
    
        return $this;
    }

    /**
     * Get createdBy
     *
     * @return integer 
     */
    public function getCreatedBy()
    {
        return $this->createdBy;
    }

    /**
     * Set service
     *
     * @param \AppBundle\Entity\Service $service
     * @return ClientAgreement
     */
    public function setService(\AppBundle\Entity\Service $service = null)
    {
        $this->service = $service;
    
        return $this;
    }

    /**
     * Get service
     *
     * @return \AppBundle\Entity\Service 
     */
    public function getService()
    {
        return $this->service;
    }

    /**
     * Set client
     *
     * @param \AppBundle\Entity\Client $client
     * @return ClientAgreement
     */
    public function setClient(\AppBundle\Entity\Client $client = null)
    {
        $this->client = $client;
    
        return $this;
    }

    /**
     * Get client
     *
     * @return \AppBundle\Entity\Client 
     */
    public function getClient()
    {
        return $this->client;
    }
    /**
     * @var integer
     */
    private $statusId;

    /**
     * @var \DateTime
     */
    private $updatedAt;

    /**
     * @var integer
     */
    private $updatedBy;


    /**
     * Set statusId
     *
     * @param integer $statusId
     * @return ClientAgreement
     */
    public function setStatusId($statusId)
    {
        $this->statusId = $statusId;
    
        return $this;
    }

    /**
     * Get statusId
     *
     * @return integer 
     */
    public function getStatusId()
    {
        return $this->statusId;
    }

    /**
     * Set updatedAt
     *
     * @param \DateTime $updatedAt
     * @return ClientAgreement
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;
    
        return $this;
    }

    /**
     * Get updatedAt
     *
     * @return \DateTime 
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * Set updatedBy
     *
     * @param integer $updatedBy
     * @return ClientAgreement
     */
    public function setUpdatedBy($updatedBy)
    {
        $this->updatedBy = $updatedBy;
    
        return $this;
    }

    /**
     * Get updatedBy
     *
     * @return integer 
     */
    public function getUpdatedBy()
    {
        return $this->updatedBy;
    }
    /**
     * @var string
     */
    private $discount;


    /**
     * Set discount
     *
     * @param string $discount
     * @return ClientAgreement
     */
    public function setDiscount($discount)
    {
        $this->discount = $discount;
    
        return $this;
    }

    /**
     * Get discount
     *
     * @return string 
     */
    public function getDiscount()
    {
        return $this->discount;
    }
    /**
     * @var \DateTime
     */
    private $agreementDate;


    /**
     * Set agreementDate
     *
     * @param \DateTime $agreementDate
     * @return ClientAgreement
     */
    public function setAgreementDate($agreementDate)
    {
        $this->agreementDate = $agreementDate;
    
        return $this;
    }

    /**
     * Get agreementDate
     *
     * @return \DateTime 
     */
    public function getAgreementDate()
    {
        return $this->agreementDate;
    }
    /**
     * @var string
     */
    private $latLng;


    /**
     * Set latLng
     *
     * @param string $latLng
     * @return ClientAgreement
     */
    public function setLatLng($latLng)
    {
        $this->latLng = $latLng;
    
        return $this;
    }

    /**
     * Get latLng
     *
     * @return string 
     */
    public function getLatLng()
    {
        return $this->latLng;
    }
}
