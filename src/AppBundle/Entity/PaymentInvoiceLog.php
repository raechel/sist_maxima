<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * PaymentInvoiceLog
 */
class PaymentInvoiceLog
{
    /**
     * @var integer
     */
    private $paymentInvoiceLogId;

    /**
     * @var integer
     */
    private $logType;

    /**
     * @var string
     */
    private $comments;

    /**
     * @var \DateTime
     */
    private $createdAt;

    /**
     * @var \AppBundle\Entity\User
     */
    private $createdBy;

    /**
     * @var \AppBundle\Entity\Client
     */
    private $client;

    /**
     * @var \AppBundle\Entity\PaymentInvoice
     */
    private $paymentInvoice;


    /**
     * Get paymentInvoiceLogId
     *
     * @return integer 
     */
    public function getPaymentInvoiceLogId()
    {
        return $this->paymentInvoiceLogId;
    }

    /**
     * Set logType
     *
     * @param integer $logType
     * @return PaymentInvoiceLog
     */
    public function setLogType($logType)
    {
        $this->logType = $logType;
    
        return $this;
    }

    /**
     * Get logType
     *
     * @return integer 
     */
    public function getLogType()
    {
        return $this->logType;
    }

    /**
     * Set comments
     *
     * @param string $comments
     * @return PaymentInvoiceLog
     */
    public function setComments($comments)
    {
        $this->comments = $comments;
    
        return $this;
    }

    /**
     * Get comments
     *
     * @return string 
     */
    public function getComments()
    {
        return $this->comments;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     * @return PaymentInvoiceLog
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;
    
        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime 
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set createdBy
     *
     * @param \AppBundle\Entity\User $createdBy
     * @return PaymentInvoiceLog
     */
    public function setCreatedBy(\AppBundle\Entity\User $createdBy = null)
    {
        $this->createdBy = $createdBy;
    
        return $this;
    }

    /**
     * Get createdBy
     *
     * @return \AppBundle\Entity\User 
     */
    public function getCreatedBy()
    {
        return $this->createdBy;
    }

    /**
     * Set client
     *
     * @param \AppBundle\Entity\Client $client
     * @return PaymentInvoiceLog
     */
    public function setClient(\AppBundle\Entity\Client $client = null)
    {
        $this->client = $client;
    
        return $this;
    }

    /**
     * Get client
     *
     * @return \AppBundle\Entity\Client 
     */
    public function getClient()
    {
        return $this->client;
    }

    /**
     * Set paymentInvoice
     *
     * @param \AppBundle\Entity\PaymentInvoice $paymentInvoice
     * @return PaymentInvoiceLog
     */
    public function setPaymentInvoice(\AppBundle\Entity\PaymentInvoice $paymentInvoice = null)
    {
        $this->paymentInvoice = $paymentInvoice;
    
        return $this;
    }

    /**
     * Get paymentInvoice
     *
     * @return \AppBundle\Entity\PaymentInvoice 
     */
    public function getPaymentInvoice()
    {
        return $this->paymentInvoice;
    }
}
