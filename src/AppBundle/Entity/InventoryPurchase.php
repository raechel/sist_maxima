<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * InventoryPurchase
 */
class InventoryPurchase
{
    /**
     * @var integer
     */
    private $inventoryPurchaseId;

    /**
     * @var integer
     */
    private $quantity;

    /**
     * @var string
     */
    private $invoiceNumber;

    /**
     * @var \DateTime
     */
    private $createdAt;

    /**
     * @var integer
     */
    private $createdBy;

    /**
     * @var \AppBundle\Entity\Organization
     */
    private $organization;

    /**
     * @var \AppBundle\Entity\Inventory
     */
    private $inventory;


    /**
     * Get inventoryPurchaseId
     *
     * @return integer 
     */
    public function getInventoryPurchaseId()
    {
        return $this->inventoryPurchaseId;
    }

    /**
     * Set quantity
     *
     * @param integer $quantity
     * @return InventoryPurchase
     */
    public function setQuantity($quantity)
    {
        $this->quantity = $quantity;
    
        return $this;
    }

    /**
     * Get quantity
     *
     * @return integer 
     */
    public function getQuantity()
    {
        return $this->quantity;
    }

    /**
     * Set invoiceNumber
     *
     * @param string $invoiceNumber
     * @return InventoryPurchase
     */
    public function setInvoiceNumber($invoiceNumber)
    {
        $this->invoiceNumber = $invoiceNumber;
    
        return $this;
    }

    /**
     * Get invoiceNumber
     *
     * @return string 
     */
    public function getInvoiceNumber()
    {
        return $this->invoiceNumber;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     * @return InventoryPurchase
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;
    
        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime 
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set createdBy
     *
     * @param integer $createdBy
     * @return InventoryPurchase
     */
    public function setCreatedBy($createdBy)
    {
        $this->createdBy = $createdBy;
    
        return $this;
    }

    /**
     * Get createdBy
     *
     * @return integer 
     */
    public function getCreatedBy()
    {
        return $this->createdBy;
    }

    /**
     * Set organization
     *
     * @param \AppBundle\Entity\Organization $organization
     * @return InventoryPurchase
     */
    public function setOrganization(\AppBundle\Entity\Organization $organization = null)
    {
        $this->organization = $organization;
    
        return $this;
    }

    /**
     * Get organization
     *
     * @return \AppBundle\Entity\Organization 
     */
    public function getOrganization()
    {
        return $this->organization;
    }

    /**
     * Set inventory
     *
     * @param \AppBundle\Entity\Inventory $inventory
     * @return InventoryPurchase
     */
    public function setInventory(\AppBundle\Entity\Inventory $inventory = null)
    {
        $this->inventory = $inventory;
    
        return $this;
    }

    /**
     * Get inventory
     *
     * @return \AppBundle\Entity\Inventory 
     */
    public function getInventory()
    {
        return $this->inventory;
    }
    /**
     * @var string
     */
    private $moneyAmount;


    /**
     * Set moneyAmount
     *
     * @param string $moneyAmount
     * @return InventoryPurchase
     */
    public function setMoneyAmount($moneyAmount)
    {
        $this->moneyAmount = $moneyAmount;
    
        return $this;
    }

    /**
     * Get moneyAmount
     *
     * @return string 
     */
    public function getMoneyAmount()
    {
        return $this->moneyAmount;
    }
    /**
     * @var string
     */
    private $description;

    /**
     * @var integer
     */
    private $purchaseType;


    /**
     * Set description
     *
     * @param string $description
     * @return InventoryPurchase
     */
    public function setDescription($description)
    {
        $this->description = $description;
    
        return $this;
    }

    /**
     * Get description
     *
     * @return string 
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set purchaseType
     *
     * @param integer $purchaseType
     * @return InventoryPurchase
     */
    public function setPurchaseType($purchaseType)
    {
        $this->purchaseType = $purchaseType;
    
        return $this;
    }

    /**
     * Get purchaseType
     *
     * @return integer 
     */
    public function getPurchaseType()
    {
        return $this->purchaseType;
    }
}
