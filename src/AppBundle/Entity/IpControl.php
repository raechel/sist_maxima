<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * IpControl
 */
class IpControl
{
    /**
     * @var integer
     */
    private $ipControlId;

    /**
     * @var string
     */
    private $ipNumber;

    /**
     * @var integer
     */
    private $clientAgreementId;

    /**
     * @var \DateTime
     */
    private $createdAt;

    /**
     * @var \DateTime
     */
    private $assignedAt;

    /**
     * @var integer
     */
    private $createdBy;


    /**
     * Get ipControlId
     *
     * @return integer 
     */
    public function getIpControlId()
    {
        return $this->ipControlId;
    }

    /**
     * Set ipNumber
     *
     * @param string $ipNumber
     * @return IpControl
     */
    public function setIpNumber($ipNumber)
    {
        $this->ipNumber = $ipNumber;
    
        return $this;
    }

    /**
     * Get ipNumber
     *
     * @return string 
     */
    public function getIpNumber()
    {
        return $this->ipNumber;
    }

    /**
     * Set clientAgreementId
     *
     * @param integer $clientAgreementId
     * @return IpControl
     */
    public function setClientAgreementId($clientAgreementId)
    {
        $this->clientAgreementId = $clientAgreementId;
    
        return $this;
    }

    /**
     * Get clientAgreementId
     *
     * @return integer 
     */
    public function getClientAgreementId()
    {
        return $this->clientAgreementId;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     * @return IpControl
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;
    
        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime 
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set assignedAt
     *
     * @param \DateTime $assignedAt
     * @return IpControl
     */
    public function setAssignedAt($assignedAt)
    {
        $this->assignedAt = $assignedAt;
    
        return $this;
    }

    /**
     * Get assignedAt
     *
     * @return \DateTime 
     */
    public function getAssignedAt()
    {
        return $this->assignedAt;
    }

    /**
     * Set createdBy
     *
     * @param integer $createdBy
     * @return IpControl
     */
    public function setCreatedBy($createdBy)
    {
        $this->createdBy = $createdBy;
    
        return $this;
    }

    /**
     * Get createdBy
     *
     * @return integer 
     */
    public function getCreatedBy()
    {
        return $this->createdBy;
    }
    /**
     * @var \AppBundle\Entity\ClientAgreement
     */
    private $clientAgreement;


    /**
     * Set clientAgreement
     *
     * @param \AppBundle\Entity\ClientAgreement $clientAgreement
     * @return IpControl
     */
    public function setClientAgreement(\AppBundle\Entity\ClientAgreement $clientAgreement = null)
    {
        $this->clientAgreement = $clientAgreement;
    
        return $this;
    }

    /**
     * Get clientAgreement
     *
     * @return \AppBundle\Entity\ClientAgreement 
     */
    public function getClientAgreement()
    {
        return $this->clientAgreement;
    }
}
