<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Platform
 */
class Platform
{
    /**
     * @var integer
     */
    private $platformId;

    /**
     * @var string
     */
    private $name;


    /**
     * Get platformId
     *
     * @return integer 
     */
    public function getPlatformId()
    {
        return $this->platformId;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return Platform
     */
    public function setName($name)
    {
        $this->name = $name;
    
        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }
}
