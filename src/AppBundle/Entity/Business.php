<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Business
 */
class Business
{
    /**
     * @var integer
     */
    private $businessId;

    /**
     * @var string
     */
    private $name;

    /**
     * @var \DateTime
     */
    private $createdAt;

    /**
     * @var integer
     */
    private $createdBy;

    /**
     * @var \AppBundle\Entity\BusinessType
     */
    private $businessType;




    /**
     * Get string version
     *
     * @return string 
     */
    public function __toString()
    {
        return $this->name;
    }
	
	

    /**
     * Get businessId
     *
     * @return integer 
     */
    public function getBusinessId()
    {
        return $this->businessId;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return Business
     */
    public function setName($name)
    {
        $this->name = $name;
    
        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     * @return Business
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;
    
        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime 
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set createdBy
     *
     * @param integer $createdBy
     * @return Business
     */
    public function setCreatedBy($createdBy)
    {
        $this->createdBy = $createdBy;
    
        return $this;
    }

    /**
     * Get createdBy
     *
     * @return integer 
     */
    public function getCreatedBy()
    {
        return $this->createdBy;
    }

    /**
     * Set businessType
     *
     * @param \AppBundle\Entity\BusinessType $businessType
     * @return Business
     */
    public function setBusinessType(\AppBundle\Entity\BusinessType $businessType = null)
    {
        $this->businessType = $businessType;
    
        return $this;
    }

    /**
     * Get businessType
     *
     * @return \AppBundle\Entity\BusinessType 
     */
    public function getBusinessType()
    {
        return $this->businessType;
    }
    /**
     * @var \AppBundle\Entity\organization
     */
    private $Organization;


    /**
     * Set Organization
     *
     * @param \AppBundle\Entity\organization $organization
     * @return Business
     */
    public function setOrganization(\AppBundle\Entity\organization $organization = null)
    {
        $this->Organization = $organization;
    
        return $this;
    }

    /**
     * Get Organization
     *
     * @return \AppBundle\Entity\organization 
     */
    public function getOrganization()
    {
        return $this->Organization;
    }
}
