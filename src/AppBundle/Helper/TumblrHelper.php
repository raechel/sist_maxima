<?php
namespace AppBundle\Helper;

class TumblrHelper
{

	public function getAllPosts($dir)
	{
		
		$url_json = $dir."functions/json/main.json";
		$json = json_decode(file_get_contents($url_json),true);
		
		return $json['response']['posts'];		
	}
    
    
}
?>