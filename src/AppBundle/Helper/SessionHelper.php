<?php
namespace AppBundle\Helper;

class SessionHelper
{
	
    public function open_url($url = "", $parameters = false) {
        $ch = curl_init ();
        curl_setopt ( $ch, CURLOPT_SSL_VERIFYPEER, false );
        curl_setopt ( $ch, CURLOPT_SSL_VERIFYHOST, false );
        curl_setopt ( $ch, CURLOPT_URL, $url );
        curl_setopt ( $ch, CURLOPT_HEADER, 0 );
        if ($parameters) {
            curl_setopt ( $ch, CURLOPT_POST, true);
            curl_setopt ( $ch, CURLOPT_POSTFIELDS, $parameters);
        }
        
        ob_start ();
        curl_exec ( $ch );
        curl_close ( $ch );
        $string = ob_get_contents ();
        ob_end_clean ();
        return $string;
    }
    
    
	
    
    /**
     * Opens a URL by post method
     *
     * @param string $url url to consume
     * @param array $params parameters for url
     * @return string response given by consuming url
     */
    public function openPostUrl($url, $params){
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $params);
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        $result = curl_exec($ch);
        curl_close($ch);

        return $result;
    }
    
    
    
   
    
    /**
     * From a string tries to find a JSON substring
     *
     * @param string $string string which may hold a json inside
     * @return Object the json object found or null
     */
    public function findJSONObject($text) {
        $textLen = strlen( $text );
        $startIndex = -1;
        $endIndex = -1;

        // Count every time a { apperas
        $contOp = 0;

        // Lopp through each character
        for( $i = 0; $i <= $textLen; $i++ ) {
            $char = substr( $text, $i, 1 );

            if ($char == "{") {
                if ($contOp == 0) {
                    // Start of JSON
                    $startIndex = $i;
                }

                $contOp ++;
            }
            else if ($char == "}") {
                $contOp--;

                if ($contOp == 0) {
                    // Close of JSON found
                    $endIndex = $i;
                    break;
                }
            }
        }

        if ($startIndex != -1 && $endIndex != -1) {
            // Start and end found, try create json
            $jsonSubstring = substr($text, $startIndex, $endIndex - $startIndex + 1);

            // Try creating object
            return json_decode($jsonSubstring);
        }

        // Default value to return
        return null;
    }
}
?>